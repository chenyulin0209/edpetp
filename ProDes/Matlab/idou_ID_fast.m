
% 閒置曲線ID %
function[dott,A1,B1]=idou_ID_fast(Data,StartTime,Density,Tensuu,pn)
    


    path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\';
    content='迴路電流';
    [~,Guess.IV(:,1)]=textread([path '充電及放電\定便因_new\' '1V' content '.txt'],'%f %f','headerlines',1);
    content='雙汙染跨壓_內插後';
    [~,Guess.IV(:,2)]=textread([path '充電及放電\定便因_new\' '1V' content '.txt'],'%f %f','headerlines',1);
    
    %%%%% 預估線最大電流 %%%%%%
    Guess.IV(:,1)=Guess.IV(:,1)/1E12*Density;

    a=size(Data);
    x=1;
    if pn==1
        for i=1:a(1)-(Tensuu-1)
            idouT(x)=mean(Data(i:i+Tensuu-1,1));
            idouI(x)=mean(Data(i:i+Tensuu-1,2));
            if idouT(x)>StartTime
                dot1(1,1)=idouT(x);
                dot1(1,2)=idouI(x);
                [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),-Guess.IV(:,2),-dot1(2));
                [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),-dot1(3)/2);
                break;
            end
            x=x+1;
        end
        for i=x:a(1)-(Tensuu-1)
            idouT(x)=mean(Data(i:i+Tensuu-1,1));
            idouI(x)=mean(Data(i:i+Tensuu-1,2));
            if idouI(x)<-dot2(2)
                dot2(1)=idouT(x);
                dot2(2)=-idouI(x);
                break;
            end
            x=x+1;
        end
        dott(1,1)=dot1(1);
        dott(1,2)=dot1(2);
        dott(1,3)=-dot1(3);
        dott(2,1)=dot2(1);
        dott(2,2)=-dot2(2);
        dott(2,3)=dot2(3);
    end
    
    if pn==5
        for i=1:a(1)-(Tensuu-1)
            idouT(x)=mean(Data(i:i+Tensuu-1,1));
            idouI(x)=mean(Data(i:i+Tensuu-1,2));
            if idouT(x)>StartTime
                dot1(1,1)=idouT(x);
                dot1(1,2)=idouI(x);
                [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),-Guess.IV(:,2),-dot1(2));
                [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),-dot1(3)/2);
                break;
            end
            x=x+1;
        end
        for i=x:a(1)-(Tensuu-1)
            idouT(x)=mean(Data(i:i+Tensuu-1,1));
            idouI(x)=mean(Data(i:i+Tensuu-1,2));
            if idouI(x)>-dot2(2)
                dot2(1)=idouT(x);
                dot2(2)=-idouI(x);
                break;
            end
            x=x+1;
        end
        dott(1,1)=dot1(1);
        dott(1,2)=dot1(2);
        dott(1,3)=-dot1(3);
        dott(2,1)=dot2(1);
        dott(2,2)=-dot2(2);
        dott(2,3)=dot2(3);
    end
    
%     dot1
%     dot2
    
    

    
%     if pn==1

%             x=x+1;
%             if x>5 && idouT(x-2)<StartTime
%                 [dot1(1),dot1(2),~]=T_find_D(idouT(:,1),idouI(:,1),StartTime);
%                 [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),-Guess.IV(:,2),-dot1(2));
%                 dot(1,1)=dot1(1);
%                 dot(1,2)=dot1(2);
%                 dot(1,3)=-dot1(3);
%                 [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),-dot1(3)/2);
%                 lll=7
%             end
%         end
%             if x>10 && idouI(x-2)<=dot2(2) && idouI(x-1)<=dot2(2)
%                 [dot2(1),~,~]=D_find_T(idouT(:,1),-idouI(:,1),dot2(2));
%                 dot(2,1)=dot2(1);
%                 dot(2,2)=-dot2(2);
%                 dot(2,3)=dot2(3);
%                 dot
%                 break;
%             end
%         end
%     end
%     
%     if pn==5
%         for i=1:a(1)-(Tensuu-1)
%             idouT(x)=mean(Data(i:i+Tensuu-1,1));
%             idouI(x)=mean(Data(i:i+Tensuu-1,2));
%             x=x+1;
%             if x>5 && idouT(x-2)<=StartTime && idouT(x-1)>StartTime
%                 [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime);
%                 [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),-Guess.IV(:,2),-dot1(2));
%                 dot(1,1)=dot1(1);
%                 dot(1,2)=dot1(2);
%                 dot(1,3)=-dot1(3);
%                 [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),-dot1(3)/2);
%                 lll=8
%             end
%             if x>10 && idouI(x-2)<=dot2(2) && idouI(x-1)<=dot2(2)
%                 [dot2(1),~,~]=D_find_T(Data(:,1),Data(:,2),-dot2(2));
%                 dot(2,1)=dot2(1);
%                 dot(2,2)=-dot2(2);
%                 dot(2,3)=dot2(3);
%                 dot
%                 break;
%             end
%         end
%     end


    
    for i=1:2
        A(i,1)=1;
        A(i,2)=dott(i,1);
        Y(i,1)=log(dott(i,3));
    end
    
    hh=inv(A'*A)*A'*Y;
    ID_result=real(hh);
    A1=exp(real(hh(1)));
    B1=real(hh(2));
% A1=0;
% B1=0;


end






