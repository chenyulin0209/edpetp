%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們演算法的多實驗組數，是不是可以抗雜訊，固定實驗組數，看不同誤差下的效果
細節：

%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，量測到的電流值
Ireal=[-19.0513]*10^(-7);

Ireal_seco=[-5.83]*10^(-7);

% 讀資料，這邊電流要用高解析度的電流，因為快速取樣電流抗雜訊的時候，取樣週期很低，
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[T10,I10]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.4*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 快速取樣的取樣週期
rapid_S=15*10^(-6);

x=1;
% 執行次數，越多次平均誤差跟標準差會漸趨穩定
times=1000;
% 不同訊雜比都跑一次
for i=[0 5 10 15 20 25 30]
%   看要執行幾次
    for j=1:times
        j
%       加入雜訊，雜訊大小是用最大電流的%數決定
        gosa=i;
        Inoise=I10+rand(685601,1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%       因為我函式沒寫好，1組實驗的時候，要用santori_ID計算
        [Eorrc(j),~,A,B,Corrc(j),~]=santori_ID([T10,Inoise],tau_v*baisuu,interval,3,Ireal(1));
%       因為我函式沒寫好，用mean_I_ID的時候，實驗組數要乘上3
        [E2rc(j),~,A,B,C2rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,6,Ireal(1));
        [E4rc(j),~,A,B,C4rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,12,Ireal(1));
        [E8rc(j),~,A,B,C8rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,24,Ireal(1));
        [E16rc(j),~,A,B,C16rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,48,Ireal(1));
        
        
        [E32rc(j),~,A,B,C32rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,96,Ireal(1));
        [E64rc(j),~,A,B,C64rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,192,Ireal(1));
        [E128rc(j),~,A,B,C128rc(j),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,384,Ireal(1));
%       這個是最大值法
        II(j)=-(A+B);
        Etr(j)=(min(Inoise)-Ireal(1))*100/Ireal(1);
    end
    
%   電流值的誤差標準差應該沒有用到
    kI(x,1)=i;
    kI(x,2)=mean(II);
    kI(x,3)=std(II);
%   1組實驗的結果，包含電流間常數的估算值
    Eo(x,1)=i;
    Eo(x,2)=mean(Eorrc(:));
    Eo(x,3)=std(Eorrc(:));
    Eo(x,4)=mean(Corrc(:));
    Eo(x,5)=std(Corrc(:));
%   2組實驗的結果，包含電流間常數的估算值
    E22(x,1)=i;
    E22(x,2)=mean(E2rc(:));
    E22(x,3)=std(E2rc(:));
%   4組實驗的結果，包含電流間常數的估算值
    E44(x,1)=i;
    E44(x,2)=mean(E4rc(:));
    E44(x,3)=std(E4rc(:));
%   8組實驗的結果，包含電流間常數的估算值
    E88(x,1)=i;
    E88(x,2)=mean(E8rc(:));
    E88(x,3)=std(E8rc(:));
%   16組實驗的結果，包含電流間常數的估算值
    E16(x,1)=i;
    E16(x,2)=mean(E16rc(:));
    E16(x,3)=std(E16rc(:));
%   32組實驗的結果，包含電流間常數的估算值
    E32(x,1)=i;
    E32(x,2)=mean(E32rc(:));
    E32(x,3)=std(E32rc(:));
%   64組實驗的結果，包含電流間常數的估算值
    E64(x,1)=i;
    E64(x,2)=mean(E64rc(:));
    E64(x,3)=std(E64rc(:));
%   128組實驗的結果，包含電流間常數的估算值
    E128(x,1)=i;
    E128(x,2)=mean(E128rc(:));
    E128(x,3)=std(E128rc(:));
%   最大值法的結果
    E1(x,1)=i;
    E1(x,2)=mean(Etr(:));
    E1(x,3)=std(Etr(:));
%   理想的結果
    Eideal(x,1)=i;
    Eideal(x,2)=0;
    Eideal(x,3)=0;
    x=x+1;
end





