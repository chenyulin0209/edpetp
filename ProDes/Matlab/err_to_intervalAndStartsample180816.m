%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
將取樣區間做正規化，看取樣區間要怎麼取比較理想
細節：
這裡的橫軸是取樣週期，要注意這個問題(詳細說明在samp_proc_effect的細節)
%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
Ireal=[-19.0513,... % p1
        4.08347,... % n1
        6.39084,... % n2
        8.69623,... % n3
        11.0059,... % n4
        13.3189,... % n5
        -10.0701]*10^(-7);...  % p0.5
   

% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
[VC_500.p05.T,VC_500.p05.I]=textread([path '0.5V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n10.T,VC_500.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n20.T,VC_500.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n30.T,VC_500.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n40.T,VC_500.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

% content='掃描電壓';
% [VC_500.p10.T,VC_500.p10.V]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.p05.T,VC_500.p05.V]=textread([path '0.5V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n10.T,VC_500.n10.V]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n20.T,VC_500.n20.V]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n30.T,VC_500.n30.V]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n40.T,VC_500.n40.V]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n50.T,VC_500.n50.V]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% 
% 
% figure(4)
% plot(VC_500.p10.T,VC_500.p10.V,...
%     VC_500.p05.T,VC_500.p05.V,...
%     VC_500.n10.T,VC_500.n10.V,...
%     VC_500.n20.T,VC_500.n20.V,...
%     VC_500.n30.T,VC_500.n30.V);


% 幾倍的電壓時間常數取樣第一個值，這裡先固定5倍
baisuu=5;

% 以指數吻合法，在不同取樣區間下，將6種脈衝振幅都做一次
x=1;
for i=0.1*tau_c:0.1*tau_c:10*tau_c
	E_rc.err.int(x)=i;
    [E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_500.p10.T,VC_500.p10.I],tau_v*baisuu,i,3,Ireal(1));
    [E_rc.err.p05(x),~,~,~,~,~]=santori_ID([VC_500.p05.T,VC_500.p05.I],tau_v*baisuu,i,3,Ireal(7));
    [E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_500.n10.T,VC_500.n10.I],tau_v*baisuu,i,3,Ireal(2));
	[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_500.n20.T,VC_500.n20.I],tau_v*baisuu,i,3,Ireal(3));
	[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_500.n30.T,VC_500.n30.I],tau_v*baisuu,i,3,Ireal(4));
	[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_500.n40.T,VC_500.n40.I],tau_v*baisuu,i,3,Ireal(5));
	[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_500.n50.T,VC_500.n50.I],tau_v*baisuu,i,3,Ireal(6));
    x=x+1;
end
% 畫出正規化取樣週期對應電流估算誤差圖
subplot(1,2,1);
grid on

hold on
plot(...
E_rc.err.int/tau_c/2,-E_rc.err.p10,...
E_rc.err.int/tau_c/2,-E_rc.err.p05,...
E_rc.err.int/tau_c/2,-E_rc.err.n10,...
E_rc.err.int/tau_c/2,-E_rc.err.n30,...
E_rc.err.int/tau_c/2,-E_rc.err.n50)
xlim([0.1 5])
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
legend('1.0V','0.5V','-1.0V','-3.0V','-5.0V','location','northwest')
set(gca,'FontSize',12)
title('(a)')
plot(0.2,0:0.1:0.7,'-.');
plot(0.4,0:0.1:0.7,'-.');

clc;clear;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
Ireal=[-19.0513,... % p1
        4.08347,... % n1
        6.39084,... % n2
        8.69623,... % n3
        11.0059,... % n4
        13.3189,... % n5
        -10.0701...  % p0.5
        ]*10^(-7);

% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.p05.T,VC_500.p05.I]=textread([path '0.5V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n10.T,VC_500.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n20.T,VC_500.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n30.T,VC_500.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n40.T,VC_500.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前一個實驗知道0.4倍最理想
interval=0.4*tau_c;

% 以指數吻合法，在不同取樣延遲下，將6種脈衝振幅都做一次
x=1;
for i=4:10
	E_rc.err.Start(x)=i;
    [E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_500.p10.T,VC_500.p10.I],tau_v*i,interval,3,Ireal(1));
%   [E_rc.err.p05(x),~,~,~,~,~]=santori_ID([VC_500.p05.T,VC_500.p05.I],tau_v*i,interval,3,Ireal(7));
%   [E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_500.n10.T,VC_500.n10.I],tau_v*i,interval,3,Ireal(2));
% 	[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_500.n20.T,VC_500.n20.I],tau_v*i,interval,3,Ireal(3));
% 	[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_500.n30.T,VC_500.n30.I],tau_v*i,interval,3,Ireal(4));
% 	[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_500.n40.T,VC_500.n40.I],tau_v*i,interval,3,Ireal(5));
% 	[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_500.n50.T,VC_500.n50.I],tau_v*i,interval,3,Ireal(6));
    x=x+1;
end

% 畫出正規化取樣延遲對應電流估算誤差圖
subplot(1,2,2);
grid on
hold on
plot(...
E_rc.err.Start,-E_rc.err.p10,...
E_rc.err.Start,-E_rc.err.p05,...
E_rc.err.Start,-E_rc.err.n10,...
E_rc.err.Start,-E_rc.err.n30,...
E_rc.err.Start,-E_rc.err.n50)
xlabel('\fontsize{12}Normalized Sample Delay(\tau_v)');ylabel('\fontsize{12}Error(%)');
legend('1.0V','0.5V','-1.0V','-3.0V','-5.0V','location','southeast')
set(gca,'FontSize',12)
title('(b)')
