%{
目的：
使用最大值法和指數吻合法，分別擷取IV曲線，再使用同樣的溫濃演算法，計算出溫濃
細節：

%}
clc;clear;close all;

% 電壓時間常數
tau_v=0.001;

% 讀資料
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\';
content='掃描電壓';
[wave.T2000D1E9.T,wave.T2000D1E9.Vs]=textread([path '2000K_1E9\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E10.T,wave.T2000D1E10.Vs]=textread([path '2000K_1E10\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E11.T,wave.T2000D1E11.Vs]=textread([path '2000K_1E11\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E12.T,wave.T2000D1E12.Vs]=textread([path '2000K_1E12\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.T2000D1E9.T,wave.T2000D1E9.I]=textread([path '2000K_1E9\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E10.T,wave.T2000D1E10.I]=textread([path '2000K_1E10\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E11.T,wave.T2000D1E11.I]=textread([path '2000K_1E11\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E12.T,wave.T2000D1E12.I]=textread([path '2000K_1E12\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.T2000D1E9.T,wave.T2000D1E9.Vc]=textread([path '2000K_1E9\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E10.T,wave.T2000D1E10.Vc]=textread([path '2000K_1E10\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E11.T,wave.T2000D1E11.Vc]=textread([path '2000K_1E11\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
[wave.T2000D1E12.T,wave.T2000D1E12.Vc]=textread([path '2000K_1E12\漸升式三角波\-5.1至1間隔0.1高解析\' content '.txt'],'%f %f','headerlines',1);
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

% 可以用下面這個讀取資料，用上面的方法，每跑一次程式都要重新讀資料，速度又慢，因此使用以下的方法
% save可以儲存資料
% save ronbun_D wave
% load可以讀取資料
% load ronbun_D wave

disp(['數據讀取完畢'])

% 輸入各個掃描脈波，自動偵測脈波起始點編號，脈波起始點時間，脈衝大小，脈衝由負轉正的編號，總脈波數
[wave.T2000D1E9.RI,wave.T2000D1E9.RT,wave.T2000D1E9.amp,wave.T2000D1E9.NtoP,wave.T2000D1E9.PN]=FindPulseStart(wave.T2000D1E9.T,wave.T2000D1E9.Vs);
[wave.T2000D1E10.RI,wave.T2000D1E10.RT,wave.T2000D1E10.amp,wave.T2000D1E10.NtoP,wave.T2000D1E10.PN]=FindPulseStart(wave.T2000D1E10.T,wave.T2000D1E10.Vs);
[wave.T2000D1E11.RI,wave.T2000D1E11.RT,wave.T2000D1E11.amp,wave.T2000D1E11.NtoP,wave.T2000D1E11.PN]=FindPulseStart(wave.T2000D1E11.T,wave.T2000D1E11.Vs);
[wave.T2000D1E12.RI,wave.T2000D1E12.RT,wave.T2000D1E12.amp,wave.T2000D1E12.NtoP,wave.T2000D1E12.PN]=FindPulseStart(wave.T2000D1E12.T,wave.T2000D1E12.Vs);

% 訊雜比
gosa=5;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 執行次數，越多次平均誤差跟標準差會漸趨穩定
kai=1;
% 因為我函式沒寫好，用Fit_I的時候，實驗組數要乘上3，所以這裡實際上是16組指數函數法
dot=48;

% 執行好幾次溫濃計算
for j=1:kai

%   加上雜訊
    wave.T2000D1E9.Inoise=wave.T2000D1E9.I+rand(size(wave.T2000D1E9.I,1),1)*abs(min(wave.T2000D1E9.I))*gosa/100-abs(min(wave.T2000D1E9.I))*gosa/100/2;
    wave.T2000D1E10.Inoise=wave.T2000D1E10.I+rand(size(wave.T2000D1E10.I,1),1)*abs(min(wave.T2000D1E10.I))*gosa/100-abs(min(wave.T2000D1E10.I))*gosa/100/2;
    wave.T2000D1E11.Inoise=wave.T2000D1E11.I+rand(size(wave.T2000D1E11.I,1),1)*abs(min(wave.T2000D1E11.I))*gosa/100-abs(min(wave.T2000D1E11.I))*gosa/100/2;
    wave.T2000D1E12.Inoise=wave.T2000D1E12.I+rand(size(wave.T2000D1E12.I,1),1)*abs(1.703E-5)*gosa/100-abs(1.703E-5)*gosa/100/2;

%   對不同濃度下的電流，使用指數函數法
    tau_c=0.5;interval=0.4*tau_c;
    [wave.T2000D1E9.IV_rc(1,:),wave.T2000D1E9.IV_rc(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.44;interval=0.4*tau_c;
    [wave.T2000D1E10.IV_rc(1,:),wave.T2000D1E10.IV_rc(2,:)]=Fit_I(wave.T2000D1E10.T,wave.T2000D1E10.amp,wave.T2000D1E10.Inoise,wave.T2000D1E10.PN,wave.T2000D1E10.RT,wave.T2000D1E10.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.17;interval=0.4*tau_c;
    [wave.T2000D1E11.IV_rc(1,:),wave.T2000D1E11.IV_rc(2,:)]=Fit_I(wave.T2000D1E11.T,wave.T2000D1E11.amp,wave.T2000D1E11.Inoise,wave.T2000D1E11.PN,wave.T2000D1E11.RT,wave.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
%{   
這邊實際上不是0.5，但選用0.5的原因為
1.這裡的脈衝曲線因為PSPICE的問題，所以tau_v設定得不夠小，會出現tau_v_effect的問題
2.根據err_to_interval，取樣週期怎麼取，都計算出來的誤差都差不多
%}
    tau_c=0.5;interval=0.4*tau_c;
    [wave.T2000D1E12.IV_rc(1,:),wave.T2000D1E12.IV_rc(2,:)]=Fit_I(wave.T2000D1E12.T,wave.T2000D1E12.amp,wave.T2000D1E12.Inoise,wave.T2000D1E12.PN,wave.T2000D1E12.RT,wave.T2000D1E12.RI,baisuu*tau_v,0.4*tau_c,dot);

    
%   對不同濃度下的電流，使用最大值法
    [wave.T2000D1E9.IV_ma(1,:),wave.T2000D1E9.IV_ma(2,:)]=Max_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI);
    [wave.T2000D1E10.IV_ma(1,:),wave.T2000D1E10.IV_ma(2,:)]=Max_I(wave.T2000D1E10.T,wave.T2000D1E10.amp,wave.T2000D1E10.Inoise,wave.T2000D1E10.PN,wave.T2000D1E10.RT,wave.T2000D1E10.RI);
    [wave.T2000D1E11.IV_ma(1,:),wave.T2000D1E11.IV_ma(2,:)]=Max_I(wave.T2000D1E11.T,wave.T2000D1E11.amp,wave.T2000D1E11.Inoise,wave.T2000D1E11.PN,wave.T2000D1E11.RT,wave.T2000D1E11.RI);
    [wave.T2000D1E12.IV_ma(1,:),wave.T2000D1E12.IV_ma(2,:)]=Max_I(wave.T2000D1E12.T,wave.T2000D1E12.amp,wave.T2000D1E12.Inoise,wave.T2000D1E12.PN,wave.T2000D1E12.RT,wave.T2000D1E12.RI);

%   使用指數函數法得到的IV曲線，計算出溫濃
    [T.T2000D1E9.rc(j),D.T2000D1E9.rc(j),~,~]=ronbunTD(wave.T2000D1E9.IV_rc(1,:),wave.T2000D1E9.IV_rc(2,:));
    [T.T2000D1E10.rc(j),D.T2000D1E10.rc(j),~,~]=ronbunTD(wave.T2000D1E10.IV_rc(1,:),wave.T2000D1E10.IV_rc(2,:));
    [T.T2000D1E11.rc(j),D.T2000D1E11.rc(j),~,~]=ronbunTD(wave.T2000D1E11.IV_rc(1,:),wave.T2000D1E11.IV_rc(2,:));
    [T.T2000D1E12.rc(j),D.T2000D1E12.rc(j),~,~]=ronbunTD(wave.T2000D1E12.IV_rc(1,:),wave.T2000D1E12.IV_rc(2,:));

%   使用最大值法得到的IV曲線，計算出溫濃
    [T.T2000D1E9.ma(j),D.T2000D1E9.ma(j),~,~]=ronbunTD(wave.T2000D1E9.IV_ma(1,:),wave.T2000D1E9.IV_ma(2,:));
    [T.T2000D1E10.ma(j),D.T2000D1E10.ma(j),~,~]=ronbunTD(wave.T2000D1E10.IV_ma(1,:),wave.T2000D1E10.IV_ma(2,:));
    [T.T2000D1E11.ma(j),D.T2000D1E11.ma(j),~,~]=ronbunTD(wave.T2000D1E11.IV_ma(1,:),wave.T2000D1E11.IV_ma(2,:));
    [T.T2000D1E12.ma(j),D.T2000D1E12.ma(j),~,~]=ronbunTD(wave.T2000D1E12.IV_ma(1,:),wave.T2000D1E12.IV_ma(2,:));


    disp(['執行次數  ' num2str(j) '/' num2str(kai) '     進度' num2str(j/kai*100) '%' ])


end

% 可以儲存溫濃的資料，放入D_err處理
% save ronbun_D_data T D

% 顯示出各電流
subplot(2,2,1)
plot(wave.T2000D1E9.T,wave.T2000D1E9.I)
subplot(2,2,2)
plot(wave.T2000D1E10.T,wave.T2000D1E10.I)
subplot(2,2,3)
plot(wave.T2000D1E11.T,wave.T2000D1E11.I)
subplot(2,2,4)
plot(wave.T2000D1E12.T,wave.T2000D1E12.I)

% 顯示出掃描脈波
figure()
plot(wave.T2000D1E11.T,wave.T2000D1E11.Vs)
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Voltage(V)');
set(gca,'FontSize',12)
grid on

