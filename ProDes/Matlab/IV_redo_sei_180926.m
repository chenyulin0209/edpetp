% 脈波閒置時間最佳化 第一分項實驗 LP脈衝下降緣，汙染層電壓/迴路電流，IV曲線重現性實驗 %
% 碳針：面積=A beta=0.25 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=0.5 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改振幅\';
content='掃描電壓';
[p10.T,p10.Vs]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);

content='迴路電流';
[p10.T,p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);

content='汙染跨壓';
[p10.T,p10.V]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);

st=2.51;
ov=4.9;

sizeP10 = size(p10.I);
for ramp=1:sizeP10
    p10_tamp.I(ramp+1)=p10.I(ramp);
    p10_tamp.T(ramp+1)=p10.T(ramp);
end
aa=p10_tamp.I';
p10_tamp.T=p10_tamp.T+0.5;
% 理想情況下，量測到的電流值
Ireal=[-5.83]*10^(-7)
%加入雜訊，雜訊大小是用最大電流的%數(gosa)決定
gosa=0.1;
Inoise=aa+rand(50589,1)*Ireal*gosa-Ireal*gosa/2;
Inoise(1,1)=0;
p10_tamp.T(1)=0;




figure()
% subplot(2,2,1)
plot(...
 p10_tamp.T,-Inoise)
% xlim([1.9 5]);
% ylim([-0.9*10^(-7) 0.8*10^(-7)]);
% title('\fontsize{12}不同振幅下的迴路電流曲線');
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Current(A)');
legend('1.0V','Location','southeast')
set(gca,'FontSize',12)
grid on ;
set(gca,'FontSize',12)