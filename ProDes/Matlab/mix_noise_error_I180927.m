%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們演算法的多實驗組數，是不是可以抗雜訊，固定實驗組數，看不同誤差下的效果
細節：

%}

clc;clear;close all;
% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
%      p10      n10     n30      n50
Ireal=[-19.0513,4.08347,8.69623,13.3189]*10^(-7);
%       p08       p04      p02     
Ireal2=[-1.6004,-0.76772,-0.30994]*10^(-6);

Ireal_seco=[-5.83]*10^(-7);
% 各電壓相對應電流矩陣數
%             1.0   0.8      0.4     0.2     -1.0  -3.0   -5.0
array_count=[685601, 500494 ,500479, 500479, 57260, 57464, 57705 ];

% 讀資料，這邊電流要用高解析度的電流，因為快速取樣電流抗雜訊的時候，取樣週期很低，
% path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
% content='迴路電流';
% [T10,I10]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
% [T08,I08]=textread([path '0.8V\' content '.txt'],'%f %f','headerlines',1);
% [T04,I04]=textread([path '0.4V\' content '.txt'],'%f %f','headerlines',1);
% [T02,I02]=textread([path '0.2V\' content '.txt'],'%f %f','headerlines',1);
% [Tn1,In1]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
% [Tn3,In3]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
% [Tn5,In5]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% 
% % 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
% interval=0.4*tau_c;
% % 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
% baisuu=7;
% % 快速取樣的取樣週期
% rapid_S=15*10^(-6);
% 
% x=1;
% % 執行次數，越多次平均誤差跟標準差會漸趨穩定
% times=1000;
% 
% % 不同訊雜比都跑一次
% for j=1:times
%     count=1;
%     for i=[0.003 0.004 0.1 0.2 0.3 0.4 0.5]*tau_c
% %           看要執行幾次/
% 
% %           加入雜訊，雜訊大小是用最大電流的%數決定
%             gosa=10;
%             Inoise=I10+rand(array_count(1),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             %單組實驗
%             [E_rc.p10(j,count),~,~,~,~,~]=santori_ID([T10,Inoise],tau_v*baisuu,i,3,Ireal(1));
% %           多組實驗 1.0V
%             [E128rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%             [E64rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(1));
%             [E32rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(1));
% %           多組實驗 0.8V
%             Inoise=I08+rand(array_count(2),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(1));
%             [E64rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(1));
%             [E32rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(1));
% %           多組實驗 0.4V
%             Inoise=I04+rand(array_count(3),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(2));
%             [E64rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(2));
%             [E32rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(2));
% %           多組實驗 0.2V
%             Inoise=I02+rand(array_count(4),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(3));
%             [E64rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(3));
%             [E32rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(3));
% %           多組實驗 -1.0V
%             Inoise=In1+rand(array_count(5),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(2));
%             [E64rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(2));
%             [E32rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(2));
% %           多組實驗 -3.0V
%             Inoise=In3+rand(array_count(6),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(3));
%             [E64rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(3));
%             [E32rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(3));
% %           多組實驗 -5.0V
%             Inoise=In5+rand(array_count(7),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(4));
%             [E64rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(4));
%             [E32rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(4));
%             
%             
% %             [E16rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,48,Ireal(1));
% %             [E8rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,24,Ireal(1));
% %             [E4rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,12,Ireal(1));
% %             [E2rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,6,Ireal(1));
%     %         [E128rc_p09(j),~,A,B,C128rc_p09(j),~]=mean_I_ID([T09,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p08(j),~,A,B,C128rc_p08(j),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p07(j),~,A,B,C128rc_p07(j),~]=mean_I_ID([T07,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p06(j),~,A,B,C128rc_p06(j),~]=mean_I_ID([T06,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p05(j),~,A,B,C128rc_p05(j),~]=mean_I_ID([T05,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p04(j),~,A,B,C128rc_p04(j),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p03(j),~,A,B,C128rc_p03(j),~]=mean_I_ID([T03,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p02(j),~,A,B,C128rc_p02(j),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%     %         [E128rc_p01(j),~,A,B,C128rc_p01(j),~]=mean_I_ID([T01,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
% 
% 
% 
%    
%     % %   128組實驗的結果，包含電流間常數的估算值 0.9V
%     %     E128_p09(x,1)=i;
%     %     E128_p09(x,2)=mean(E128rc_p09(:));
%     %     E128_p09(x,3)=std(E128rc_p09(:)); 
%     % %   128組實驗的結果，包含電流間常數的估算值 0.8V
%     %     E128_p08(x,1)=i;
%     %     E128_p08(x,2)=mean(E128rc_p08(:));
%     %     E128_p08(x,3)=std(E128rc_p08(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.7V
%     %     E128_p07(x,1)=i;
%     %     E128_p07(x,2)=mean(E128rc_p07(:));
%     %     E128_p07(x,3)=std(E128rc_p07(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.6V
%     %     E128_p06(x,1)=i;
%     %     E128_p06(x,2)=mean(E128rc_p06(:));
%     %     E128_p06(x,3)=std(E128rc_p06(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.5V
%     %     E128_p05(x,1)=i;
%     %     E128_p05(x,2)=mean(E128rc_p05(:));
%     %     E128_p05(x,3)=std(E128rc_p05(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.4V
%     %     E128_p04(x,1)=i;
%     %     E128_p04(x,2)=mean(E128rc_p04(:));
%     %     E128_p04(x,3)=std(E128rc_p04(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.3V
%     %     E128_p03(x,1)=i;
%     %     E128_p03(x,2)=mean(E128rc_p03(:));
%     %     E128_p03(x,3)=std(E128rc_p02(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.2V
%     %     E128_p02(x,1)=i;
%     %     E128_p02(x,2)=mean(E128rc_p02(:));
%     %     E128_p02(x,3)=std(E128rc_p02(:));
%     % %   128組實驗的結果，包含電流間常數的估算值 0.1V
%     %     E128_p01(x,1)=i;
%     %     E128_p01(x,2)=mean(E128rc_p01(:));
%     %     E128_p01(x,3)=std(E128rc_p01(:));
%         
%         count=count+1;
%     end
%  
% end
% x=1;
% for i=[0.003 0.004 0.1 0.2 0.3 0.4 0.5]
%      %   128組實驗的結果，包含電流間常數的估算值 1.0V
%         E128_p10(x,1)=i;
%         E128_p10(x,2)=mean(E128rc_p10(:,x));
%         E128_p10(x,3)=std(E128rc_p10(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 1.0V
%         E64_p10(x,1)=i;
%         E64_p10(x,2)=mean(E64rc_p10(:,x));
%         E64_p10(x,3)=std(E64rc_p10(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 1.0V
%         E32_p10(x,1)=i;
%         E32_p10(x,2)=mean(E32rc_p10(:,x));
%         E32_p10(x,3)=std(E32rc_p10(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.8V
%         E128_p08(x,1)=i;
%         E128_p08(x,2)=mean(E128rc_p08(:,x));
%         E128_p08(x,3)=std(E128rc_p08(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.8V
%         E64_p08(x,1)=i;
%         E64_p08(x,2)=mean(E64rc_p08(:,x));
%         E64_p08(x,3)=std(E64rc_p08(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 0.8V
%         E32_p08(x,1)=i;
%         E32_p08(x,2)=mean(E32rc_p08(:,x));
%         E32_p08(x,3)=std(E32rc_p08(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.4V
%         E128_p04(x,1)=i;
%         E128_p04(x,2)=mean(E128rc_p04(:,x));
%         E128_p04(x,3)=std(E128rc_p04(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.4V
%         E64_p04(x,1)=i;
%         E64_p04(x,2)=mean(E64rc_p04(:,x));
%         E64_p04(x,3)=std(E64rc_p04(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 0.4V
%         E32_p04(x,1)=i;
%         E32_p04(x,2)=mean(E32rc_p04(:,x));
%         E32_p04(x,3)=std(E32rc_p04(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.2V
%         E128_p02(x,1)=i;
%         E128_p02(x,2)=mean(E128rc_p02(:,x));
%         E128_p02(x,3)=std(E128rc_p02(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.2V
%         E64_p02(x,1)=i;
%         E64_p02(x,2)=mean(E64rc_p02(:,x));
%         E64_p02(x,3)=std(E64rc_p02(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 0.2V
%         E32_p02(x,1)=i;
%         E32_p02(x,2)=mean(E32rc_p02(:,x));
%         E32_p02(x,3)=std(E32rc_p02(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -1.0V
%         E128_n10(x,1)=i;
%         E128_n10(x,2)=mean(E128rc_n10(:,x));
%         E128_n10(x,3)=std(E128rc_n10(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -1.0V
%         E64_n10(x,1)=i;
%         E64_n10(x,2)=mean(E64rc_n10(:,x));
%         E64_n10(x,3)=std(E64rc_n10(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 -1.0V
%         E32_n10(x,1)=i;
%         E32_n10(x,2)=mean(E32rc_n10(:,x));
%         E32_n10(x,3)=std(E32rc_n10(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -3.0V
%         E128_n30(x,1)=i;
%         E128_n30(x,2)=mean(E128rc_n30(:,x));
%         E128_n30(x,3)=std(E128rc_n30(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -3.0V
%         E64_n30(x,1)=i;
%         E64_n30(x,2)=mean(E64rc_n30(:,x));
%         E64_n30(x,3)=std(E64rc_n30(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 -3.0V
%         E32_n30(x,1)=i;
%         E32_n30(x,2)=mean(E32rc_n30(:,x));
%         E32_n30(x,3)=std(E32rc_n30(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -5.0V
%         E128_n50(x,1)=i;
%         E128_n50(x,2)=mean(E128rc_n50(:,x));
%         E128_n50(x,3)=std(E128rc_n50(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -5.0V
%         E64_n50(x,1)=i;
%         E64_n50(x,2)=mean(E64rc_n50(:,x));
%         E64_n50(x,3)=std(E64rc_n50(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 -5.0V
%         E32_n50(x,1)=i;
%         E32_n50(x,2)=mean(E32rc_n50(:,x));
%         E32_n50(x,3)=std(E32rc_n50(:,x)); 
%         
%     x=x+1;
% end
load('current_circuit_200k_1E11tau_v_0.1674over500_error.mat');

%     掃描電壓1.0V
figure()
hold on
errorbar(E128_p10(:,1),E128_p10(:,2),E128_p10(:,3));
errorbar(E64_p10(:,1),E64_p10(:,2),E64_p10(:,3));
errorbar(E32_p10(:,1),E32_p10(:,2),E32_p10(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('1.0V')
%     掃描電壓0.8V
figure()
hold on
errorbar(E128_p08(:,1),E128_p08(:,2),E128_p08(:,3));
errorbar(E64_p08(:,1),E64_p08(:,2),E64_p08(:,3));
errorbar(E32_p08(:,1),E32_p08(:,2),E32_p08(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('0.8V')
%     掃描電壓0.4V
figure()
hold on
errorbar(E128_p04(:,1),E128_p04(:,2),E128_p04(:,3));
errorbar(E64_p04(:,1),E64_p04(:,2),E64_p04(:,3));
errorbar(E32_p04(:,1),E32_p04(:,2),E32_p04(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('0.4V')
%     掃描電壓0.2V
figure()
hold on
errorbar(E128_p02(:,1),E128_p02(:,2),E128_p02(:,3));
errorbar(E64_p02(:,1),E64_p02(:,2),E64_p02(:,3));
errorbar(E32_p02(:,1),E32_p02(:,2),E32_p02(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('0.2V')
%     掃描電壓-1.0V
figure()
hold on
errorbar(E128_n10(:,1),E128_n10(:,2),E128_n10(:,3));
errorbar(E64_n10(:,1),E64_n10(:,2),E64_n10(:,3));
errorbar(E32_n10(:,1),E32_n10(:,2),E32_n10(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('-1.0V')
%     掃描電壓-3.0V
figure()
hold on
errorbar(E128_n30(:,1),E128_n30(:,2),E128_n30(:,3));
errorbar(E64_n30(:,1),E64_n30(:,2),E64_n30(:,3));
errorbar(E32_n30(:,1),E32_n30(:,2),E32_n30(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('-3.0V')
%     掃描電壓-5.0V
figure()
hold on
errorbar(E128_n50(:,1),E128_n50(:,2),E128_n50(:,3));
errorbar(E64_n50(:,1),E64_n50(:,2),E64_n50(:,3));
errorbar(E32_n50(:,1),E32_n50(:,2),E32_n50(:,3));
xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
legend('128組','64組','32組','location','northwest')
title('-5.0V')








