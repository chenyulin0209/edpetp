% 學長函式，先保留 %

function[Ie,Ii,I]=DebyeCurrentSim(Vpr,Vpl,Ne,Ni,Te,A,beta)
%{
 Vpr 輸入探針電壓    ex -1~0.5
 Vpl 浮動電位，      ex 0
 Ne  電子濃度   (1~5*10^4 cm^-3)
 Te  電子溫度   (1500K)
 探針類型  beta:0 平板 0.5:圓柱 1:球
%}
%固定的科學常數
e=1.602*10^-19;
m_e=9.109*10^-31;
m_i=14*1.66*(10^-27);
k=1.3806488*10^-23;

if Vpr>Vpl
    Ie=-e*A*Ne*sqrt( (k*Te)/(2*pi*m_e) )*( 1+e*(Vpr-Vpl)/(k*Te) ).^beta; 
    Ii= e*A*Ni*sqrt( (k*Te)/(2*pi*m_i) )*exp(-e*(Vpr-Vpl)/(k*Te));
    I =Ii+Ie;
else
    Ie=-e*A*Ne*sqrt( (k*Te)/(2*pi*m_e) )*exp( e*(Vpr-Vpl)/(k*Te) );
    Ii= e*A*Ni*sqrt( (k*Te)/(2*pi*m_i) )*( 1-e*(Vpr-Vpl)/(k*Te) ).^beta;
    I =Ii+Ie;
end