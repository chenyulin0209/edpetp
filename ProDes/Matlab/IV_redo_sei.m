% 脈波閒置時間最佳化 第一分項實驗 LP脈衝下降緣，汙染層電壓/迴路電流，IV曲線重現性實驗 %
% 碳針：面積=A beta=0.25 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=0.5 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改振幅\';
content='掃描電壓';
[p10.T,p10.Vs]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[n10.T,n10.Vs]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[n20.T,n20.Vs]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[n30.T,n30.Vs]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[n40.T,n40.Vs]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[n50.T,n50.Vs]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

content='迴路電流';
[p10.T,p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[n10.T,n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[n20.T,n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[n30.T,n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[n40.T,n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[n50.T,n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

content='汙染跨壓';
[p10.T,p10.V]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[n10.T,n10.V]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[n20.T,n20.V]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[n30.T,n30.V]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[n40.T,n40.V]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[n50.T,n50.V]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

heii=[0.18 0.11 0.065 0.029];

st=2.51;
ov=4.9;

[p10.IV(:,1),p10.IV(:,2)]=Ideal_IV_make(p10.T,p10.V,p10.I,st,ov);
[n10.IV(:,1),n10.IV(:,2)]=Ideal_IV_make(n10.T,n10.V,n10.I,st,ov);
[n20.IV(:,1),n20.IV(:,2)]=Ideal_IV_make(n20.T,n20.V,n20.I,st,ov);
[n30.IV(:,1),n30.IV(:,2)]=Ideal_IV_make(n30.T,n30.V,n30.I,st,ov);
[n40.IV(:,1),n40.IV(:,2)]=Ideal_IV_make(n40.T,n40.V,n40.I,st,ov);
[n50.IV(:,1),n50.IV(:,2)]=Ideal_IV_make(n50.T,n50.V,n50.I,st,ov);

figure()
% subplot(2,2,1)
plot(...
n50.T,-n50.I,...
n40.T,-n40.I,...
n30.T,-n30.I,...
n20.T,-n20.I,...
n10.T,-n10.I,...
p10.T,-p10.I)
% xlim([1.9 5]);
% ylim([-0.9*10^(-7) 0.8*10^(-7)]);
% title('\fontsize{12}不同振幅下的迴路電流曲線');
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Current(A)');
legend('-5.0V','-4.0V','-3.0V','-2.0V','-1.0V','1.0V','Location','southeast')
set(gca,'FontSize',12)

figure()
% subplot(2,2,3)
plot(...
n50.T,n50.I,...
n40.T+heii(4),n40.I,...
n30.T+heii(3),n30.I,...
n20.T+heii(2),n20.I,...
n10.T+heii(1),n10.I,...
p10.T,p10.I)
xlim([0 5]);
% ylim([-0.9*10^(-7) 0.8*10^(-7)]);
% title('\fontsize{12}不同振幅下的迴路電流曲線(平移後)');
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Current(A)');
legend('-5.0V','-4.0V','-3.0V','-2.0V','-1.0V','1.0V','Location','southeast')
set(gca,'FontSize',12)

figure()
% subplot(2,2,2)
plot(...
n50.T,n50.V,...
n40.T,n40.V,...
n30.T,n30.V,...
n20.T,n20.V,...
n10.T,n10.V,...
p10.T,p10.V)
% ylim([-0.9 0.2])
% title('\fontsize{12}不同振幅下的汙染層跨壓曲線');
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Voltage of Contamination(V)');
legend('-5.0V','-4.0V','-3.0V','-2.0V','-1.0V','1.0V','Location','southeast')
set(gca,'FontSize',12)


figure()
% subplot(2,2,4)
plot(...
n50.T,n50.V,...
n40.T+heii(4),n40.V,...
n30.T+heii(3),n30.V,...
n20.T+heii(2),n20.V,...
n10.T+heii(1),n10.V,...
p10.T,p10.V)
xlim([0 5]);
% ylim([-0.9 0.2])
% title('\fontsize{12}不同振幅下的汙染層跨壓曲線(平移後)');
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Voltage of Contamination(V)');
legend('-5.0V','-4.0V','-3.0V','-2.0V','-1.0V','1.0V','Location','southeast')
set(gca,'FontSize',12)

figure()
% subplot(2,2,1)
grid on
hold on
plot(...
n50.IV(:,1),-n50.IV(:,2),...
n40.IV(:,1),-n40.IV(:,2),...
n30.IV(:,1),-n30.IV(:,2),...
n20.IV(:,1),-n20.IV(:,2),...
n10.IV(:,1),-n10.IV(:,2),...
p10.IV(:,1),-p10.IV(:,2))
% title('\fontsize{12}不同振幅下汙染層跨壓/迴路電流IV曲線');
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('-5.0V','-4.0V','-3.0V','-2.0V','-1.0V','1.0V','Location','southeast')
set(gca,'FontSize',12)

figure()
% subplot(2,2,2)
grid on
hold on
plot(...
p10.IV(:,1),-p10.IV(:,2),...
n50.IV(:,1),-n50.IV(:,2))
% title('\fontsize{12}不同振幅下汙染層跨壓/迴路電流IV曲線');
xlabel('\fontsize{12}Current(A)');ylabel('\fontsize{12}Voltage of Contamination(V)');

set(gca,'FontSize',12)