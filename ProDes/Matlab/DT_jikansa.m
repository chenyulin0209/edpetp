% 各溫濃下時間估算誤差 %

clear;clc;close all;

%%%%%% 路徑 %%%%%%
ath='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\1500K_1E11\充電及放電\定變因\';
content='掃描電壓';
[T1500.D1E11.T1,T1500.D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500.D1E11.T5,T1500.D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T1500.D1E11.T1,T1500.D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500.D1E11.T5,T1500.D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T1500.D1E11.T1,T1500.D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500.D1E11.T5,T1500.D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);


path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E9\充電及放電\定變因\';
content='掃描電壓';
[T2000.D1E9.T1,T2000.D1E9.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E9.T5,T2000.D1E9.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2000.D1E9.T1,T2000.D1E9.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E9.T5,T2000.D1E9.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2000.D1E9.T1,T2000.D1E9.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E9.T5,T2000.D1E9.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E10\充電及放電\定變因\';
content='掃描電壓';
[T2000.D1E10.T1,T2000.D1E10.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E10.T5,T2000.D1E10.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2000.D1E10.T1,T2000.D1E10.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E10.T5,T2000.D1E10.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2000.D1E10.T1,T2000.D1E10.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E10.T5,T2000.D1E10.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因\';
content='掃描電壓';
[T2000.D1E11.T1,T2000.D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E11.T5,T2000.D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2000.D1E11.T1,T2000.D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E11.T5,T2000.D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2000.D1E11.T1,T2000.D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E11.T5,T2000.D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E12\充電及放電\定變因\';
content='掃描電壓';
[T2000.D1E12.T1,T2000.D1E12.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E12.T5,T2000.D1E12.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2000.D1E12.T1,T2000.D1E12.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E12.T5,T2000.D1E12.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2000.D1E12.T1,T2000.D1E12.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000.D1E12.T5,T2000.D1E12.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\充電及放電\定便因_new\';
content='掃描電壓';
[T2500.D1E11.T1,T2500.D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500.D1E11.T5,T2500.D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2500.D1E11.T1,T2500.D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500.D1E11.T5,T2500.D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2500.D1E11.T1,T2500.D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500.D1E11.T5,T2500.D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);



% %save
save('NoInter_Diff_DandT_data.mat');
%  load('no_inter_data_tamp');

ten=1;
Srapid=0.01;

tau_v=0.001;
% tau_c/tau_v=500
tau_c = tau_v *500;
% 幾倍的tau_c -> Ideal_idou_ID()
times = 0.5;
Step = 10^-6;%15*10^-6; 
Ti=3.5;
Ten=100;
kan=0.0001;
baisuu=7;
%{
D=1E9;
[S1,D1E9.A1,D1E9.B1]=Ideal_idou_ID([D1E9.T1,D1E9.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,D1E9.A5,D1E9.B5]=Ideal_idou_ID([D1E9.T5,D1E9.I5],Ti+baisuu*tau_v,D,Ten,5);
[D1E9.E1(:,2),D1E9.E1(:,1)]=jikansa(D1E9.T1,D1E9.V1,D1E9.A1,D1E9.B1,0.0001,abs(min(D1E9.V1)),kan);
[D1E9.E5(:,2),D1E9.E5(:,1)]=jikansa(D1E9.T5,-D1E9.V5,-D1E9.A5,D1E9.B5,0.0001,abs(max(D1E9.V5)),kan);
figure(1)
% subplot(2,2,1)
plot(D1E9.E1(:,2),D1E9.E1(:,1),D1E9.E5(:,2),D1E9.E5(:,1))
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)

D=1E10;
[S1,D1E10.A1,D1E10.B1]=Ideal_idou_ID([D1E10.T1,D1E10.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,D1E10.A5,D1E10.B5]=Ideal_idou_ID([D1E10.T5,D1E10.I5],Ti+baisuu*tau_v,D,Ten,5);
[D1E10.E1(:,2),D1E10.E1(:,1)]=jikansa(D1E10.T1,D1E10.V1,D1E10.A1,D1E10.B1,0.001,abs(min(D1E10.V1)),kan);
[D1E10.E5(:,2),D1E10.E5(:,1)]=jikansa(D1E10.T5,-D1E10.V5,-D1E10.A5,D1E10.B5,0.001,abs(max(D1E10.V5)),kan);
figure(2)
% subplot(2,2,2)
plot(D1E10.E1(:,2),D1E10.E1(:,1),D1E10.E5(:,2),D1E10.E5(:,1))
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)

kan=0.001;
D=1E11;
[S1,D1E11.A1,D1E11.B1]=Ideal_idou_ID([D1E11.T1,D1E11.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,D1E11.A5,D1E11.B5]=Ideal_idou_ID([D1E11.T5,D1E11.I5],Ti+baisuu*tau_v,D,Ten,5);
[D1E11.E1(:,2),D1E11.E1(:,1)]=jikansa(D1E11.T1,D1E11.V1,D1E11.A1,D1E11.B1,0.001,abs(min(D1E11.V1)),kan);
[D1E11.E5(:,2),D1E11.E5(:,1)]=jikansa(D1E11.T5,-D1E11.V5,-D1E11.A5,D1E11.B5,0.001,abs(max(D1E11.V5)),kan);
figure(3)
% subplot(2,2,3)
plot(D1E11.E1(:,2),D1E11.E1(:,1),D1E11.E5(:,2),D1E11.E5(:,1))
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)

D=1E12;
[S1,D1E12.A1,D1E12.B1]=Ideal_idou_ID([D1E12.T1,D1E12.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,D1E12.A5,D1E12.B5]=Ideal_idou_ID([D1E12.T5,D1E12.I5],Ti+baisuu*tau_v,D,Ten,5);
[D1E12.E1(:,2),D1E12.E1(:,1)]=jikansa(D1E12.T1,D1E12.V1,D1E12.A1,D1E12.B1,0.001,abs(min(D1E12.V1)),kan);
[D1E12.E5(:,2),D1E12.E5(:,1)]=jikansa(D1E12.T5,-D1E12.V5,-D1E12.A5,D1E12.B5,0.001,abs(max(D1E12.V5)),kan);
figure(4)
% subplot(2,2,4)
plot(D1E12.E1(:,2),D1E12.E1(:,1),D1E12.E5(:,2),D1E12.E5(:,1))
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)

%}
%{
D=1E11;
[S1,T1500.A1,T1500.B1]=Ideal_idou_ID([T1500.T1,T1500.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,T1500.A5,T1500.B5]=Ideal_idou_ID([T1500.T5,T1500.I5],Ti+baisuu*tau_v,D,Ten,5);
[T1500.E1(:,2),T1500.E1(:,1)]=jikansa(T1500.T1,T1500.V1,T1500.A1,T1500.B1,0.001,abs(min(T1500.V1)),kan);
[T1500.E5(:,2),T1500.E5(:,1)]=jikansa(T1500.T5,-T1500.V5,-T1500.A5,T1500.B5,0.001,abs(max(T1500.V5)),kan);
figure(5)
% subplot(2,2,4)
plot(T1500.E1(:,2),T1500.E1(:,1),T1500.E5(:,2),T1500.E5(:,1))
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)
%}
D=1E11;
%%%%%% [~,T2500.A1,T2500.B1]=Ideal_idou_ID_HiDen( [T2500.T1,T2500.I1] ,Ti ,baisuu*tau_v ,Step ,64 , times*tau_c, D, Ten, 1);
%%%%%%[~,T2500.A5,T2500.B5]=Ideal_idou_ID_HiDen([T2500.T5,T2500.I5] ,Ti ,baisuu*tau_v , Step ,64 ,times*tau_c, D, Ten, 5);
%[T2500.E1(:,2),T2500.E1(:,1)]=jikansa( Ti, T2500.T1, T2500.V1, T2500.A1, T2500.B1, 0.001, abs(min(T2500.V1)), kan);
%[T2500.E5(:,2),T2500.E5(:,1)]=jikansa(Ti, T2500.T5, -T2500.V5, -T2500.A5, T2500.B5, 0.001, abs(max(T2500.V5)), kan);
%figure(6)
% subplot(2,2,4)
%plot(T2500.E1(:,2),T2500.E1(:,1),T2500.E5(:,2),T2500.E5(:,1))
cont=1;
for i=1:5
    [tt,T2500.A1,T2500.B1]=Ideal_idou_ID_HiDen( [T2500.T1,T2500.I1] ,Ti ,baisuu*tau_v ,Step ,64 , times*tau_c, D, Ten, 1);
%    [~,T2500.A5,T2500.B5]=Ideal_idou_ID_HiDen([T2500.T5,T2500.I5] ,Ti ,baisuu*tau_v , Step ,64 ,times*tau_c, D, Ten, 5);
    Step=Step*10;
    AB_value(cont,1)=tt(1);
    AB_value(cont,2)=tt(2);
    cont= cont+1;
end
figure();
t=0.001:0.001:5;
plot(t,T2500.A1*exp(T2500.B1*t));
figure();
plot(T2500.E1(:,2),T2500.E1(:,1))

xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Positive Voltage','Negative Voltage','Location','southwest')
set(gca,'FontSize',12)


% figure(3)

% subplot(2,2,1)
% hold on
% plot(E1s(1,:,2),E1s(1,:,1),E1s(2,:,2),E1s(2,:,1),E1s(3,:,2),E1s(3,:,1),E1s(4,:,2),E1s(4,:,1),E1s(5,:,2),E1s(5,:,1))
% plot(E5s(1,:,2),E5s(1,:,1),E5s(2,:,2),E5s(2,:,1),E5s(3,:,2),E5s(3,:,1),E5s(4,:,2),E5s(4,:,1),E5s(5,:,2),E5s(5,:,1))
% xlim([0 0.1])
% xlabel('\fontsize{12}容許電壓(V)');ylabel('\fontsize{12}時間差(s)');
% legend('起始延遲5\tau_{v}','起始延遲6\tau_{v}','起始延遲7\tau_{v}','起始延遲8\tau_{v}','起始延遲9\tau_{v}','Location','southeast')
% set(gca,'FontSize',12)
% grid on

% subplot(2,2,1)
% hold on
% plot(E1s(1,:,2),E1s(1,:,1))
% plot(E5s(1,:,2),E5s(1,:,1))
% % xlim([0 0.1])
% xlabel('\fontsize{12}容許電壓(V)');ylabel('\fontsize{12}時間差(s)');
% set(gca,'FontSize',12)
% grid on


% 濃度估錯的誤差
% 改濃度誤差的幅度
% 改溫度誤差的幅度
% 雜訊，取樣延遲，影響
% 雜訊，移動平均組數，影響


% subplot(2,2,2)
% hold on
% plot(Real.T1,Real.Vc1,Real.T5,Real.Vc5)
% t=2:0.001:5;
% ke1=A1(1)*exp(B1(1)*t);
% ke5=A5(1)*exp(B5(1)*t);
% plot(t,-ke1,'r--')
% plot(t,ke5,'r--')
% plot(S1(:,1),S1(:,3),'rx')
% plot(S5(:,1),S5(:,3),'rx')
% 
% subplot(2,2,3)
% hold on
% plot(Real.T1,Real.I1,Real.T5,Real.I5)
% plot(S1(:,1),S1(:,2),'rx')
% plot(S5(:,1),S5(:,2),'rx')
% 
% subplot(2,2,4)
% hold on
% plot(Real.T1,Real.Vs1,Real.T5,Real.Vs5)


