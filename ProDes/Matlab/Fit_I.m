% 輸入電流，用吻合法抓出理想脈波電流值 %
function [V,I]=Fit_I(Tin,Vin,Iin,number,starttime,startindex,delay,interval,setnumber)
    x=1;
    for i=1:number
        if Vin(i)==0
            I(i)=0;
        else
            [~,~,~,~,I(i)]=ronbun_mean_ID([Tin,Iin],starttime(x),starttime(x)+delay,interval,15E-6,setnumber,startindex(x));
            x=x+1;
        end
    end
    V=Vin;
end



