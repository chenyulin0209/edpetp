% text program
clc;clear;close all;
% path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\充電及放電\定便因_new\';
% content='掃描電壓_內插專用';
% [T2500.T1,T2500.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);


Apr = 58.2E-4;
Ar = 6;
beta1= 0;
beta2= 0;
Te= 2500;
Ne= 1E11;

count=1;
for i=-5:0.00001:1
    Guess.IV(count,2)=i;
count=count+1;
end

[-Guess.IV(:,1),~]=FDDSI_Vcurve(Guess.IV(:,2),Apr,Ar,beta1,beta2,Te,Ne);
save('ideal_IV_table_2.mat'); 

