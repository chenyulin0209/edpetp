% 脈波閒置時間最佳化 第二分項實驗 改酬載電阻 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電阻\80k\';
content='掃描電壓';
[ps80k.T1,ps80k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps80k.T5,ps80k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps80k.T1,ps80k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps80k.T5,ps80k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps80k.T1,ps80k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps80k.T5,ps80k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電阻\160k\';
content='掃描電壓';
[ps160k.T1,ps160k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps160k.T5,ps160k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps160k.T1,ps160k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps160k.T5,ps160k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps160k.T1,ps160k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps160k.T5,ps160k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電阻\320k\';
content='掃描電壓';
[ps320k.T1,ps320k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps320k.T5,ps320k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps320k.T1,ps320k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps320k.T5,ps320k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps320k.T1,ps320k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps320k.T5,ps320k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

st=2.01;
ov=4.9;

[ps80k.IV1(:,1),ps80k.IV1(:,2)]=Ideal_IV_make(ps80k.T1,ps80k.V1,ps80k.I1,st,ov);
[ps80k.IV5(:,1),ps80k.IV5(:,2)]=Ideal_IV_make(ps80k.T5,ps80k.V5,ps80k.I5,st,ov);
[ps80k.IV]=IV_mix(ps80k.IV1,ps80k.IV5);
figure(1)
subplot(2,2,1)
plot(ps80k.T1,ps80k.Vs2,ps80k.T5,ps80k.Vs5)
subplot(2,2,2)
plot(ps80k.T1,ps80k.I1,ps80k.T5,ps80k.I5)
subplot(2,2,3)
plot(ps80k.T1,ps80k.V1,ps80k.T5,ps80k.V5)
subplot(2,2,4)
plot(ps80k.IV1(:,1),ps80k.IV1(:,2),ps80k.IV5(:,1),ps80k.IV5(:,2))

[ps160k.IV1(:,1),ps160k.IV1(:,2)]=Ideal_IV_make(ps160k.T1,ps160k.V1,ps160k.I1,st,ov);
[ps160k.IV5(:,1),ps160k.IV5(:,2)]=Ideal_IV_make(ps160k.T5,ps160k.V5,ps160k.I5,st,ov);
[ps160k.IV]=IV_mix(ps160k.IV1,ps160k.IV5);
figure(2)
subplot(2,2,1)
plot(ps160k.T1,ps160k.Vs2,ps160k.T5,ps160k.Vs5)
subplot(2,2,2)
plot(ps160k.T1,ps160k.I1,ps160k.T5,ps160k.I5)
subplot(2,2,3)
plot(ps160k.T1,ps160k.V1,ps160k.T5,ps160k.V5)
subplot(2,2,4)
plot(ps160k.IV1(:,1),ps160k.IV1(:,2),ps160k.IV5(:,1),ps160k.IV5(:,2))

[ps320k.IV1(:,1),ps320k.IV1(:,2)]=Ideal_IV_make(ps320k.T1,ps320k.V1,ps320k.I1,st,ov);
[ps320k.IV5(:,1),ps320k.IV5(:,2)]=Ideal_IV_make(ps320k.T5,ps320k.V5,ps320k.I5,st,ov);
[ps320k.IV]=IV_mix(ps320k.IV1,ps320k.IV5);
figure(3)
subplot(2,2,1)
plot(ps320k.T1,ps320k.Vs2,ps320k.T5,ps320k.Vs5)
subplot(2,2,2)
plot(ps320k.T1,ps320k.I1,ps320k.T5,ps320k.I5)
subplot(2,2,3)
plot(ps320k.T1,ps320k.V1,ps320k.T5,ps320k.V5)
subplot(2,2,4)
plot(ps320k.IV1(:,1),ps320k.IV1(:,2),ps320k.IV5(:,1),ps320k.IV5(:,2))

figure(4)
% subplot(2,2,1)
grid on
hold on
plot(ps320k.IV(:,1),-ps320k.IV(:,2),ps160k.IV(:,1),-ps160k.IV(:,2),ps80k.IV(:,1),-ps80k.IV(:,2))
ylim([-0.5E-6 3E-6])
% title('\fontsize{12}不同酬載汙染層電阻下的IV曲線');
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Contamination Resistance of Satellite 320k','Contamination Resistance of Satellite 160k','Contamination Resistance of Satellite 80k','Location','northwest')
set(gca,'FontSize',12)

figure()
subplot(2,2,2)
plot(ps80k.IV1(:,1),ps80k.IV1(:,2),ps80k.IV5(:,1),ps80k.IV5(:,2))
subplot(2,2,3)
plot(ps160k.IV1(:,1),ps160k.IV1(:,2),ps160k.IV5(:,1),ps160k.IV5(:,2))
subplot(2,2,4)
plot(ps320k.IV1(:,1),ps320k.IV1(:,2),ps320k.IV5(:,1),ps320k.IV5(:,2))


figure(6)
subplot(2,2,1)
plot(ps80k.T1,ps80k.V1,ps160k.T1,ps160k.V1,ps320k.T1,ps320k.V1)
subplot(2,2,3)
plot(ps80k.T5,ps80k.V5,ps160k.T5,ps160k.V5,ps320k.T5,ps320k.V5)



