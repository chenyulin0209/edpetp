% 脈波閒置時間最佳化 第二分項實驗 改電子濃度 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E9\充電及放電\定變因\';
content='掃描電壓';
[D1E9.T1,D1E9.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E9.T5,D1E9.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E9.T1,D1E9.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E9.T5,D1E9.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E9.T1,D1E9.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E9.T5,D1E9.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E10\充電及放電\定變因\';
content='掃描電壓';
[D1E10.T1,D1E10.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E10.T5,D1E10.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E10.T1,D1E10.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E10.T5,D1E10.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E10.T1,D1E10.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E10.T5,D1E10.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因\';
content='掃描電壓';
[D1E11.T1,D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E11.T1,D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E11.T1,D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E12\充電及放電\定變因\';
content='掃描電壓';
[D1E12.T1,D1E12.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E12.T5,D1E12.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E12.T1,D1E12.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E12.T5,D1E12.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E12.T1,D1E12.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E12.T5,D1E12.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

st=2.01;
ov=4.9;

[D1E9.IV1(:,1),D1E9.IV1(:,2)]=Ideal_IV_make(D1E9.T1,D1E9.V1,D1E9.I1,st,ov);
[D1E9.IV5(:,1),D1E9.IV5(:,2)]=Ideal_IV_make(D1E9.T5,D1E9.V5,D1E9.I5,st,ov);
[D1E9.IV]=IV_mix(D1E9.IV1,D1E9.IV5);
figure(1)
subplot(2,2,1)
plot(D1E9.T1,D1E9.Vs1,D1E9.T5,D1E9.Vs5)
subplot(2,2,2)
plot(D1E9.T1,D1E9.I1,D1E9.T5,D1E9.I5)
subplot(2,2,3)
plot(D1E9.T1,D1E9.V1,D1E9.T5,D1E9.V5)
subplot(2,2,4)
plot(D1E9.IV1(:,1),D1E9.IV1(:,2),D1E9.IV5(:,1),D1E9.IV5(:,2))

[D1E10.IV1(:,1),D1E10.IV1(:,2)]=Ideal_IV_make(D1E10.T1,D1E10.V1,D1E10.I1,st,ov);
[D1E10.IV5(:,1),D1E10.IV5(:,2)]=Ideal_IV_make(D1E10.T5,D1E10.V5,D1E10.I5,st,ov);
[D1E10.IV]=IV_mix(D1E10.IV1,D1E10.IV5);
figure(2)
subplot(2,2,1)
plot(D1E10.T1,D1E10.Vs1,D1E10.T5,D1E10.Vs5)
subplot(2,2,2)
plot(D1E10.T1,D1E10.I1,D1E10.T5,D1E10.I5)
subplot(2,2,3)
plot(D1E10.T1,D1E10.V1,D1E10.T5,D1E10.V5)
subplot(2,2,4)
plot(D1E10.IV1(:,1),D1E10.IV1(:,2),D1E10.IV5(:,1),D1E10.IV5(:,2))

[D1E11.IV1(:,1),D1E11.IV1(:,2)]=Ideal_IV_make(D1E11.T1,D1E11.V1,D1E11.I1,st,ov);
[D1E11.IV5(:,1),D1E11.IV5(:,2)]=Ideal_IV_make(D1E11.T5,D1E11.V5,D1E11.I5,st,ov);
[D1E11.IV]=IV_mix(D1E11.IV1,D1E11.IV5);
figure(3)
subplot(2,2,1)
plot(D1E11.T1,D1E11.Vs1,D1E11.T5,D1E11.Vs5)
subplot(2,2,2)
plot(D1E11.T1,D1E11.I1,D1E11.T5,D1E11.I5)
subplot(2,2,3)
plot(D1E11.T1,D1E11.V1,D1E11.T5,D1E11.V5)
subplot(2,2,4)
plot(D1E11.IV1(:,1),D1E11.IV1(:,2),D1E11.IV5(:,1),D1E11.IV5(:,2))

[D1E12.IV1(:,1),D1E12.IV1(:,2)]=Ideal_IV_make(D1E12.T1,D1E12.V1,D1E12.I1,st,ov);
[D1E12.IV5(:,1),D1E12.IV5(:,2)]=Ideal_IV_make(D1E12.T5,D1E12.V5,D1E12.I5,st,ov);
[D1E12.IV]=IV_mix(D1E12.IV1,D1E12.IV5);
figure(4)
subplot(2,2,1)
plot(D1E12.T1,D1E12.Vs1,D1E12.T5,D1E12.Vs5)
subplot(2,2,2)
plot(D1E12.T1,D1E12.I1,D1E12.T5,D1E12.I5)
subplot(2,2,3)
plot(D1E12.T1,D1E12.V1,D1E12.T5,D1E12.V5)
subplot(2,2,4)
plot(D1E12.IV1(:,1),D1E12.IV1(:,2),D1E12.IV5(:,1),D1E12.IV5(:,2))

figure(5)
% subplot(2,2,1)
grid on
hold on
plot(D1E12.IV(:,1),-D1E12.IV(:,2),D1E11.IV(:,1),-D1E11.IV(:,2),D1E10.IV(:,1),-D1E10.IV(:,2),D1E9.IV(:,1),-D1E9.IV(:,2))
ylim([-1E-5 6E-5])
% plot([0 0],[-3 2],'k')
% plot([-5*10^(-5) 10^(-5)],[0 0],'k')
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Electron Density 10^6 cm^{-3}','Electron Density 10^5 cm^{-3}','Electron Density 10^4 cm^{-3}','Electron Density 10^3 cm^{-3}','Location','northwest')
set(gca,'FontSize',12)

figure(6)
% subplot(2,2,2)
grid on
hold on
plot(D1E12.IV(:,1),-D1E12.IV(:,2),D1E11.IV(:,1),-D1E11.IV(:,2)*10,D1E10.IV(:,1),-D1E10.IV(:,2)*100,D1E9.IV(:,1),-D1E9.IV(:,2)*1000)
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Electron Density 10^6 cm^{-3}','Electron Density 10^5 cm^{-3}(Current x 10)','Electron Density 10^4 cm^{-3}(Current x 100)','Electron Density 10^3 cm^{-3}(Current x 1000)','Location','northwest')
set(gca,'FontSize',12)


path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

figure(7)
% subplot(2,2,3)
grid on
hold on
plot(D1E12.IV(:,1),-D1E12.IV(:,2),D1E11.IV(:,1),-D1E11.IV(:,2)*10,D1E10.IV(:,1),-D1E10.IV(:,2)*100,D1E9.IV(:,1),-D1E9.IV(:,2)*1000)
ylim([-1E-5 8E-5])
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Electron Density 10^6 cm^{-3}','Electron Density 10^5 cm^{-3}(Current x 10)','Electron Density 10^4 cm^{-3}(Current x 100)','Electron Density 10^3 cm^{-3}(Current x 1000)','Location','northwest')
set(gca,'FontSize',12)
figure(8)
subplot(2,2,4)
plot(D1E9.IV(:,1),-D1E9.IV(:,2))
title('\fontsize{12}不同電子溫度下的IV曲線');xlabel('\fontsize{12}迴路電流(A)');ylabel('\fontsize{12}汙染層跨壓(V)');
legend('1E9','Location','northwest')
set(gca,'FontSize',12)

figure(9)
subplot(2,2,1)
plot(D1E12.T1,D1E12.V1,D1E11.T1,D1E11.V1,D1E10.T1,D1E10.V1,D1E9.T1,D1E9.V1)
% plot(D1E10.T1,D1E10.V1,D1E9.T1,D1E9.V1)




