%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
將取樣區間做正規化，看取樣區間要怎麼取比較理想
細節：
這裡的橫軸是取樣週期，要注意這個問題(詳細說明在samp_proc_effect的細節)
%}
clc;clear;close all;

clc;clear;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;

% 理想情況下，脈衝振幅相對應應該量測到的電流值
%      p10      n10     n30      n50
Ireal=[-19.0513,4.08347,8.69623,13.3189]*10^(-7);
%       p08       p04      p02     
Ireal2=[-1.6004,-0.76772,-0.30994]*10^(-6);

Ireal_seco=[-5.83]*10^(-7);
% 各電壓相對應電流矩陣數
%             1.0   0.8      0.4     0.2     -1.0  -3.0   -5.0
array_count=[685601, 500494 ,500479, 500479, 57260, 57464, 57705 ];
    
    
    
% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
[VC_500.p08.T,VC_500.p08.I]=textread([path '0.8V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.p04.T,VC_500.p04.I]=textread([path '0.4V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.p02.T,VC_500.p02.I]=textread([path '0.2V\' content '.txt'],'%f %f','headerlines',1);

[VC_500.n10.T,VC_500.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n30.T,VC_500.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前一個實驗知道-0.8倍最理想
interval=0.2*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 快速取樣的取樣週期
rapid_S=15*10^(-6);
% 以指數吻合法，在不同取樣延遲下，將6種脈衝振幅都做一次

% 執行次數，越多次平均誤差跟標準差會漸趨穩定
times=1000;


for j=1:times
    count=1;
    for i=[1 2 3 4 5 6 7 8 9 10]
        %        加入雜訊，雜訊大小是用最大電流的%數決定
        gosa=10;
        
%         E_rc.err.Start(x,1)=i;
%         多組實驗 1.0V
        Inoise=VC_500.p10.I+rand(array_count(1),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_p10(j,count),~,~,~,~,~]=mean_I_ID([VC_500.p10.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal(1));
%         多組實驗 0.8V
		Inoise=VC_500.p08.I+rand(array_count(2),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_p08(j,count),~,~,~,~,~]=mean_I_ID([VC_500.p08.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal2(1));
%         多組實驗 0.4V
		Inoise=VC_500.p04.I+rand(array_count(3),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_p04(j,count),~,~,~,~,~]=mean_I_ID([VC_500.p04.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal2(2));
%         多組實驗 0.2V
		Inoise=VC_500.p02.I+rand(array_count(4),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
		[E32rc_p02(j,count),~,~,~,~,~]=mean_I_ID([VC_500.p02.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal2(3));
%         多組實驗 -1.0V
		Inoise=VC_500.n10.I+rand(array_count(5),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_n10(j,count),~,~,~,~,~]=mean_I_ID([VC_500.n10.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal(2));
%         多組實驗 -3.0V
		Inoise=VC_500.n30.I+rand(array_count(6),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_n30(j,count),~,~,~,~,~]=mean_I_ID([VC_500.n30.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal(3));
%         多組實驗 -5.0V
		Inoise=VC_500.n50.I+rand(array_count(7),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
        [E32rc_n50(j,count),~,~,~,~,~]=mean_I_ID([VC_500.n50.T,Inoise], tau_v*i, interval, rapid_S, 96, Ireal(4));
  
        count=count+1;
    end
end
x=1;
for i=[1 2 3 4 5 6 7 8 9 10]
%     32組實驗的結果，包含電流間常數的估算值 1.0V	
        E32_p10(x,1)=i;
        E32_p10(x,2)=mean(E32rc_p10(:,x));
        E32_p10(x,3)=std(E32rc_p10(:,x)); 
%     32組實驗的結果，包含電流間常數的估算值 0.8V	
        E32_p08(x,1)=i;
        E32_p08(x,2)=mean(E32rc_p08(:,x));
        E32_p08(x,3)=std(E32rc_p08(:,x)); 
%     32組實驗的結果，包含電流間常數的估算值 0.4V	
        E32_p04(x,1)=i;
        E32_p04(x,2)=mean(E32rc_p04(:,x));
        E32_p04(x,3)=std(E32rc_p04(:,x));

%     32組實驗的結果，包含電流間常數的估算值 0.2V	
        E32_p02(x,1)=i;
        E32_p02(x,2)=mean(E32rc_p02(:,x));
        E32_p02(x,3)=std(E32rc_p02(:,x));  
%     32組實驗的結果，包含電流間常數的估算值 -1.0V	
        E32_n10(x,1)=i;
        E32_n10(x,2)=mean(E32rc_n10(:,x));
        E32_n10(x,3)=std(E32rc_n10(:,x)); 
%     32組實驗的結果，包含電流間常數的估算值 -3.0V	
        E32_n30(x,1)=i;
        E32_n30(x,2)=mean(E32rc_n30(:,x));
        E32_n30(x,3)=std(E32rc_n30(:,x)); 
%     32組實驗的結果，包含電流間常數的估算值 -5.0V	
        E32_n50(x,1)=i;
        E32_n50(x,2)=mean(E32rc_n50(:,x));
        E32_n50(x,3)=std(E32rc_n50(:,x)); 
        x=x+1;  
end
hold on ;
errorbar(E32_p10(:,1),-E32_p10(:,2),E32_p10(:,3));
errorbar(E32_p08(:,1),-E32_p08(:,2),E32_p08(:,3));
errorbar(E32_p04(:,1),-E32_p04(:,2),E32_p04(:,3));
errorbar(E32_p02(:,1),-E32_p02(:,2),E32_p02(:,3));
errorbar(E32_n10(:,1),-E32_n10(:,2),E32_n10(:,3));
errorbar(E32_n30(:,1),-E32_n30(:,2),E32_n30(:,3));
errorbar(E32_n50(:,1),-E32_n50(:,2),E32_n50(:,3));


legend('1.0V','0.8V','0.4V','0.2V','-1.0V','-3.0V','-5.0V','location','northwest')

save err_to_StartSample181004.mat

