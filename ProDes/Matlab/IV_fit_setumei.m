% 最低解析度，確認所有溫濃皆適用 %

clc;clear;close all;

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\漸升式三角波\';

DT='離4過4\';
content='掃描電壓';
[wave.T2000D1E11N8.T,wave.T2000D1E11N8.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.T2000D1E11N8.T,wave.T2000D1E11N8.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.T2000D1E11N8.T,wave.T2000D1E11N8.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
DT='離2過2\';
content='掃描電壓';
[wave.T2000D1E11N4.T,wave.T2000D1E11N4.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.T2000D1E11N4.T,wave.T2000D1E11N4.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.T2000D1E11N4.T,wave.T2000D1E11N4.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);


% save IV_fit_setumei_data perfect wave
load IV_fit_setumei_data

disp(['數據讀取完畢'])

figure()
plot(wave.T2000D1E11N8.T,wave.T2000D1E11N8.Vs)
figure()
plot(wave.T2000D1E11N4.T,wave.T2000D1E11N4.Vs)
figure()

[wave.T2000D1E11N4.RI,wave.T2000D1E11N4.RT,wave.T2000D1E11N4.amp,wave.T2000D1E11N4.NtoP,wave.T2000D1E11N4.PN]=FindPulseStart(wave.T2000D1E11N4.T,wave.T2000D1E11N4.Vs);
[wave.T2000D1E11N8.RI,wave.T2000D1E11N8.RT,wave.T2000D1E11N8.amp,wave.T2000D1E11N8.NtoP,wave.T2000D1E11N8.PN]=FindPulseStart(wave.T2000D1E11N8.T,wave.T2000D1E11N8.Vs);

tau_c=0.02;
tau_v=0.001;
baisuu=7;
interval=0.4*tau_c;
x=1;

g100=5.92E-10;
g100=0;
% gosa=0;
kai=1;

for j=1:kai
    wave.T2000D1E11N4.Inoise=wave.T2000D1E11N4.I+rand(size(wave.T2000D1E11N4.I,1),1)*g100*2-g100;
    wave.T2000D1E11N8.Inoise=wave.T2000D1E11N8.I+rand(size(wave.T2000D1E11N8.I,1),1)*g100*2-g100;

    dot=48;
%     tau_c=0.1722;
    tau_c=0.17;
    [wave.T2000D1E11N4.IV_rc(1,:),wave.T2000D1E11N4.IV_rc(2,:)]=Fit_I(wave.T2000D1E11N4.T,wave.T2000D1E11N4.amp,wave.T2000D1E11N4.Inoise,wave.T2000D1E11N4.PN,wave.T2000D1E11N4.RT,wave.T2000D1E11N4.RI,baisuu*tau_v,0.4*tau_c,dot);
    [wave.T2000D1E11N8.IV_rc(1,:),wave.T2000D1E11N8.IV_rc(2,:)]=Fit_I(wave.T2000D1E11N8.T,wave.T2000D1E11N8.amp,wave.T2000D1E11N8.Inoise,wave.T2000D1E11N8.PN,wave.T2000D1E11N8.RT,wave.T2000D1E11N8.RI,baisuu*tau_v,0.4*tau_c,dot);

    [T.T2000D1E11N4.rc(j),D.T2000D1E11N4.rc(j),Vrc4,Ierc4,Iirc4]=ronbunTD(wave.T2000D1E11N4.IV_rc(1,:),wave.T2000D1E11N4.IV_rc(2,:));    
    [T.T2000D1E11N8.rc(j),D.T2000D1E11N8.rc(j),Vrc8,Ierc8,Iirc8]=ronbunTD(wave.T2000D1E11N8.IV_rc(1,:),wave.T2000D1E11N8.IV_rc(2,:));    

    disp(['執行次數  ' num2str(j) '/' num2str(kai) '     進度' num2str(j/kai*100) '%' ])
end

% disp(['512點'])
% disp(['T2000D1E11 溫度' num2str(T.T2000D1E11.rc) '   濃度' num2str(D.T2000D1E11.rc) ])



% aVpl=FIVcurveFindVpl(1,2000);
% Vfix=FDDmodVdcal(perfect.Vs(1:25722)',aVpl,1,2000,6);
% 
% figure()
% subplot(2,2,1)
% plot(wave.T2000D1E11N4.T,wave.T2000D1E11N4.Inoise)
% subplot(2,2,2)
% plot(wave.T2000D1E11N8.T,wave.T2000D1E11N8.Inoise)

Vi=-5:0.01:0.5;

Y=[-Iirc4(1) -Iirc4(2)]';
A=[Vrc4(1),1;Vrc4(2),1];
hh=inv(A'*A)*A'*Y;
ID_result=real(hh);
A4=real(hh(1));
B4=real(hh(2));
YIi4=A4*Vi+B4;

Ve=-1:0.01:0.5;

Y=[log(-0.0184E-5) log(-0.1122E-5) log(-0.1992E-5)]';
A=[1,Vrc4(3);1,Vrc4(4);1,Vrc4(5)];
hh=inv(A'*A)*A'*Y;
ID_result=real(hh);
A4=exp(real(hh(1)));
B4=(real(hh(2)));
YIe4=A4*exp(Ve*B4);
% 
% figure()
% % subplot(2,2,3)
% hold on
% plot(Vfix(1:25722),-perfect.I(1:25722),V,YIe4+YIi4,'g--',V,YIi4,'r--')
% plot(Vrc4(3:5),-Iirc4(3:5)-Ierc4(3:5),'go')
% plot(Vrc4(1:2),-Iirc4(1:2),'ro')
% % plot(V,YIe4+YIi4,'gx');
% % plot(V,YIi4,'rx');
% xlabel('\fontsize{12}Probe Voltage(V)');ylabel('\fontsize{12}Current(A)');
% % legend('','')
% set(gca,'FontSize',12)
% grid on
% 
% figure()
% subplot(2,2,4)
% hold on
% plot(Vfix(1:25722),-perfect.I(1:25722))
% plot(Vrc4,-Iirc4-Ierc4,'ro')


subplot(4,2,2)
plot(wave.T2000D1E11N4.IV_rc(1,:),wave.T2000D1E11N4.IV_rc(2,:),perfect.Vs,perfect.I)

[Vpl]=FIVcurveFindVpl(1,2000)
Vss=-5:0.01:0.41;
[Iee,Iii,III]=DebyeCurrentSim(Vss,Vpl,1E11,1E11,2000,58.2E-4,1);

figure()
% subplot(4,2,1)
hold on
plot(Vss,-III,'b',Vi,YIi4,'r--',Vrc4(1:2),-Ierc4(1:2)-Iirc4(1:2),'ro')
% plot()
xlim([-6 2])
xlabel('\fontsize{12}Probe Voltage(V)');ylabel('\fontsize{12}Probe Current(A)');
legend('I-V curve','fitting line','location','northwest')
set(gca,'FontSize',12)
grid on

figure()
% subplot(4,2,5)
hold on
plot(Vss,-Iee,'b',Ve,YIe4,'r--',Vrc4(3:5),-Ierc4(3:5),'ro')
% plot()
% title('電子流')
xlabel('\fontsize{12}Probe Voltage(V)');ylabel('\fontsize{12}Electron Current(A)');
legend('I-V curve','fitting line','location','northwest')
xlim([-6 2])
set(gca,'FontSize',12)
grid on








