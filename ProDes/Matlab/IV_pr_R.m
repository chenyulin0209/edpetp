% 脈波閒置時間最佳化 第二分項實驗 改探針電阻 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電阻\500k\';
content='掃描電壓';
[pr500k.T1,pr500k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr500k.T5,pr500k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr500k.T1,pr500k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr500k.T5,pr500k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr500k.T1,pr500k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr500k.T5,pr500k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電阻\1000k\';
content='掃描電壓';
[pr1000k.T1,pr1000k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr1000k.T5,pr1000k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr1000k.T1,pr1000k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr1000k.T5,pr1000k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr1000k.T1,pr1000k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr1000k.T5,pr1000k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電阻\2000k\';
content='掃描電壓';
[pr2000k.T1,pr2000k.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr2000k.T5,pr2000k.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr2000k.T1,pr2000k.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr2000k.T5,pr2000k.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr2000k.T1,pr2000k.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr2000k.T5,pr2000k.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

st=2.01;
ov=4.9;

[pr500k.IV1(:,1),pr500k.IV1(:,2)]=Ideal_IV_make(pr500k.T1,pr500k.V1,pr500k.I1,st,ov);
[pr500k.IV5(:,1),pr500k.IV5(:,2)]=Ideal_IV_make(pr500k.T5,pr500k.V5,pr500k.I5,st,ov);
[pr500k.IV]=IV_mix(pr500k.IV1,pr500k.IV5);
figure(1)
subplot(2,2,1)
plot(pr500k.T1,pr500k.Vs2,pr500k.T5,pr500k.Vs5)
subplot(2,2,2)
plot(pr500k.T1,pr500k.I1,pr500k.T5,pr500k.I5)
subplot(2,2,3)
plot(pr500k.T1,pr500k.V1,pr500k.T5,pr500k.V5)
subplot(2,2,4)
plot(pr500k.IV1(:,1),pr500k.IV1(:,2),pr500k.IV5(:,1),pr500k.IV5(:,2))

[pr1000k.IV1(:,1),pr1000k.IV1(:,2)]=Ideal_IV_make(pr1000k.T1,pr1000k.V1,pr1000k.I1,st,ov);
[pr1000k.IV5(:,1),pr1000k.IV5(:,2)]=Ideal_IV_make(pr1000k.T5,pr1000k.V5,pr1000k.I5,st,ov);
[pr1000k.IV]=IV_mix(pr1000k.IV1,pr1000k.IV5);
figure(2)
subplot(2,2,1)
plot(pr1000k.T1,pr1000k.Vs2,pr1000k.T5,pr1000k.Vs5)
subplot(2,2,2)
plot(pr1000k.T1,pr1000k.I1,pr1000k.T5,pr1000k.I5)
subplot(2,2,3)
plot(pr1000k.T1,pr1000k.V1,pr1000k.T5,pr1000k.V5)
subplot(2,2,4)
plot(pr1000k.IV1(:,1),pr1000k.IV1(:,2),pr1000k.IV5(:,1),pr1000k.IV5(:,2))

[pr2000k.IV1(:,1),pr2000k.IV1(:,2)]=Ideal_IV_make(pr2000k.T1,pr2000k.V1,pr2000k.I1,st,ov);
[pr2000k.IV5(:,1),pr2000k.IV5(:,2)]=Ideal_IV_make(pr2000k.T5,pr2000k.V5,pr2000k.I5,st,ov);
[pr2000k.IV]=IV_mix(pr2000k.IV1,pr2000k.IV5);
figure(3)
subplot(2,2,1)
plot(pr2000k.T1,pr2000k.Vs2,pr2000k.T5,pr2000k.Vs5)
subplot(2,2,2)
plot(pr2000k.T1,pr2000k.I1,pr2000k.T5,pr2000k.I5)
subplot(2,2,3)
plot(pr2000k.T1,pr2000k.V1,pr2000k.T5,pr2000k.V5)
subplot(2,2,4)
plot(pr2000k.IV1(:,1),pr2000k.IV1(:,2),pr2000k.IV5(:,1),pr2000k.IV5(:,2))

figure(4)
% subplot(2,2,1)
grid on
hold on
plot(pr2000k.IV(:,1),-pr2000k.IV(:,2),pr1000k.IV(:,1),-pr1000k.IV(:,2),pr500k.IV(:,1),-pr500k.IV(:,2))
% title('\fontsize{12}不同探針汙染層電阻下的IV曲線');
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Contamination Resistance of Probe 2000k','Contamination Resistance of Probe 1000k','Contamination Resistance of Probe 500k','Location','northwest')
set(gca,'FontSize',12)


figure()
subplot(2,2,2)
plot(pr500k.IV1(:,1),pr500k.IV1(:,2),pr500k.IV5(:,1),pr500k.IV5(:,2))
subplot(2,2,3)
plot(pr1000k.IV1(:,1),pr1000k.IV1(:,2),pr1000k.IV5(:,1),pr1000k.IV5(:,2))
subplot(2,2,4)
plot(pr2000k.IV1(:,1),pr2000k.IV1(:,2),pr2000k.IV5(:,1),pr2000k.IV5(:,2))



path='C:\Users\eric\Desktop\工作用資料夾\ETPEDP\論文模擬\脈波閒置時間最佳化beta1\最大電流研究\';
content='掃描電壓';
[T1,V1]=textread([path 'IV是無汙染IV嗎\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T1,I1]=textread([path 'IV是無汙染IV嗎\' content '.txt'],'%f %f','headerlines',1);

figure(5)
subplot(2,2,1)
hold on
grid on
plot(V1,-I1,pr2000k.IV1(:,2),-pr2000k.IV1(:,1),'rx',pr2000k.IV5(:,2),-pr2000k.IV5(:,1),'rx')
xlim([-5 5])
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('無汙染層IV曲線','迴路電流對應汙染總跨壓曲線','location','northwest')
set(gca,'FontSize',12)

figure(6)
subplot(2,2,1)
plot(pr500k.T1,pr500k.I1,pr1000k.T1,pr1000k.I1,pr2000k.T1,pr2000k.I1)
subplot(2,2,2)
plot(pr500k.T5,pr500k.I5,pr1000k.T5,pr1000k.I5,pr2000k.T5,pr2000k.I5)





