% 找出脈衝激發(前一點是0，後一點不是0)的，編序值，時間點，激發電壓大小，第幾個電壓負轉正，脈波總數 %
function [index,timing,amplitude,indexNtoP,pulsenumber]=FindPulseStart(T,Vs)
    x=1;
    for i=2:size(Vs,1)
        if Vs(i)~=0 && Vs(i-1)==0
            index(x)=i;
            timing(x)=T(index(x));
            [~,amplitude_temp(x),~]=T_find_D(T,Vs,timing(x)+0.12);
            x=x+1;
        end
    end
    pulsenumber_temp=size(amplitude_temp,2);
    for i=2:pulsenumber_temp
        if amplitude_temp(i)>0 && amplitude_temp(i-1)<0
            indexNtoP=i-1;
        end
    end
    amplitude(1:indexNtoP)=amplitude_temp(1:indexNtoP);
    amplitude(indexNtoP+1)=0;
    amplitude(indexNtoP+2:pulsenumber_temp+1)=amplitude_temp(indexNtoP+1:pulsenumber_temp);
    pulsenumber=pulsenumber_temp+1;
end

