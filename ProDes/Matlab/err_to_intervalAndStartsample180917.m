
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
Ireal=[-19.0513,... % p1
        4.08347,... % n1
        6.39084,... % n2
        8.69623,... % n3
        11.0059,... % n4
        13.3189,... % n5
        -10.0701]*10^(-7);...  % p0.5
   

% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p05.T,VC_500.p05.I]=textread([path '0.5V\' content '.txt'],'%f %f','headerlines',1);

content='掃描電壓';
[VC_500.p05.T,VC_500.p05.V]=textread([path '0.5V\' content '.txt'],'%f %f','headerlines',1);


 
%  function[Error,SampleValue,A1,B1,C,K]=santori_ID(Data,StartTime,SamplePeriod,Tensuu,RealValue)


% 幾倍的電壓時間常數取樣第一個值，這裡先固定5倍
baisuu=5;

[E_rc.err.p05,sample,A,B,C,~]=santori_ID([VC_500.p05.T,VC_500.p05.I],tau_v*baisuu,1*tau_c,3,Ireal(7));
E_rc.err.p05
 figure()
subplot(2,1,1)
 plot(VC_500.p05.T,VC_500.p05.V);
 subplot(2,1,2) 
 hold on;
 plot(VC_500.p05.T,VC_500.p05.I);
 plot(sample(:,1),sample(:,2),'x');
 
 t = 0:0.01:5;
 I = A+B*exp(t*C);
 plot(t,I,'r')
 
 
 