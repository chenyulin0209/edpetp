%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們的演算法是不是可以修正，電壓電流時間常數比，所造成的電流估算值誤差
細節：

%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
Ireal=[-19.0513,4.08347,6.39084,8.69623,11.0059,13.3189]*10^(-7);
% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over100\';
content='迴路電流';
[VC_100.p10.T,VC_100.p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_100.n10.T,VC_100.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_100.n20.T,VC_100.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_100.n30.T,VC_100.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_100.n40.T,VC_100.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_100.n50.T,VC_100.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over300\';
content='迴路電流';
[VC_300.p10.T,VC_300.p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_300.n10.T,VC_300.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_300.n20.T,VC_300.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_300.n30.T,VC_300.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_300.n40.T,VC_300.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_300.n50.T,VC_300.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n10.T,VC_500.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n20.T,VC_500.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n30.T,VC_500.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n40.T,VC_500.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over1000\';
content='迴路電流';
[VC_1000.p10.T,VC_1000.p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_1000.n10.T,VC_1000.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_1000.n20.T,VC_1000.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_1000.n30.T,VC_1000.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_1000.n40.T,VC_1000.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_1000.n50.T,VC_1000.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);


% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.4*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;

% 以演算法確認，電壓電流時間常數比100的非理想脈衝響應電流，吻合出來的效果
x=1;
tau_v=tau_c/100;
E_rc.err.cv(x)=100;
[E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_100.p10.T,VC_100.p10.I],tau_v*baisuu,interval,3,Ireal(1));
[E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_100.n10.T,VC_100.n10.I],tau_v*baisuu,interval,3,Ireal(2));
[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_100.n20.T,VC_100.n20.I],tau_v*baisuu,interval,3,Ireal(3));
[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_100.n30.T,VC_100.n30.I],tau_v*baisuu,interval,3,Ireal(4));
[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_100.n40.T,VC_100.n40.I],tau_v*baisuu,interval,3,Ireal(5));
[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_100.n50.T,VC_100.n50.I],tau_v*baisuu,interval,3,Ireal(6));
x=x+1;

% 以演算法確認，電壓電流時間常數比300的非理想脈衝響應電流，吻合出來的效果
tau_v=tau_c/300;
E_rc.err.cv(x)=300;
[E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_300.p10.T,VC_300.p10.I],tau_v*baisuu,interval,3,Ireal(1));
[E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_300.n10.T,VC_300.n10.I],tau_v*baisuu,interval,3,Ireal(2));
[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_300.n20.T,VC_300.n20.I],tau_v*baisuu,interval,3,Ireal(3));
[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_300.n30.T,VC_300.n30.I],tau_v*baisuu,interval,3,Ireal(4));
[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_300.n40.T,VC_300.n40.I],tau_v*baisuu,interval,3,Ireal(5));
[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_300.n50.T,VC_300.n50.I],tau_v*baisuu,interval,3,Ireal(6));
x=x+1;

% 以演算法確認，電壓電流時間常數比500的非理想脈衝響應電流，吻合出來的效果
tau_v=tau_c/500;
E_rc.err.cv(x)=500;
[E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_500.p10.T,VC_500.p10.I],tau_v*baisuu,interval,3,Ireal(1));
[E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_500.n10.T,VC_500.n10.I],tau_v*baisuu,interval,3,Ireal(2));
[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_500.n20.T,VC_500.n20.I],tau_v*baisuu,interval,3,Ireal(3));
[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_500.n30.T,VC_500.n30.I],tau_v*baisuu,interval,3,Ireal(4));
[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_500.n40.T,VC_500.n40.I],tau_v*baisuu,interval,3,Ireal(5));
[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_500.n50.T,VC_500.n50.I],tau_v*baisuu,interval,3,Ireal(6));
x=x+1;

% 以演算法確認，電壓電流時間常數比1000的非理想脈衝響應電流，吻合出來的效果
tau_v=tau_c/1000;
E_rc.err.cv(x)=1000;
[E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_1000.p10.T,VC_1000.p10.I],tau_v*baisuu,interval,3,Ireal(1));
[E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_1000.n10.T,VC_1000.n10.I],tau_v*baisuu,interval,3,Ireal(2));
[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_1000.n20.T,VC_1000.n20.I],tau_v*baisuu,interval,3,Ireal(3));
[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_1000.n30.T,VC_1000.n30.I],tau_v*baisuu,interval,3,Ireal(4));
[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_1000.n40.T,VC_1000.n40.I],tau_v*baisuu,interval,3,Ireal(5));
[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_1000.n50.T,VC_1000.n50.I],tau_v*baisuu,interval,3,Ireal(6));
x=x+1;



% 把圖畫出來，這張圖其實沒什麼意義，就只是比較容易看出來，誤差最低和最高的值
figure(1)
plot(...
[1.0 -1.0 -2.0 -3.0 -4.0 -5.0],...
[E_rc.err.p10(1),E_rc.err.n10(1),E_rc.err.n20(1),E_rc.err.n30(1),E_rc.err.n40(1),E_rc.err.n50(1)],...
[1.0 -1.0 -2.0 -3.0 -4.0 -5.0],...
[E_rc.err.p10(2),E_rc.err.n10(2),E_rc.err.n20(2),E_rc.err.n30(2),E_rc.err.n40(2),E_rc.err.n50(2)],...
[1.0 -1.0 -2.0 -3.0 -4.0 -5.0],...
[E_rc.err.p10(3),E_rc.err.n10(3),E_rc.err.n20(3),E_rc.err.n30(3),E_rc.err.n40(3),E_rc.err.n50(3)],...
[1.0 -1.0 -2.0 -3.0 -4.0 -5.0],...
[E_rc.err.p10(4),E_rc.err.n10(4),E_rc.err.n20(4),E_rc.err.n30(4),E_rc.err.n40(4),E_rc.err.n50(4)])
legend('100倍','300倍','500倍','1000倍','location','northwest')

% 把最低和最高的誤差值顯示出來
x=1;
max([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])
min([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])

x=2;
max([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])
min([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])

x=3;
max([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])
min([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])

x=4;
max([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])
min([E_rc.err.p10(x),E_rc.err.n10(x),E_rc.err.n20(x),E_rc.err.n30(x),E_rc.err.n40(x),E_rc.err.n50(x)])



