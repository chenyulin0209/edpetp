% 輸入電流資料，起始延遲，取樣週期，點數(這個點數應該用不到)，真實值(用來和吻合出來的電流值計算誤差)
% 輸出真實值和吻合出來的電流值之間的誤差，取樣點，吻合出的係數A，吻合出的係數B，吻合出的係數C，K(可以不用理他)
function[Error,SampleValue,A1,B1,C,K]=santori_ID(Data,StartTime,SamplePeriod,Tensuu,RealValue)
    %PSPICE模擬出的總點數
    DataLength=size(Data);
    %取樣開始時間
    SampleTime=StartTime;
    %取樣點數(應該就固定3點)
    DotNumber=Tensuu;
    %找到取樣的點數在矩陣中的編序值(老師都稱為index)
    for i=1:DotNumber
        for j=1:DataLength(1)
            if Data(j,1)>=SampleTime
                t(i)=Data(j,1);
                I(i)=Data(j,2);
%                 SamplePeriod 取樣區間(第一點到最後點的時間)   SamplePeriod/2取樣週期(點跟點之間時間)
                SampleTime=SampleTime+SamplePeriod/2;
                break
            end
        end
    end
    
%   傳出取樣值
    SampleValue=[t(:),I(:)];
    
    
    
%   計算出K(應該用不到)
    K=K_keisan(t,I);
%   計算出係數C(這邊有用到一個副函式C_keisan，你對副函式按右鍵，會有一個OPEN C_keisan，就可以看到他內容了)
    C=real(C_keisan(t,I,SamplePeriod/2));

    
%   透過取樣點的編號找到相對應的電流值存進矩陣
    Y=I(:);
    
%   將時間變數存進矩陣
    for i=1:Tensuu
        A(i,1)=1;
        A(i,2)=exp(C*t(i));
    end

%   使出最小方差法
    hh=inv(A'*A)*A'*Y;
    ID_result=real(hh);
    
%   計算出係數A
    A1=hh(1);
%   計算出係數B
    B1=hh(2);

%   得到估算值
    keisan=A1+B1;
    rironn=RealValue;
%   計算出和真實值間的誤差
    Error=(keisan-rironn)/rironn*100;



end