
clear;
clc;
close all;
v_start = -6;
count=1;
while v_start<=6
    voltage(count,1) = v_start;
    v_start = v_start+0.000004;
    count=count+1;
end

[I,~]=FDDSI_Vcurve(-3:0.01:5,58.2E-4,6,0,0,2500,1E11);
figure(1);
plot(-3:0.01:5,I);


