% 最低解析度，確認所有溫濃皆適用 %

clc;clear;close all;

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\';
DT='2000K_1E9\漸升式三角波\離2過2\';
content='掃描電壓';
[wave.T2000D1E9.T,wave.T2000D1E9.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.T2000D1E9.T,wave.T2000D1E9.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.T2000D1E9.T,wave.T2000D1E9.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);


load lowest_res wave

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

disp(['數據讀取完畢'])

[wave.T2000D1E9.RI,wave.T2000D1E9.RT,wave.T2000D1E9.amp,wave.T2000D1E9.NtoP,wave.T2000D1E9.PN]=FindPulseStart(wave.T2000D1E9.T,wave.T2000D1E9.Vs);

tau_c=0.02;
tau_c=0.5;
tau_v=0.0001;
baisuu=7;
interval=0.4*tau_c;
dot=192;
dot=48;
x=1;


g20=2.96E-9;
g100=5.92E-10;
gosa=0;
kai=1;


for j=1:kai
    wave.T2000D1E9.Inoise=wave.T2000D1E9.I+rand(size(wave.T2000D1E9.I,1),1)*g100*2-g100;

    tau_c=0.5;
    dot=24;
    [wave.T2000D1E9.IV_rc.N8(1,:),wave.T2000D1E9.IV_rc.N8(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=48;
    [wave.T2000D1E9.IV_rc.N16(1,:),wave.T2000D1E9.IV_rc.N16(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=96;
    [wave.T2000D1E9.IV_rc.N32(1,:),wave.T2000D1E9.IV_rc.N32(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=192;
    [wave.T2000D1E9.IV_rc.N64(1,:),wave.T2000D1E9.IV_rc.N64(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=384;
    [wave.T2000D1E9.IV_rc.N128(1,:),wave.T2000D1E9.IV_rc.N128(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);


    [T.T2000D1E9.rc.N8(j),D.T2000D1E9.rc.N8(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc.N8(1,:),wave.T2000D1E9.IV_rc.N8(2,:));
    [T.T2000D1E9.rc.N16(j),D.T2000D1E9.rc.N16(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc.N16(1,:),wave.T2000D1E9.IV_rc.N16(2,:));
    [T.T2000D1E9.rc.N32(j),D.T2000D1E9.rc.N32(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc.N32(1,:),wave.T2000D1E9.IV_rc.N32(2,:));
    [T.T2000D1E9.rc.N64(j),D.T2000D1E9.rc.N64(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc.N64(1,:),wave.T2000D1E9.IV_rc.N64(2,:));
    [T.T2000D1E9.rc.N128(j),D.T2000D1E9.rc.N128(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc.N128(1,:),wave.T2000D1E9.IV_rc.N128(2,:));

    disp(['執行次數  ' num2str(j) '/' num2str(kai) '     進度' num2str(j/kai*100) '%' ])


end

load set_DT_data

disp(['8點'])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc.N8) '   濃度' num2str(D.T2000D1E9.rc.N8) ])
disp(['16點'])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc.N16) '   濃度' num2str(D.T2000D1E9.rc.N16) ])
disp(['32點'])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc.N32) '   濃度' num2str(D.T2000D1E9.rc.N32) ])
disp(['64點'])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc.N64) '   濃度' num2str(D.T2000D1E9.rc.N64) ])
disp(['128點'])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc.N128) '   濃度' num2str(D.T2000D1E9.rc.N128) ])




figure()
hold on
errorbar([3 4 5 6 7],...
    [(mean(T.T2000D1E9.rc.N8)-2000)/2000*100,(mean(T.T2000D1E9.rc.N16)-2000)/2000*100,(mean(T.T2000D1E9.rc.N32)-2000)/2000*100,(mean(T.T2000D1E9.rc.N64)-2000)/2000*100,(mean(T.T2000D1E9.rc.N128)-2000)/2000*100],...
    [std(T.T2000D1E9.rc.N8)/2000*100,std(T.T2000D1E9.rc.N16)/2000*100,std(T.T2000D1E9.rc.N32)/2000*100,std(T.T2000D1E9.rc.N64)/2000*100,std(T.T2000D1E9.rc.N128)/2000*100])
% plot([3 7],[0 0],'g')
% title('\fontsize{12}濃度估算誤差')
xlabel('\fontsize{12}Sample Groups 2^n(Groups)');ylabel('\fontsize{12}Error of Electron Temperature(%)');
set(gca,'FontSize',12)
grid on



figure()
hold on
errorbar([3 4 5 6 7],...
    [(mean(D.T2000D1E9.rc.N8)-1E9)/1E9*100,(mean(D.T2000D1E9.rc.N16)-1E9)/1E9*100,(mean(D.T2000D1E9.rc.N32)-1E9)/1E9*100,(mean(D.T2000D1E9.rc.N64)-1E9)/1E9*100,(mean(D.T2000D1E9.rc.N128)-1E9)/1E9*100],...
    [std(D.T2000D1E9.rc.N8)/1E9*100,std(D.T2000D1E9.rc.N16)/1E9*100,std(D.T2000D1E9.rc.N32)/1E9*100,std(D.T2000D1E9.rc.N64)/1E9*100,std(D.T2000D1E9.rc.N128)/1E9*100])
% plot([3 7],[0 0],'g')
xlabel('\fontsize{12}Sample Groups 2^n(Groups)');ylabel('\fontsize{12}Error of Electron Density(%)');
set(gca,'FontSize',12)
grid on










