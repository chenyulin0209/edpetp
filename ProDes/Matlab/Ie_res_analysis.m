%{
ヘ
だ猂Ie_res暗ㄓ挡狦ゑ耕ぃ筿溃秆猂芠诡ㄤキА粇畉㎝夹非畉
灿竊

%}

% 弄Ie_res挡狦
load Ie_res_result

% 挡狦
disp(['16翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot16.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot16.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot16.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot16.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot16.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot16.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot16.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot16.rc)) ])
disp(['32翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot32.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot32.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot32.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot32.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot32.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot32.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot32.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot32.rc)) ])
disp(['64翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot64.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot64.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot64.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot64.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot64.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot64.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot64.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot64.rc)) ])
disp(['128翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot128.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot128.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot128.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot128.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot128.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot128.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot128.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot128.rc)) ])
disp(['256翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot256.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot256.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot256.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot256.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot256.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot256.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot256.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot256.rc)) ])
disp(['512翴'])
disp(['T2000D1E11 2翴 放' num2str(mean(T.N2.T2000D1E11.dot512.rc)) ' 夹非畉' num2str(std(T.N2.T2000D1E11.dot512.rc)) '   緻' num2str(mean(D.N2.T2000D1E11.dot512.rc)) ' 夹非畉' num2str(std(D.N2.T2000D1E11.dot512.rc)) ])
disp(['T2000D1E11 4翴 放' num2str(mean(T.N4.T2000D1E11.dot512.rc)) ' 夹非畉' num2str(std(T.N4.T2000D1E11.dot512.rc)) '   緻' num2str(mean(D.N4.T2000D1E11.dot512.rc)) ' 夹非畉' num2str(std(D.N4.T2000D1E11.dot512.rc)) ])



% 放挡狦
figure()
Treal=2000;
hold on
errorbar(...
    [4 5 6 7 8 9],...
    [(mean(T.N2.T2000D1E11.dot16.rc)-Treal)/Treal*100 (mean(T.N2.T2000D1E11.dot32.rc)-Treal)/Treal*100 (mean(T.N2.T2000D1E11.dot64.rc)-Treal)/Treal*100 (mean(T.N2.T2000D1E11.dot128.rc)-Treal)/Treal*100 (mean(T.N2.T2000D1E11.dot256.rc)-Treal)/Treal*100 (mean(T.N2.T2000D1E11.dot512.rc)-Treal)/Treal*100],...
    [std(T.N2.T2000D1E11.dot16.rc)/Treal*100 std(T.N2.T2000D1E11.dot32.rc)/Treal*100 std(T.N2.T2000D1E11.dot64.rc)/Treal*100 std(T.N2.T2000D1E11.dot128.rc)/Treal*100 std(T.N2.T2000D1E11.dot256.rc)/Treal*100 std(T.N2.T2000D1E11.dot512.rc)/Treal*100],'b')
errorbar(...
    [4 5 6 7 8 9],...
    [(mean(T.N4.T2000D1E11.dot16.rc)-Treal)/Treal*100 (mean(T.N4.T2000D1E11.dot32.rc)-Treal)/Treal*100 (mean(T.N4.T2000D1E11.dot64.rc)-Treal)/Treal*100 (mean(T.N4.T2000D1E11.dot128.rc)-Treal)/Treal*100 (mean(T.N4.T2000D1E11.dot256.rc)-Treal)/Treal*100 (mean(T.N4.T2000D1E11.dot512.rc)-Treal)/Treal*100],...
    [std(T.N4.T2000D1E11.dot16.rc)/Treal*100 std(T.N4.T2000D1E11.dot32.rc)/Treal*100 std(T.N4.T2000D1E11.dot64.rc)/Treal*100 std(T.N4.T2000D1E11.dot128.rc)/Treal*100 std(T.N4.T2000D1E11.dot256.rc)/Treal*100 std(T.N4.T2000D1E11.dot512.rc)/Treal*100],'r')
plot([3.5 9.5],[0 0],'g')
ylim([-30 40])
xlabel('\fontsize{12}Total Sample Point 2^n*x3');ylabel('\fontsize{12}Error of Electron Temperature(%)');
legend('Sample 2 points in Electron Saturation Region','Sample 4 points in Electron Saturation Region')
set(gca,'FontSize',12)
grid on

% 緻挡狦
figure()
Dreal=1E11;
hold on
errorbar(...
    [4 5 6 7 8 9],...
    [(mean(D.N2.T2000D1E11.dot16.rc)-Dreal)/Dreal*100 (mean(D.N2.T2000D1E11.dot32.rc)-Dreal)/Dreal*100 (mean(D.N2.T2000D1E11.dot64.rc)-Dreal)/Dreal*100 (mean(D.N2.T2000D1E11.dot128.rc)-Dreal)/Dreal*100 (mean(D.N2.T2000D1E11.dot256.rc)-Dreal)/Dreal*100 (mean(D.N2.T2000D1E11.dot512.rc)-Dreal)/Dreal*100],...
    [std(D.N2.T2000D1E11.dot16.rc)/Dreal*100 std(D.N2.T2000D1E11.dot32.rc)/Dreal*100 std(D.N2.T2000D1E11.dot64.rc)/Dreal*100 std(D.N2.T2000D1E11.dot128.rc)/Dreal*100 std(D.N2.T2000D1E11.dot256.rc)/Dreal*100 std(D.N2.T2000D1E11.dot512.rc)/Dreal*100],'b')
errorbar(...
    [4 5 6 7 8 9],...
    [(mean(D.N4.T2000D1E11.dot16.rc)-Dreal)/Dreal*100 (mean(D.N4.T2000D1E11.dot32.rc)-Dreal)/Dreal*100 (mean(D.N4.T2000D1E11.dot64.rc)-Dreal)/Dreal*100 (mean(D.N4.T2000D1E11.dot128.rc)-Dreal)/Dreal*100 (mean(D.N4.T2000D1E11.dot256.rc)-Dreal)/Dreal*100 (mean(D.N4.T2000D1E11.dot512.rc)-Dreal)/Dreal*100],...
    [std(D.N4.T2000D1E11.dot16.rc)/Dreal*100 std(D.N4.T2000D1E11.dot32.rc)/Dreal*100 std(D.N4.T2000D1E11.dot64.rc)/Dreal*100 std(D.N4.T2000D1E11.dot128.rc)/Dreal*100 std(D.N4.T2000D1E11.dot256.rc)/Dreal*100 std(D.N4.T2000D1E11.dot512.rc)/Dreal*100],'r')
plot([3.5 9.5],[0 0],'g')
ylim([-15 20])
xlabel('\fontsize{12}Total Sample Point 2^n*x3');ylabel('\fontsize{12}Error of Electron Density(%)');
legend('Sample 2 points in Electron Saturation Region','Sample 4 points in Electron Saturation Region')
set(gca,'FontSize',12)
grid on



