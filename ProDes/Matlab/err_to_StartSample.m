%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
將取樣延遲做正規化，看取樣延遲要怎麼取比較理想
細節：

%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
Ireal=[-19.0513,4.08347,6.39084,8.69623,11.0059,13.3189]*10^(-7);

% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n10.T,VC_500.n10.I]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n20.T,VC_500.n20.I]=textread([path '-2.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n30.T,VC_500.n30.I]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n40.T,VC_500.n40.I]=textread([path '-4.0V\' content '.txt'],'%f %f','headerlines',1);
[VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前一個實驗知道0.4倍最理想
interval=0.4*tau_c;

% 以指數吻合法，在不同取樣延遲下，將6種脈衝振幅都做一次
x=1;
for i=4:10
	E_rc.err.Start(x)=i;
    [E_rc.err.p10(x),~,~,~,~,~]=santori_ID([VC_500.p10.T,VC_500.p10.I],tau_v*i,interval,3,Ireal(1));
    [E_rc.err.n10(x),~,~,~,~,~]=santori_ID([VC_500.n10.T,VC_500.n10.I],tau_v*i,interval,3,Ireal(2));
	[E_rc.err.n20(x),~,~,~,~,~]=santori_ID([VC_500.n20.T,VC_500.n20.I],tau_v*i,interval,3,Ireal(3));
	[E_rc.err.n30(x),~,~,~,~,~]=santori_ID([VC_500.n30.T,VC_500.n30.I],tau_v*i,interval,3,Ireal(4));
	[E_rc.err.n40(x),~,~,~,~,~]=santori_ID([VC_500.n40.T,VC_500.n40.I],tau_v*i,interval,3,Ireal(5));
	[E_rc.err.n50(x),~,~,~,~,~]=santori_ID([VC_500.n50.T,VC_500.n50.I],tau_v*i,interval,3,Ireal(6));
    x=x+1;
end

% 畫出正規化取樣延遲對應電流估算誤差圖
figure(2)
grid on
hold on
plot(...
E_rc.err.Start,E_rc.err.p10,...
E_rc.err.Start,E_rc.err.n10,...
E_rc.err.Start,E_rc.err.n20,...
E_rc.err.Start,E_rc.err.n30,...
E_rc.err.Start,E_rc.err.n40,...
E_rc.err.Start,E_rc.err.n50)
xlabel('\fontsize{12}Normalized Sample Delay(\tau_v)');ylabel('\fontsize{12}Error(%)');
legend('1.0V','-1.0V','-2.0V','-3.0V','-4.0V','-5.0V','location','southeast')
set(gca,'FontSize',12)





