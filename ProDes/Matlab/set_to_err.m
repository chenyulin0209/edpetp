%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們演算法的多實驗組數，是不是可以抗雜訊
細節：

%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，量測到的電流值
Ireal=[-19.0513]*10^(-7)

% 讀資料，這邊電流要用高解析度的電流，因為快速取樣電流抗雜訊的時候，取樣週期很低，
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[T10,I10]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);

% 訊雜比
gosa=10;
% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.4*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 快速取樣的取樣週期
rapid_S=15*10^(-6);
% 執行次數，越多次平均誤差跟標準差會漸趨穩定
kai=1;
for i=1:kai
    i
%   加入雜訊，雜訊大小是用最大電流的%數決定
    Inoise=I10+rand(685601,1)*Ireal*gosa/100-Ireal*gosa/100/2;
%   因為我函式沒寫好，1組實驗的時候，要用santori_ID計算
    [E1(i),~,~,~,~,~]=santori_ID([T10,Inoise],tau_v*baisuu,interval,3,Ireal(1));
%   因為我函式沒寫好，用mean_I_ID的時候，實驗組數要乘上3
    [E2(i),~,~,~,~,~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,6,Ireal(1));
    [E4(i),~,~,~,~,~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,12,Ireal(1));
    [E8(i),~,~,~,~,~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,24,Ireal(1));
    [E16(i),~,~,~,~,~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,48,Ireal(1));
    [E32(i),~,~,~,~,~]=mean_I_ID([T10,Inoise],tau_v*baisuu,interval,rapid_S,96,Ireal(1));
end

% 讀取之前跑過1000次的資料
load set_to_err

% 畫出不同組數下電流估算誤差和標準差
figure(1)
hold on
errorbar([0 1 2 3 4 5],[mean(E1) mean(E2) mean(E4) mean(E8) mean(E16) mean(E32)],[std(E1) std(E2) std(E4) std(E8) std(E16) std(E32)],'b')
grid on
xlabel('\fontsize{12}2^n(Groups)');ylabel('\fontsize{12}Error(%)');
set(gca,'FontSize',12)


