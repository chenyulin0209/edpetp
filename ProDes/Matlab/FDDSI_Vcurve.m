%{
以溫濃等參數畫出取得  無汙染層、雙Debye 電流響應I,探針電壓 vpr
Vscan   輸入掃瞄電壓矩陣            -3:0.01:1
Apr     探針面積 Probe Area        58.2E-4
Ar      探針衛星面積比 Area ratio  6
beta1   探針形狀參數               0 0.5 1
beta1   衛星形狀參數               0 0.5 1
Te      環境電子溫度 electron temperature
Ne      環境電子濃度 electron density, 本演算法假設Ne與離子濃度 ion density 相同 

Note. 作為 DDS無汙染查表  應可用 (Vscan,I)作為Table 需測試 181023
%}

function [I,vpr]=FDDSI_Vcurve(Vscan,Apr,Ar,beta1,beta2,Te,Ne)

Vpl=FDDSVpl(Ar,beta1,beta2,Te);
for i=1:length(Vscan)
  vpr(i)=FDDSv(Vscan(i),Ar,beta1,beta2,Te);  %算v(i)在Ar B1 B2 Te下的vpr(i) 分壓
  [~,~,I(i)]=DebyeCurrentSim(vpr(i),Vpl,Ne,Ne,Te,Apr,beta1); %算 vpr(i)下 Ipr的電流為
end



%{
figure()
plot(vpr,-I);        %實際Probe IV曲線
hold on
grid on
plot(Vscan,-I,'r');  %受面積比扭曲 曲線/Table
legend('理想曲線','受小面積扭曲曲線');
%}
end
