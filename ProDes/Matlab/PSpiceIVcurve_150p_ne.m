clc;
clear;

A = csvread('pspice_result_ne.csv');		% 將 data.csv 的內容讀到矩陣 A
B = csvread('ideal_I_Vsweep.csv');


Vs=A(:,1)';

%1E9
Vf_1E9=A(242,12);
Vp_1E9=A(:,12)';
I_1E9=A(:,13)';

subplot(2,2,1);
plot(Vp_1E9,(I_1E9*10^6),'b');
hold on;
plot(Vs+Vf_1E9,(I_1E9*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : ne=1E9m-3');
%axis([-1.4 0 -2 6]);

%1E10
Vf_1E10=A(242,2);
Vp_1E10=A(:,2)';
I_1E10=A(:,3)';

subplot(2,2,2);
plot(Vp_1E10,(I_1E10*10^6),'b');
hold on;
plot(Vs+Vf_1E10,(I_1E10*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : ne=1E10m-3');
%axis([-1.4 0 -2 6]);

%1E11
Vf_1E11=A(242,8);
Vp_1E11=A(:,8);
I_1E11=A(:,9);

subplot(2,2,3);
plot(Vp_1E11,(I_1E11*10^6),'b');
hold on;
plot(Vs+Vf_1E11,(I_1E11*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : ne=1E11m-3');
%axis([-1.4 0 -2 6]);

%1E12
Vf_1E12=A(242,10);
Vp_1E12=A(:,10)
I_1E12=A(:,11);

subplot(2,2,4);
plot(Vp_1E12,(I_1E12*10^6),'b');
hold on;
plot(Vs+Vf_1E12,(I_1E12*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : ne=1E12m-3');
%axis([-1.4 0 -2 6]);
%---------------------------------------
figure;
Videal=B(:,1)';
Iideal=B(:,2)';
Iideal_1E11=B(:,7)';
Iideal_1E12=B(:,8)';
Iideal_1E9=B(:,9)';


subplot(2,2,1);
plot(Videal,(Iideal_1E9),'b');
hold on;
plot(Vp_1E9,(I_1E9*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : ne=1E9m-3');

subplot(2,2,2);
plot(Videal,(Iideal),'b');
hold on;
plot(Vp_1E10,(I_1E10*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : ne=1E10m-3');

subplot(2,2,3);
plot(Videal,(Iideal_1E11),'b');
hold on;
plot(Vp_1E11,(I_1E11*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : ne=1E11m-3');
%axis([-0.5 0.5 -0.02 0.04]);


subplot(2,2,4);
plot(Videal,(Iideal_1E12),'b');
hold on;
plot(Vp_1E12,(I_1E12*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : ne=1E12m-3');
%axis([-0.5 0.5 -0.02 0.04]);
%---------------------------------------
for i=1:1:476
    err_1E9(i)=(Vp_1E9(i)-Vf_1E9)/Vs(i)*100;
end;

for i=1:1:476
    err_1E10(i)=(Vp_1E10(i)-Vf_1E10)/Vs(i)*100;
end;

for i=1:1:476
    err_1E11(i)=(Vp_1E11(i)-Vf_1E11)/Vs(i)*100;
end;

for i=1:1:476
    err_1E12(i)=(Vp_1E12(i)-Vf_1E12)/Vs(i)*100;
end;

figure;
plot(Vs,err_1E9,':b');
hold on;
plot(Vs,err_1E10,'b');
hold on;
plot(Vs,err_1E11,'--g');
hold on;
plot(Vs,err_1E12,'r');

grid on;
xlabel('Sweeping voltage(V)');
ylabel('Voltage of Probe-to-Sweep ratio(%)');
legend('ne=1E9m-3','ne=1E10m-3','ne=1E11m-3','ne=1E12m-3');
legend('Location','SouthWest');
title('Voltage of Probe-to-Sweep with ne relation');
axis([-0.5 0.5 0 100]);



