% 輸入電流，用吻合法抓出理想脈波電流值 %
function [V,I]=Max_I(Tin,Vin,Iin,number,starttime,startindex)
    x=1;
    for i=1:number
        if Vin(i)==0
            I(i)=0;
        elseif Vin(i)>0
            [~,~,c2]=T_find_D(Tin,Iin,starttime(x)+0.2);
            I(i)=min(Iin(startindex(x):c2));
            x=x+1;
        else
            [~,~,c2]=T_find_D(Tin,Iin,starttime(x)+0.2);
            I(i)=max(Iin(startindex(x):c2));
            x=x+1;
        end
    end
    V=Vin;
end

