function[Error,SampleValue,A1,B1,Error_new]=nitori_ID(Data,StartTime,SamplePeriod,Tensuu,RealValue)
    %PSPICE模擬出的總點數
    DataLength=size(Data);
    %取樣開始時間
    SampleTime=StartTime;
    %取樣點數
    DotNumber=Tensuu;
    %找到取樣的點數在矩陣中的編號
    for i=1:DotNumber
        for j=1:DataLength(1)
            if Data(j,1)>=SampleTime
                t(i)=Data(j,1);
                I(i)=Data(j,2);
                SampleTime=SampleTime+SamplePeriod;
                break
            end
        end
    end
    
    SampleValue=[t(:),I(:)];
    
    A1=(I(2)-I(1))/(t(2)-t(1));
    B1=I(1)-A1*t(1);

    keisan=B1
    rironn=RealValue;
    Error=(keisan-rironn)/rironn*100;

    %新方法
    keisann_new=min(Data(:,2));
    Error_new=(keisann_new-rironn)/rironn*100;

end