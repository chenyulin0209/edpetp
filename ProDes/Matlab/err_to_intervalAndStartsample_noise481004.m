%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
將取樣區間做正規化，看取樣區間要怎麼取比較理想
細節：
這裡的橫軸是取樣週期，要注意這個問題(詳細說明在samp_proc_effect的細節)
%}
clc;clear;close all;
load('CurrCuit_200k_1E11_tau_v_0.1674over500_Error_32_181003.1_0.03to0.1.mat');
point.p10=E32_p10;
point.p08=E32_p08;
point.p04=E32_p04;
point.n30=E32_n30;
point.n50=E32_n50;

load('CurrCuit_200k_1E11_tau_v_0.1674over500_Error_32_181003.2_0.1to0.5_0.2tau_c.mat');

subplot(1,2,1);
point.p10=[point.p10;E32_p10 ];
hold on;grid on;
point.p08=[point.p08;E32_p08 ];
point.p04=[point.p04;E32_p04 ];
point.n30=[point.n30;E32_n30 ];
point.n50=[point.n50;E32_n50 ];

%標準差縮放比例
tamp=0.02;

errorbar(point.p10(:,1),point.p10(:,2),point.p10(:,3)*tamp);
errorbar(point.p08(:,1),point.p08(:,2),point.p08(:,3)*tamp);
errorbar(point.p04(:,1),point.p04(:,2),point.p04(:,3)*tamp);
errorbar(point.n30(:,1),point.n30(:,2),point.n30(:,3)*tamp);
errorbar(point.n50(:,1),point.n50(:,2),point.n50(:,3)*tamp);
legend('1.0V','0.8V','0.4V','-3.0V','-5.0V','location','northwest');
xlim([0 0.5]);ylim([ -0.1 2.3]);
xlabel('\fontsize{12}Normalized Sample Period T_p(\tau_C(1V))');ylabel('\fontsize{12}Error(%)');

subplot(1,2,2);
load('err_to_StartSample181004.mat');
hold on ;grid on;
errorbar(E32_p10(:,1),-E32_p10(:,2),E32_p10(:,3)*tamp);
errorbar(E32_p08(:,1),-E32_p08(:,2),E32_p08(:,3)*tamp);
errorbar(E32_p04(:,1),-E32_p04(:,2),E32_p04(:,3)*tamp);
errorbar(E32_n30(:,1),-E32_n30(:,2),E32_n30(:,3)*tamp);
errorbar(E32_n50(:,1),-E32_n50(:,2),E32_n50(:,3)*tamp);
legend('1.0V','0.8V','0.4V','-3.0V','-5.0V','location','northwest');
xlim([2.5 10]);ylim([ -1 5]);

xlabel('\fontsize{12}Normalized Sample Delay T_d(\tau_v)');ylabel('\fontsize{12}Error(%)');
