%{
ヘ
だ猂ronbun_D暗ㄓ挡狦ゑ耕程猭㎝计猭芠诡ㄢキА粇畉㎝夹非畉
灿竊

%}
clc;clear;close all;

% 弄ronbun_D挡狦
load ronbun_D_data

% рㄢ贺よ猭キА放㎝夹非畉キА緻㎝夹非畉陪ボㄓ
disp(['计  放キА ' num2str(mean(T.T2000D1E9.rc)) '   放夹非畉 ' num2str(std(T.T2000D1E9.rc)) '  緻キА ' num2str(mean(D.T2000D1E9.rc)) '  緻夹非畉 ' num2str(std(D.T2000D1E9.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2000D1E9.ma)) '   放夹非畉 ' num2str(std(T.T2000D1E9.ma)) '  緻キА ' num2str(mean(D.T2000D1E9.ma)) '  緻夹非畉 ' num2str(std(D.T2000D1E9.ma)) ])
disp(['计  放キА ' num2str(mean(T.T2000D1E10.rc)) '   放夹非畉 ' num2str(std(T.T2000D1E10.rc)) '  緻キА ' num2str(mean(D.T2000D1E10.rc)) '  緻夹非畉 ' num2str(std(D.T2000D1E10.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2000D1E10.ma)) '   放夹非畉 ' num2str(std(T.T2000D1E10.ma)) '  緻キА ' num2str(mean(D.T2000D1E10.ma)) '  緻夹非畉 ' num2str(std(D.T2000D1E10.ma)) ])
disp(['计  放キА ' num2str(mean(T.T2000D1E11.rc)) '   放夹非畉 ' num2str(std(T.T2000D1E11.rc)) '  緻キА ' num2str(mean(D.T2000D1E11.rc)) '  緻夹非畉 ' num2str(std(D.T2000D1E11.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2000D1E11.ma)) '   放夹非畉 ' num2str(std(T.T2000D1E11.ma)) '  緻キА ' num2str(mean(D.T2000D1E11.ma)) '  緻夹非畉 ' num2str(std(D.T2000D1E11.ma)) ])
disp(['计  放キА ' num2str(mean(T.T2000D1E12.rc)) '   放夹非畉 ' num2str(std(T.T2000D1E12.rc)) '  緻キА ' num2str(mean(D.T2000D1E12.rc)) '  緻夹非畉 ' num2str(std(D.T2000D1E12.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2000D1E12.ma)) '   放夹非畉 ' num2str(std(T.T2000D1E12.ma)) '  緻キА ' num2str(mean(D.T2000D1E12.ma)) '  緻夹非畉 ' num2str(std(D.T2000D1E12.ma)) ])

% ㏕﹚跑筿放2000K
Tre=2000;
% 礶ぃ緻放︳衡粇畉瓜
figure()
hold on
errorbar(...
    [9 10 11 12],...
    [(mean(T.T2000D1E9.rc-Tre)/Tre*100) (mean(T.T2000D1E10.rc-Tre)/Tre*100) (mean(T.T2000D1E11.rc-Tre)/Tre*100) (mean(T.T2000D1E12.rc-Tre)/Tre*100)],...
    [(std(T.T2000D1E9.rc)/Tre*100) (std(T.T2000D1E10.rc)/Tre*100) (std(T.T2000D1E11.rc)/Tre*100) (std(T.T2000D1E12.rc)/Tre*100)],'r')
errorbar(...
    [9 10 11 12],...
    [(mean(T.T2000D1E9.ma-Tre)/Tre*100) (mean(T.T2000D1E10.ma-Tre)/Tre*100) (mean(T.T2000D1E11.ma-Tre)/Tre*100) (mean(T.T2000D1E12.ma-Tre)/Tre*100)],...
    [(std(T.T2000D1E9.ma)/Tre*100) (std(T.T2000D1E10.ma)/Tre*100) (std(T.T2000D1E11.ma)/Tre*100) (std(T.T2000D1E12.ma)/Tre*100)],'b')
plot([9 12],[0 0],'g')
xlabel('\fontsize{12}Electron Density 10^n(m^{-3})');ylabel('\fontsize{12}Error of Electron Temperature(%)');
legend('Log-Linear Regression','Maximum Current Algorithm','Desired Value','location','southwest')
set(gca,'FontSize',12)
grid on

% 礶ぃ緻緻︳衡粇畉瓜
figure()
hold on
errorbar(...
    [9 10 11 12],...
    [(mean(D.T2000D1E9.rc-1E9)/1E9*100) (mean(D.T2000D1E10.rc-1E10)/1E10*100) (mean(D.T2000D1E11.rc-1E11)/1E11*100) (mean(D.T2000D1E12.rc-1E12)/1E12*100)],...
    [(std(D.T2000D1E9.rc)/1E9*100) (std(D.T2000D1E10.rc)/1E10*100) (std(D.T2000D1E11.rc)/1E11*100) (std(D.T2000D1E12.rc)/1E12*100)],'r')
errorbar(...
    [9 10 11 12],...
    [(mean(D.T2000D1E9.ma-1E9)/1E9*100) (mean(D.T2000D1E10.ma-1E10)/1E10*100) (mean(D.T2000D1E11.ma-1E11)/1E11*100) (mean(D.T2000D1E12.ma-1E12)/1E12*100)],...
    [(std(D.T2000D1E9.ma)/1E9*100) (std(D.T2000D1E10.ma)/1E10*100) (std(D.T2000D1E11.ma)/1E11*100) (std(D.T2000D1E12.ma)/1E12*100)],'b')
plot([9 12],[0 0],'g')
ylim([-15 25])
xlabel('\fontsize{12}Electron Density 10^n(m^{-3})');ylabel('\fontsize{12}Error of Electron Density(%)');
legend('Log-Linear Regression','Maximum Current Algorithm','Desired Value','location','southwest')
set(gca,'FontSize',12)
grid on

