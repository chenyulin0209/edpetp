%{
目的：
用最大值法、線性吻合法、指數吻合法，去取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
說明演算法應該選用哪一個模型效果比較好
細節：
取樣區間，在指數吻合法中，會在區間內取頭中尾三點
取樣區間，在線性吻合法中，會在區間內取頭尾兩點
所以描述取樣週期的時候要注意
%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，量測到的電流值
seikaku=-1.90522*10^(-6);

% 讀資料
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='掃描電壓';
[tau_v_ideal.T,tau_v_ideal.V]=textread([path '理想\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500.T,tau_v_over_500.V]=textread([path 'tau_v_0.1674over500\1.0V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[tau_v_ideal.T,tau_v_ideal.I]=textread([path '理想\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500.T,tau_v_over_500.I]=textread([path 'tau_v_0.1674over500\1.0V\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電壓時間常數取樣第一個值，這裡是設定倍數
baisuu=5;

% 以三種方法，在不同取樣區間下都做一次
x=1;
for i=0.02*tau_c:0.01*tau_c:10*tau_c
%   兩點線性吻合法
    [E1(x,1),SS,A,B,E_n]=nitori_ID([tau_v_over_500.T,tau_v_over_500.I],tau_v*baisuu,i,2,seikaku);
    E1(x,2)=i;
%   三點指數吻合法
    [E2(x,1),S,A,B,C,E_n]=santori_ID([tau_v_over_500.T,tau_v_over_500.I],tau_v*baisuu,i,3,seikaku);
    E2(x,2)=i;
%   最大值法
    E3(x,1)=((min(tau_v_over_500.I))-seikaku)/seikaku*100;
    E3(x,2)=i;
    x=x+1;
end

% 畫出不同取樣區間下，三方法對應的電流估算誤差
figure(1)
hold on
grid on
plot(E1(:,2)/tau_c,E1(:,1),E2(:,2)/tau_c,E2(:,1),E3(:,2)/tau_c,E3(:,1))
xlim([0.1 3]);
xlabel('\fontsize{12}Normalized Sample Interval(\tau_c)');ylabel('\fontsize{12}Error(%)');
legend('Simple Linear Regression','Log-Linear Regression','Maximum Current Response','Location','northwest')
set(gca,'FontSize',12)

% 幾倍的電壓時間常數取樣第一個值，這裡抓50倍是為了圖面的說明性
baisuu=50;
% 幾倍的電流時間常數做為取樣區間，這裡抓2倍是為了圖面的說明性
interval=2*tau_c;
% 執行兩點線性吻合
[E1,SS,A_line,B_line,E_n]=nitori_ID([tau_v_over_500.T,tau_v_over_500.I],tau_v*baisuu,interval,2,seikaku);
% 執行三點指數吻合
[E2,S,A_log,B_log,C_log,E_n]=santori_ID([tau_v_over_500.T,tau_v_over_500.I],tau_v*baisuu,interval,3,seikaku);
% 得到係數之後，畫出兩條線
t=0:0.001:4;
keisan_line=A_line*t*tau_c+B_line;
keisan_log=A_log+B_log*exp(C_log*t*tau_c);
% 畫出三種方法的取樣方式說明圖
figure(2)
hold on
grid on
plot(...
    tau_v_over_500.T/tau_c,-tau_v_over_500.I,'k',...
    t,-keisan_log,'r--',...
    t,-keisan_line,'b--',...
    [0 2.2],[-min(tau_v_over_500.I) -min(tau_v_over_500.I)],'m--',...
    S(:,1)/tau_c,-S(:,2),'r^',...
    SS(:,1)/tau_c,-SS(:,2),'bv')
xlim([0 2.2]);
ylim([-5E-7 25E-7]);
xlabel('\fontsize{12}Normalized Time(\tau_c)');ylabel('\fontsize{12}Current(A)');
legend('Current Response of True Pulse',...
'Log-Linear Regression Curve',...
'Simple Linear Regression Curve',...
'Maximum Current Response of True Pulse',...
'Location','Southwest')
set(gca,'FontSize',12)





