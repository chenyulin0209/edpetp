%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們演算法的多實驗組數，是不是可以抗雜訊，固定實驗組數，看不同誤差下的效果
細節：

%}

clc;clear;close all;
% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 理想情況下，脈衝振幅相對應應該量測到的電流值
%      p10      n10     n30      n50
Ireal=[-19.0513,4.08347,8.69623,13.3189]*10^(-7);
%       p08       p04      p02     
Ireal2=[-1.6004,-0.76772,-0.30994]*10^(-6);

Ireal_seco=[-5.83]*10^(-7);
% 各電壓相對應電流矩陣數
%             1.0   0.8      0.4     0.2     -1.0  -3.0   -5.0
array_count=[685601, 500494 ,500479, 500479, 57260, 57464, 57705 ];

% 讀資料，這邊電流要用高解析度的電流，因為快速取樣電流抗雜訊的時候，取樣週期很低，
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[T10,I10]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
[T08,I08]=textread([path '0.8V\' content '.txt'],'%f %f','headerlines',1);
[T04,I04]=textread([path '0.4V\' content '.txt'],'%f %f','headerlines',1);
% [T02,I02]=textread([path '0.2V\' content '.txt'],'%f %f','headerlines',1);
% [Tn1,In1]=textread([path '-1.0V\' content '.txt'],'%f %f','headerlines',1);
[Tn3,In3]=textread([path '-3.0V\' content '.txt'],'%f %f','headerlines',1);
[Tn5,In5]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.2*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 快速取樣的取樣週期
rapid_S=15*10^(-6);

x=1;
% 執行次數，越多次平均誤差跟標準差會漸趨穩定
times=1000;

% 不同訊雜比都跑一次
for j=1:times
    count=1;
    for i=[0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 ]*tau_c
%           看要執行幾次/

%           加入雜訊，雜訊大小是用最大電流的%數決定
            gosa=10;
            Inoise=I10+rand(array_count(1),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
            %單組實驗
            [E_rc.p10(j,count),~,~,~,~,~]=santori_ID([T10,Inoise],tau_v*baisuu,i,3,Ireal(1));
%           多組實驗 1.0V
%             [E128rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
%             [E64rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(1));
            [E32rc_p10(j,count),~,A,B,C128rc_p10(j,count),~]=mean_I_ID([T10,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(1));
%           多組實驗 0.8V
            Inoise=I08+rand(array_count(2),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(1));
%             [E64rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(1));
            [E32rc_p08(j,count),~,A,B,C128rc_p08(j,count),~]=mean_I_ID([T08,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(1));
%           多組實驗 0.4V
            Inoise=I04+rand(array_count(3),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(2));
%             [E64rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(2));
            [E32rc_p04(j,count),~,A,B,C128rc_p04(j,count),~]=mean_I_ID([T04,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(2));
% %           多組實驗 0.2V
%             Inoise=I02+rand(array_count(4),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal2(3));
%             [E64rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal2(3));
%             [E32rc_p02(j,count),~,A,B,C128rc_p02(j,count),~]=mean_I_ID([T02,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal2(3));
% %           多組實驗 -1.0V
%             Inoise=In1+rand(array_count(5),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(2));
%             [E64rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(2));
%             [E32rc_n10(j,count),~,A,B,C128rc_n10(j,count),~]=mean_I_ID([Tn1,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(2));
%           多組實驗 -3.0V
            Inoise=In3+rand(array_count(6),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(3));
%             [E64rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(3));
            [E32rc_n30(j,count),~,A,B,C128rc_n30(j,count),~]=mean_I_ID([Tn3,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(3));
%           多組實驗 -5.0V
            Inoise=In5+rand(array_count(7),1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%             [E128rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(4));
%             [E64rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,192,Ireal(4));
            [E32rc_n50(j,count),~,A,B,C128rc_n50(j,count),~]=mean_I_ID([Tn5,Inoise],tau_v*baisuu,i,rapid_S,96,Ireal(4));

        count=count+1;
    end
 
end
x=1;
for i=[0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 ]
%      %   128組實驗的結果，包含電流間常數的估算值 1.0V
%         E128_p10(x,1)=i;
%         E128_p10(x,2)=mean(E128rc_p10(:,x));
%         E128_p10(x,3)=std(E128rc_p10(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 1.0V
%         E64_p10(x,1)=i;
%         E64_p10(x,2)=mean(E64rc_p10(:,x));
%         E64_p10(x,3)=std(E64rc_p10(:,x)); 
    %   32組實驗的結果，包含電流間常數的估算值 1.0V
        E32_p10(x,1)=i;
        E32_p10(x,2)=mean(E32rc_p10(:,x));
        E32_p10(x,3)=std(E32rc_p10(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.8V
%         E128_p08(x,1)=i;
%         E128_p08(x,2)=mean(E128rc_p08(:,x));
%         E128_p08(x,3)=std(E128rc_p08(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.8V
%         E64_p08(x,1)=i;
%         E64_p08(x,2)=mean(E64rc_p08(:,x));
%         E64_p08(x,3)=std(E64rc_p08(:,x)); 
    %   32組實驗的結果，包含電流間常數的估算值 0.8V
        E32_p08(x,1)=i;
        E32_p08(x,2)=mean(E32rc_p08(:,x));
        E32_p08(x,3)=std(E32rc_p08(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.4V
%         E128_p04(x,1)=i;
%         E128_p04(x,2)=mean(E128rc_p04(:,x));
%         E128_p04(x,3)=std(E128rc_p04(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.4V
%         E64_p04(x,1)=i;
%         E64_p04(x,2)=mean(E64rc_p04(:,x));
%         E64_p04(x,3)=std(E64rc_p04(:,x)); 
    %   32組實驗的結果，包含電流間常數的估算值 0.4V
        E32_p04(x,1)=i;
        E32_p04(x,2)=mean(E32rc_p04(:,x));
        E32_p04(x,3)=std(E32rc_p04(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 0.2V
%         E128_p02(x,1)=i;
%         E128_p02(x,2)=mean(E128rc_p02(:,x));
%         E128_p02(x,3)=std(E128rc_p02(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 0.2V
%         E64_p02(x,1)=i;
%         E64_p02(x,2)=mean(E64rc_p02(:,x));
%         E64_p02(x,3)=std(E64rc_p02(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 0.2V
%         E32_p02(x,1)=i;
%         E32_p02(x,2)=mean(E32rc_p02(:,x));
%         E32_p02(x,3)=std(E32rc_p02(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -1.0V
%         E128_n10(x,1)=i;
%         E128_n10(x,2)=mean(E128rc_n10(:,x));
%         E128_n10(x,3)=std(E128rc_n10(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -1.0V
%         E64_n10(x,1)=i;
%         E64_n10(x,2)=mean(E64rc_n10(:,x));
%         E64_n10(x,3)=std(E64rc_n10(:,x)); 
%     %   32組實驗的結果，包含電流間常數的估算值 -1.0V
%         E32_n10(x,1)=i;
%         E32_n10(x,2)=mean(E32rc_n10(:,x));
%         E32_n10(x,3)=std(E32rc_n10(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -3.0V
%         E128_n30(x,1)=i;
%         E128_n30(x,2)=mean(E128rc_n30(:,x));
%         E128_n30(x,3)=std(E128rc_n30(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -3.0V
%         E64_n30(x,1)=i;
%         E64_n30(x,2)=mean(E64rc_n30(:,x));
%         E64_n30(x,3)=std(E64rc_n30(:,x)); 
    %   32組實驗的結果，包含電流間常數的估算值 -3.0V
        E32_n30(x,1)=i;
        E32_n30(x,2)=mean(E32rc_n30(:,x));
        E32_n30(x,3)=std(E32rc_n30(:,x)); 
%      %   128組實驗的結果，包含電流間常數的估算值 -5.0V
%         E128_n50(x,1)=i;
%         E128_n50(x,2)=mean(E128rc_n50(:,x));
%         E128_n50(x,3)=std(E128rc_n50(:,x)); 
%     %   64組實驗的結果，包含電流間常數的估算值 -5.0V
%         E64_n50(x,1)=i;
%         E64_n50(x,2)=mean(E64rc_n50(:,x));
%         E64_n50(x,3)=std(E64rc_n50(:,x)); 
    %   32組實驗的結果，包含電流間常數的估算值 -5.0V
        E32_n50(x,1)=i;
        E32_n50(x,2)=mean(E32rc_n50(:,x));
        E32_n50(x,3)=std(E32rc_n50(:,x)); 
        
    x=x+1;
end
save CurrCuit_200k_1E11_tau_v_0.1674over500_Error_32_181003.2_0.1to0.5_0.2tau_c.mat








