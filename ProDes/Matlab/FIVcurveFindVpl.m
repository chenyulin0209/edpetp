%{
  Vpl :  Ie=Ii 的 (Vpr-Vpl)=-Vpl
%}
function [Vplo]=FIVcurveFindVpl(B,Te)%beta,Te
            k=1.3806488*10^-23;
            e=1.602*10^-19;
            m_e=9.109*10^-31;
            m_i=14*1.66*10^-27;
            
            FI=@(Vpl)sqrt(m_e/m_i)*(1-e*(-Vpl)/(k*Te))^B-exp(e*(-Vpl)/(k*Te));
     Vl=0;Vr=5;Vm=2.5;
     Il=FI(Vl);Ir=FI(Vr);Im=FI(Vm);
     
            
     if Il*Ir>=0 %同號
         Vplo=0;
         msg='FIVcurveFindVpl error 找不到Ii=Ie'
         return;
     end 
     
     count=0;Err=10;
     while(count<=1E5&&Err>=1E-15)
         count=count+1;
         if(Im==0)
             Vplo=Vm;
         elseif(Im*Il>0) %Vm 在 Vl 測，可取代Vl
             Vl=Vm;Il=Im;
         else
             Vr=Vm;
         end
     Err=abs(Im);
     Vm=(Vl+Vr)/2; Im=FI(Vm);
     end
     Vplo=Vm;
     if count>=1E5
         msg='FIVcurveFindVpl error'
     end
end