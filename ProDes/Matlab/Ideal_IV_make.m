% 輸入時間電壓電流，選出所需間隔的V與I %
function [Vout,Iout]=Ideal_IV_make(Tin,Vin,Iin,start,over)
    [~,~,c1]=T_find_D(Tin,Vin,start);
    [~,~,c2]=T_find_D(Tin,Vin,over);
    Iout=Iin(c1:c2);
    Vout=Vin(c1:c2);
end


