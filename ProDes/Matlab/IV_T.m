% 脈波閒置時間最佳化 第二分項實驗 改電子溫度 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\1500K_1E11\充電及放電\定變因\';
content='掃描電壓';
[T1500K.T1,T1500K.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500K.T5,T1500K.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T1500K.T1,T1500K.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500K.T5,T1500K.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T1500K.T1,T1500K.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T1500K.T5,T1500K.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因\';
content='掃描電壓';
[T2000K.T1,T2000K.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000K.T5,T2000K.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2000K.T1,T2000K.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000K.T5,T2000K.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2000K.T1,T2000K.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2000K.T5,T2000K.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\充電及放電\定變因\';
content='掃描電壓';
[T2500K.T1,T2500K.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500K.T5,T2500K.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[T2500K.T1,T2500K.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500K.T5,T2500K.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[T2500K.T1,T2500K.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[T2500K.T5,T2500K.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

st=2.01;
ov=4.9;

[T1500K.IV1(:,1),T1500K.IV1(:,2)]=Ideal_IV_make(T1500K.T1,T1500K.V1,T1500K.I1,st,ov);
[T1500K.IV5(:,1),T1500K.IV5(:,2)]=Ideal_IV_make(T1500K.T5,T1500K.V5,T1500K.I5,st,ov);
[T1500K.IV]=IV_mix(T1500K.IV1,T1500K.IV5);
figure(1)
subplot(2,2,1)
plot(T1500K.T1,T1500K.Vs1,T1500K.T5,T1500K.Vs5)
subplot(2,2,2)
plot(T1500K.T1,T1500K.I1,T1500K.T5,T1500K.I5)
subplot(2,2,3)
plot(T1500K.T1,T1500K.V1,T1500K.T5,T1500K.V5)
subplot(2,2,4)
plot(T1500K.IV1(:,1),T1500K.IV1(:,2),T1500K.IV5(:,1),T1500K.IV5(:,2))

[T2000K.IV1(:,1),T2000K.IV1(:,2)]=Ideal_IV_make(T2000K.T1,T2000K.V1,T2000K.I1,st,ov);
[T2000K.IV5(:,1),T2000K.IV5(:,2)]=Ideal_IV_make(T2000K.T5,T2000K.V5,T2000K.I5,st,ov);
[T2000K.IV]=IV_mix(T2000K.IV1,T2000K.IV5);
figure(2)
subplot(2,2,1)
plot(T2000K.T1,T2000K.Vs1,T2000K.T5,T2000K.Vs5)
subplot(2,2,2)
plot(T2000K.T1,T2000K.I1,T2000K.T5,T2000K.I5)
subplot(2,2,3)
plot(T2000K.T1,T2000K.V1,T2000K.T5,T2000K.V5)
subplot(2,2,4)
plot(T2000K.IV1(:,1),T2000K.IV1(:,2),T2000K.IV5(:,1),T2000K.IV5(:,2))

[T2500K.IV1(:,1),T2500K.IV1(:,2)]=Ideal_IV_make(T2500K.T1,T2500K.V1,T2500K.I1,st,ov);
[T2500K.IV5(:,1),T2500K.IV5(:,2)]=Ideal_IV_make(T2500K.T5,T2500K.V5,T2500K.I5,st,ov);
[T2500K.IV]=IV_mix(T2500K.IV1,T2500K.IV5);
figure(3)
subplot(2,2,1)
plot(T2500K.T1,T2500K.Vs1,T2500K.T5,T2500K.Vs5)
subplot(2,2,2)
plot(T2500K.T1,T2500K.I1,T2500K.T5,T2500K.I5)
subplot(2,2,3)
plot(T2500K.T1,T2500K.V1,T2500K.T5,T2500K.V5)
subplot(2,2,4)
plot(T2500K.IV1(:,1),T2500K.IV1(:,2),T2500K.IV5(:,1),T2500K.IV5(:,2))

figure(4)
% subplot(2,2,1)
grid on
hold on
plot(T1500K.IV(:,1),-T1500K.IV(:,2),T2000K.IV(:,1),-T2000K.IV(:,2),T2500K.IV(:,1),-T2500K.IV(:,2))
% title('\fontsize{12}不同電子溫度下的IV曲線');
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Electron Temperature 1500K','Electron Temperature 2000K','Electron Temperature 2500K','Location','northwest')
set(gca,'FontSize',12)

figure()
subplot(2,2,2)
plot(T1500K.IV1(:,1),T1500K.IV1(:,2),T1500K.IV5(:,1),T1500K.IV5(:,2))
subplot(2,2,3)
plot(T2000K.IV1(:,1),T2000K.IV1(:,2),T2000K.IV5(:,1),T2000K.IV5(:,2))
subplot(2,2,4)
plot(T2500K.IV1(:,1),T2500K.IV1(:,2),T2500K.IV5(:,1),T2500K.IV5(:,2))



