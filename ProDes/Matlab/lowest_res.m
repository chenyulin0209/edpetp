% 最低解析度，確認所有溫濃皆適用 %

clc;clear;close all;

% path='C:\Users\eric\Desktop\MATLAB交接\數據\';
% DT='1500K_1E11\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T1500D1E11.T,wave.T1500D1E11.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T1500D1E11.T,wave.T1500D1E11.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T1500D1E11.T,wave.T1500D1E11.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% DT='2000K_1E9\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T2000D1E9.T,wave.T2000D1E9.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T2000D1E9.T,wave.T2000D1E9.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T2000D1E9.T,wave.T2000D1E9.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% DT='2000K_1E10\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T2000D1E10.T,wave.T2000D1E10.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T2000D1E10.T,wave.T2000D1E10.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T2000D1E10.T,wave.T2000D1E10.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% DT='2000K_1E11\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T2000D1E11.T,wave.T2000D1E11.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T2000D1E11.T,wave.T2000D1E11.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T2000D1E11.T,wave.T2000D1E11.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% DT='2000K_1E12\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T2000D1E12.T,wave.T2000D1E12.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T2000D1E12.T,wave.T2000D1E12.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T2000D1E12.T,wave.T2000D1E12.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% DT='2500K_1E11\漸升式三角波\離2過2\';
% content='掃描電壓';
% [wave.T2500D1E11.T,wave.T2500D1E11.Vs]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='迴路電流';
% [wave.T2500D1E11.T,wave.T2500D1E11.I]=textread([path DT content '.txt'],'%f %f','headerlines',1);
% content='汙染跨壓';
% [wave.T2500D1E11.T,wave.T2500D1E11.Vc]=textread([path DT content '.txt'],'%f %f','headerlines',1);


load lowest_res wave

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

disp(['數據讀取完畢'])

[wave.T1500D1E11.RI,wave.T1500D1E11.RT,wave.T1500D1E11.amp,wave.T1500D1E11.NtoP,wave.T1500D1E11.PN]=FindPulseStart(wave.T1500D1E11.T,wave.T1500D1E11.Vs);
[wave.T2000D1E9.RI,wave.T2000D1E9.RT,wave.T2000D1E9.amp,wave.T2000D1E9.NtoP,wave.T2000D1E9.PN]=FindPulseStart(wave.T2000D1E9.T,wave.T2000D1E9.Vs);
[wave.T2000D1E10.RI,wave.T2000D1E10.RT,wave.T2000D1E10.amp,wave.T2000D1E10.NtoP,wave.T2000D1E10.PN]=FindPulseStart(wave.T2000D1E10.T,wave.T2000D1E10.Vs);
[wave.T2000D1E11.RI,wave.T2000D1E11.RT,wave.T2000D1E11.amp,wave.T2000D1E11.NtoP,wave.T2000D1E11.PN]=FindPulseStart(wave.T2000D1E11.T,wave.T2000D1E11.Vs);
[wave.T2000D1E12.RI,wave.T2000D1E12.RT,wave.T2000D1E12.amp,wave.T2000D1E12.NtoP,wave.T2000D1E12.PN]=FindPulseStart(wave.T2000D1E12.T,wave.T2000D1E12.Vs);
[wave.T2500D1E11.RI,wave.T2500D1E11.RT,wave.T2500D1E11.amp,wave.T2500D1E11.NtoP,wave.T2500D1E11.PN]=FindPulseStart(wave.T2500D1E11.T,wave.T2500D1E11.Vs);


tau_c=0.02;
tau_c=0.5;
tau_v=0.0001;
baisuu=7;
interval=0.4*tau_c;
dot=192;
dot=48;
x=1;


g20=2.96E-9;
g100=5.92E-10;
% gosa=0;
kai=1;

for j=1:kai
    wave.T1500D1E11.Inoise=wave.T1500D1E11.I+rand(size(wave.T1500D1E11.I,1),1)*g100*2-g100;
    wave.T2000D1E9.Inoise=wave.T2000D1E9.I+rand(size(wave.T2000D1E9.I,1),1)*g100*2-g100;
    wave.T2000D1E10.Inoise=wave.T2000D1E10.I+rand(size(wave.T2000D1E10.I,1),1)*g100*2-g100;
    wave.T2000D1E11.Inoise=wave.T2000D1E11.I+rand(size(wave.T2000D1E11.I,1),1)*g100*2-g100;
    wave.T2000D1E12.Inoise=wave.T2000D1E12.I+rand(size(wave.T2000D1E12.I,1),1)*g20*2-g20;
    wave.T2500D1E11.Inoise=wave.T2500D1E11.I+rand(size(wave.T2500D1E11.I,1),1)*g100*2-g100;

    
    tau_c=0.1722;
    

    dot=384;
    tau_c=0.17;
    [wave.T1500D1E11.IV_rc(1,:),wave.T1500D1E11.IV_rc(2,:)]=Fit_I(wave.T1500D1E11.T,wave.T1500D1E11.amp,wave.T1500D1E11.Inoise,wave.T1500D1E11.PN,wave.T1500D1E11.RT,wave.T1500D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.5;
    [wave.T2000D1E9.IV_rc(1,:),wave.T2000D1E9.IV_rc(2,:)]=Fit_I(wave.T2000D1E9.T,wave.T2000D1E9.amp,wave.T2000D1E9.Inoise,wave.T2000D1E9.PN,wave.T2000D1E9.RT,wave.T2000D1E9.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.44;
    [wave.T2000D1E10.IV_rc(1,:),wave.T2000D1E10.IV_rc(2,:)]=Fit_I(wave.T2000D1E10.T,wave.T2000D1E10.amp,wave.T2000D1E10.Inoise,wave.T2000D1E10.PN,wave.T2000D1E10.RT,wave.T2000D1E10.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.17;
    [wave.T2000D1E11.IV_rc(1,:),wave.T2000D1E11.IV_rc(2,:)]=Fit_I(wave.T2000D1E11.T,wave.T2000D1E11.amp,wave.T2000D1E11.Inoise,wave.T2000D1E11.PN,wave.T2000D1E11.RT,wave.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.5;
    [wave.T2000D1E12.IV_rc(1,:),wave.T2000D1E12.IV_rc(2,:)]=Fit_I(wave.T2000D1E12.T,wave.T2000D1E12.amp,wave.T2000D1E12.Inoise,wave.T2000D1E12.PN,wave.T2000D1E12.RT,wave.T2000D1E12.RI,baisuu*tau_v,0.4*tau_c,dot);
    tau_c=0.17;
    [wave.T2500D1E11.IV_rc(1,:),wave.T2500D1E11.IV_rc(2,:)]=Fit_I(wave.T2500D1E11.T,wave.T2500D1E11.amp,wave.T2500D1E11.Inoise,wave.T2500D1E11.PN,wave.T2500D1E11.RT,wave.T2500D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);

    
    [T.T1500D1E11.rc(j),D.T1500D1E11.rc(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T1500D1E11.IV_rc(1,:),wave.T1500D1E11.IV_rc(2,:));
    [T.T2000D1E9.rc(j),D.T2000D1E9.rc(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E9.IV_rc(1,:),wave.T2000D1E9.IV_rc(2,:));
    [T.T2000D1E10.rc(j),D.T2000D1E10.rc(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E10.IV_rc(1,:),wave.T2000D1E10.IV_rc(2,:));
    [T.T2000D1E11.rc(j),D.T2000D1E11.rc(j),Vrc,Ierc,Iirc]=ronbunTD(wave.T2000D1E11.IV_rc(1,:),wave.T2000D1E11.IV_rc(2,:));
    [T.T2000D1E12.rc(j),D.T2000D1E12.rc(j),~,~,~]=ronbunTD(wave.T2000D1E12.IV_rc(1,:),wave.T2000D1E12.IV_rc(2,:));
    [T.T2500D1E11.rc(j),D.T2500D1E11.rc(j),~,~,~]=ronbunTD(wave.T2500D1E11.IV_rc(1,:),wave.T2500D1E11.IV_rc(2,:));
    
    
    % function [aTe,aNe,Vfix,aIe1,aIi1]=ronbunTD(V,I)


    disp(['執行次數  ' num2str(j) '/' num2str(kai) '     進度' num2str(j/kai*100) '%' ])


end

disp(['512點'])
disp(['T1500D1E11 溫度' num2str(T.T1500D1E11.rc) '   濃度' num2str(D.T1500D1E11.rc) ])
disp(['T2000D1E9 溫度' num2str(T.T2000D1E9.rc) '   濃度' num2str(D.T2000D1E9.rc) ])
disp(['T2000D1E10 溫度' num2str(T.T2000D1E10.rc) '   濃度' num2str(D.T2000D1E10.rc) ])
disp(['T2000D1E11 溫度' num2str(T.T2000D1E11.rc) '   濃度' num2str(D.T2000D1E11.rc) ])
disp(['T2000D1E12 溫度' num2str(T.T2000D1E12.rc) '   濃度' num2str(D.T2000D1E12.rc) ])
disp(['T2500D1E11 溫度' num2str(T.T2500D1E11.rc) '   濃度' num2str(D.T2500D1E11.rc) ])



% save lowest_res_result T D



% figure()
% plot(wave.T1500D1E11.T,wave.T1500D1E11.Inoise)
% figure()
% plot(wave.T2000D1E9.T,wave.T2000D1E9.Inoise)
% figure()
% plot(wave.T2000D1E10.T,wave.T2000D1E10.Inoise)
% figure()
% plot(wave.T2000D1E11.T,wave.T2000D1E11.Inoise)
% figure()
% plot(wave.T2000D1E12.T,wave.T2000D1E12.Inoise)
% figure()
% plot(wave.T2500D1E11.T,wave.T2500D1E11.Inoise)



% figure()
% subplot(2,2,1)
% hold on
% plot(perfect.Vs,-perfect.I)
% plot(Vrc,-Iirc,'rx-')


% m=((Iirc(2)-Iirc(1))/(Vrc(2)-Vrc(1)))
% a=Iirc(2)-m*Vrc(2);
% V=-5:0.01:1;
% y=m*V+a;
% plot(V,-y,'r-')



% subplot(4,2,2)
% plot(wave.N4.T2000D1E11.dot16.IV_rc(1,:),wave.N4.T2000D1E11.dot16.IV_rc(2,:),perfect.Vs,perfect.I)
% 
% 
% [Vpl]=FIVcurveFindVpl(1,2000)
% Vss=-5:0.01:0.41;
% [Iee,Iii,III]=DebyeCurrentSim(Vss,Vpl,1E11,1E11,2000,58.2E-4,1);
% 
% subplot(4,2,1)
% plot(Vrc,-Ierc-Iirc,'bo-',Vss,-III,'g')
% title('電流')
% legend('指數吻合','理想','location','northwest')
% xlim([-6 2])
% grid on
% 
% subplot(4,2,3)
% plot(Vrc,-Iirc,'bo-',Vss,-Iii,'g')
% title('離子流')
% legend('指數吻合','理想','location','northwest')
% xlim([-6 2])
% grid on
% 
% subplot(4,2,5)
% plot(Vrc,-Ierc,'bo-',Vss,-Iee,'g')
% title('電子流')
% legend('指數吻合','理想','location','northwest')
% xlim([-6 2])
% grid on
% 
% subplot(4,2,7)
% plot(Vrc,log(-Ierc),'bo-',Vss,log(-Iee),'g')
% title('ln(電子流)')
% legend('指數吻合','理想','location','northwest')
% xlim([-6 2])
% grid on











