%{
目的：
用指數吻合法，取樣非理想脈衝響應電流，估算不受汙染層影響的理想脈衝響應電流值
看我們演算法擷取的ＩＶ曲線長什麼樣子
細節：

%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 電壓時間常數
tau_v=0.001;

% 讀資料
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\漸升式三角波\';
content='掃描電壓';
[wave.T,wave.Vs]=textread([path '-5.1至1間隔0.1\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.T,wave.I]=textread([path '-5.1至1間隔0.1\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[~,wave.Vc]=textread([path '-5.1至1間隔0.1\' content '.txt'],'%f %f','headerlines',1);
% 讀理想的IV曲線，和擷取的做比較
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.4*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;

% 執行次數，越多次平均誤差跟標準差會漸趨穩定
kai=1;
% 訊雜比
gosa=10;

% 輸入掃描脈波，自動偵測脈波起始點編號，脈波起始點時間，脈衝大小，脈衝由負轉正的編號，總脈波數
[wave.RI,wave.RT,wave.amp,wave.NtoP,wave.PN]=FindPulseStart(wave.T,wave.Vs);
% 擷取好幾次IV曲線
for i=1:kai
%   加上雜訊
    wave.Inoise=wave.I+rand(903996,1)*abs(min(wave.I))*gosa/100-abs(min(wave.I))*gosa/100/2;
%   用指數吻合法取IV曲線
    [wave.IV_rc(1,:),wave.IV_rc(2,:)]=Fit_I(wave.T,wave.amp,wave.Inoise,wave.PN,wave.RT,wave.RI,baisuu*tau_v,interval,384);
%   用最大值法取IV曲線
    [wave.IV_ma(1,:),wave.IV_ma(2,:)]=Max_I(wave.T,wave.amp,wave.Inoise,wave.PN,wave.RT,wave.RI);
%   存下各個輸入脈衝的相應電流值(最大值法)
    error.p10.ma(i)=wave.IV_ma(2,63);
    error.p05.ma(i)=wave.IV_ma(2,58);
    error.n05.ma(i)=wave.IV_ma(2,47);
    error.n10.ma(i)=wave.IV_ma(2,42);
    error.n15.ma(i)=wave.IV_ma(2,37);
    error.n20.ma(i)=wave.IV_ma(2,32);
    error.n25.ma(i)=wave.IV_ma(2,27);
    error.n30.ma(i)=wave.IV_ma(2,22);
    error.n35.ma(i)=wave.IV_ma(2,17);
    error.n40.ma(i)=wave.IV_ma(2,12);
    error.n45.ma(i)=wave.IV_ma(2,7);
    error.n50.ma(i)=wave.IV_ma(2,2);
%   存下各個輸入脈衝的相應電流值(指數吻合法)
    error.p10.rc(i)=wave.IV_rc(2,63);
    error.p05.rc(i)=wave.IV_rc(2,58);
    error.n05.rc(i)=wave.IV_rc(2,47);
    error.n10.rc(i)=wave.IV_rc(2,42);
    error.n15.rc(i)=wave.IV_rc(2,37);
    error.n20.rc(i)=wave.IV_rc(2,32);
    error.n25.rc(i)=wave.IV_rc(2,27);
    error.n30.rc(i)=wave.IV_rc(2,22);
    error.n35.rc(i)=wave.IV_rc(2,17);
    error.n40.rc(i)=wave.IV_rc(2,12);
    error.n45.rc(i)=wave.IV_rc(2,7);
    error.n50.rc(i)=wave.IV_rc(2,2);
    
end

% 計算各個輸入脈衝的平均電流值(最大值法)
error.p10.ma_mean=-mean(error.p10.ma(:));
error.p05.ma_mean=-mean(error.p05.ma(:));
error.n05.ma_mean=-mean(error.n05.ma(:));
error.n10.ma_mean=-mean(error.n10.ma(:));
error.n15.ma_mean=-mean(error.n15.ma(:));
error.n20.ma_mean=-mean(error.n20.ma(:));
error.n25.ma_mean=-mean(error.n25.ma(:));
error.n30.ma_mean=-mean(error.n30.ma(:));
error.n35.ma_mean=-mean(error.n35.ma(:));
error.n40.ma_mean=-mean(error.n40.ma(:));
error.n45.ma_mean=-mean(error.n45.ma(:));
error.n50.ma_mean=-mean(error.n50.ma(:));
% 計算各個輸入脈衝的電流標準差(最大值法)
error.p10.ma_std=-std(error.p10.ma(:));
error.p05.ma_std=-std(error.p05.ma(:));
error.n05.ma_std=-std(error.n05.ma(:));
error.n10.ma_std=-std(error.n10.ma(:));
error.n15.ma_std=-std(error.n15.ma(:));
error.n20.ma_std=-std(error.n20.ma(:));
error.n25.ma_std=-std(error.n25.ma(:));
error.n30.ma_std=-std(error.n30.ma(:));
error.n35.ma_std=-std(error.n35.ma(:));
error.n40.ma_std=-std(error.n40.ma(:));
error.n45.ma_std=-std(error.n45.ma(:));
error.n50.ma_std=-std(error.n50.ma(:));
% 計算各個輸入脈衝的平均電流值(指數吻合法)
error.p10.rc_mean=-mean(error.p10.rc(:));
error.p05.rc_mean=-mean(error.p05.rc(:));
error.n05.rc_mean=-mean(error.n05.rc(:));
error.n10.rc_mean=-mean(error.n10.rc(:));
error.n15.rc_mean=-mean(error.n15.rc(:));
error.n20.rc_mean=-mean(error.n20.rc(:));
error.n25.rc_mean=-mean(error.n25.rc(:));
error.n30.rc_mean=-mean(error.n30.rc(:));
error.n35.rc_mean=-mean(error.n35.rc(:));
error.n40.rc_mean=-mean(error.n40.rc(:));
error.n45.rc_mean=-mean(error.n45.rc(:));
error.n50.rc_mean=-mean(error.n50.rc(:));
% 計算各個輸入脈衝的電流標準差(指數吻合法)
error.p10.rc_std=-std(error.p10.rc(:));
error.p05.rc_std=-std(error.p05.rc(:));
error.n05.rc_std=-std(error.n05.rc(:));
error.n10.rc_std=-std(error.n10.rc(:));
error.n15.rc_std=-std(error.n15.rc(:));
error.n20.rc_std=-std(error.n20.rc(:));
error.n25.rc_std=-std(error.n25.rc(:));
error.n30.rc_std=-std(error.n30.rc(:));
error.n35.rc_std=-std(error.n35.rc(:));
error.n40.rc_std=-std(error.n40.rc(:));
error.n45.rc_std=-std(error.n45.rc(:));
error.n50.rc_std=-std(error.n50.rc(:));


% 畫出兩種IV曲線擷取比較圖
figure()
hold on
errorbar(...
    [-5 -4.5 -4 -3.5 -3 -2.5 -2 -1.5 -1 -0.5 0 0.5 1],...
    [
    error.n50.rc_mean,...
    error.n45.rc_mean,...
    error.n40.rc_mean,...
    error.n35.rc_mean,...
    error.n30.rc_mean,...
    error.n25.rc_mean,...
    error.n20.rc_mean,...
    error.n15.rc_mean,...
    error.n10.rc_mean,...
    error.n05.rc_mean,...
    0,...
    error.p05.rc_mean,...
    error.p10.rc_mean],...
    [
    error.n50.rc_std,...
    error.n45.rc_std,...
    error.n40.rc_std,...
    error.n35.rc_std,...
    error.n30.rc_std,...
    error.n25.rc_std,...
    error.n20.rc_std,...
    error.n15.rc_std,...
    error.n10.rc_std,...
    error.n05.rc_std,...
    0,...
    error.p05.rc_std,...
    error.p10.rc_std],'r')
errorbar(...
    [-5 -4.5 -4 -3.5 -3 -2.5 -2 -1.5 -1 -0.5 0 0.5 1],...
    [
    error.n50.ma_mean,...
    error.n45.ma_mean,...
    error.n40.ma_mean,...
    error.n35.ma_mean,...
    error.n30.ma_mean,...
    error.n25.ma_mean,...
    error.n20.ma_mean,...
    error.n15.ma_mean,...
    error.n10.ma_mean,...
    error.n05.ma_mean,...
    0,...
    error.p05.ma_mean,...
    error.p10.ma_mean],...
    [
    error.n50.ma_std,...
    error.n45.ma_std,...
    error.n40.ma_std,...
    error.n35.ma_std,...
    error.n30.ma_std,...
    error.n25.ma_std,...
    error.n20.ma_std,...
    error.n15.ma_std,...
    error.n10.ma_std,...
    error.n05.ma_std,...
    0,...
    error.p05.ma_std,...
    error.p10.ma_std],'g')
plot(perfect.Vs,-perfect.I,'b')
xlabel('\fontsize{12}Probe Voltage(V)');ylabel('\fontsize{12}Current(A)');
legend('Log-Linear Regression','Maximum Current Algorithm','Desired Value','location','northwest')
xlim([-5.1 1])
set(gca,'FontSize',12)
grid on


