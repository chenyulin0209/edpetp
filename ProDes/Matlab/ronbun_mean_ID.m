function[SampleValue,A1,B1,C,result]=ronbun_mean_ID(Data,IDdot,StartTime,SamplePeriod,minPeriod,Tensuu,kasoku)
    %PSPICE模擬出的總點數
    DataLength=size(Data);
    %取樣開始時間
    SampleTime=StartTime;
    %取樣點數
    DotNumber=Tensuu;

    index=1;
    for i=0:DotNumber/3
        for j=0:DotNumber/3-1
            SPdot(index)=StartTime+i*SamplePeriod/2+j*minPeriod;
            index=index+1;
        end
    end

    %找到取樣的點數在矩陣中的編號
    for i=1:DotNumber
        for j=kasoku:DataLength(1)
            if Data(j,1)>=SPdot(i)
                t(i)=Data(j,1)-IDdot;
                I(i)=Data(j,2);
                SampleValue=[t(i)+IDdot,I(i)];
                break
            end
        end
    end

    

    t1=mean(t(1:DotNumber/3));
    t2=mean(t(DotNumber/3+1:2*DotNumber/3));
    t3=mean(t(2*DotNumber/3+1:3*DotNumber/3));
    I1=mean(I(1:DotNumber/3));
    I2=mean(I(DotNumber/3+1:2*DotNumber/3));
    I3=mean(I(2*DotNumber/3+1:3*DotNumber/3));
    
    C=C_keisan([t1 t2 t3],[I1 I2 I3],SamplePeriod/2);

    %透過取樣點的編號找到相對應的電流值存進矩陣
    for i=1:Tensuu
        Y(i,1)=I(i);
        A(i,1)=1;
        A(i,2)=exp(C*t(i));
    end

    %最小方差法
    hh=inv(A'*A)*A'*Y;
    ID_result=hh;
    A1=real(hh(1));
    B1=real(hh(2));

%     result=A1+B1*exp(C*IDdot);
    result=A1+B1;

end