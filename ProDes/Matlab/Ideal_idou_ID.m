
% 閒置曲線ID %
%{
    目的: 電壓放電曲線 擷取點 並用__法求出AB 回歸曲線 
    輸入: 
        Data: 模擬 IT 數據
        StartTime:  擷取點起始延遲
        Delaytime:  擷取點時間(x倍tau_v)
        dt: 擷取點時間(x倍tau_c)
        Density: 電子濃度
        Tensuu: 未知
        pn: 輸入電壓
    輸出:
        dot: 時間、電壓、電流
        A1: Y=A*exp(Bt) 式中 A
        B1: Y=A*exp(Bt) 式中 B
%}
function[dot,A1,B1]=Ideal_idou_ID_(Data ,StartTime ,Delaytime , dt, Density, Tensuu, pn)
    %{
        dt -> 下降取擷取點 = StartTime+Delaytime + n * dt
    %}
    n=2;

    load('ideal_IV_table.mat');
%{
    path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\';
    content='迴路電流';
    [~,Guess.IV(:,1)]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
    content='掃描電壓';
    [~,Guess.IV(:,2)]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

    %%%%% 預估線最大電流 用濃度去推IV取線 %%%%%%
    Guess.IV(:,1)=Guess.IV(:,1)/1E12*Density;
%}
    if pn==1
        % 起始時間電流%
        [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Delaytime);
        %在IV取線上找到對應電壓% -> 最高電壓
        [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),Guess.IV(:,2),dot1(2));
        %
        dot(1,1)=dot1(1);
        dot(1,2)=dot1(2);
        dot(1,3)=dot1(3);
        
        for i=1:n
             [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Delaytime+i*dt);
            [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),Guess.IV(:,2),dot1(2));
            dot(i+1,1)=dot1(1);
            dot(i+1,2)=dot1(2);
            dot(i+1,3)=dot1(3);
        end
        
        %{
        %[]                            [          相對應電壓       ] 最高電壓衰減一半
        [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),-dot1(3)/2);
        [dot2(1),~,~]=D_find_T(Data(:,1),-Data(:,2),dot2(2));
        dot(2,1)=dot2(1);
        dot(2,2)=-dot2(2); 
        dot(2,3)=dot2(3);
        %}
    end
    if pn==5
        [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Delaytime);
        [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),Guess.IV(:,2),dot1(2));
        dot(1,1)=dot1(1);
        dot(1,2)=dot1(2);
        dot(1,3)=dot1(3);
        
        for i=1:n
             [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Delaytime+i*dt);
            [~,dot1(3),~]=T_find_D(-Guess.IV(:,1),Guess.IV(:,2),dot1(2));
            dot(i+1,1)=dot1(1);
            dot(i+1,2)=dot1(2);
            dot(i+1,3)=dot1(3);
        end
        
        
        %{
        [dot2(2),dot2(3),~]=D_find_T(-Guess.IV(:,1),Guess.IV(:,2),dot1(3)/2);
        [dot2(1),~,~]=D_find_T(Data(:,1),-Data(:,2),dot2(2));
        dot(2,1)=dot2(1);
        dot(2,2)=-dot2(2);
        dot(2,3)=dot2(3);
        %}
    end

%     for i=1:2
%         [SampleV(i,1),SampleV(i,2),~]=T_find_D(-Guess.IV(:,1),-Guess.IV(:,2),-SampleI(i,2));
%     end
%     SampleV;
%     
%     subplot(2,2,4)
%     hold on
%     plot(Guess.IV(:,2),Guess.IV(:,1))
%     plot(dot(:,3),dot(:,2),'ro')
    
    
    for i=1:n+1
        A(i,1)=1;
        A(i,2)=dot(i,1)-StartTime;
        Y(i,1)=log(dot(i,3));
    end
    
    hh=inv(A'*A)*A'*Y;
    ID_result=real(hh);
    %181026 修改 原:exp(real(hh(1)));
    A1=real(exp(hh(1)));
    B1=real(hh(2));


end






