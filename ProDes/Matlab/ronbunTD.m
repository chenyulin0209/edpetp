function [aTe,aNe,Vfix,aIe1,aIi1]=ronbunTD(V,I)

    k=1.3806488*10^-23;
    e=1.602*10^-19;
    A=58.2*10^(-4);
    m_e=9.109*10^-31;
    m_i=14*1.66*10^-27;
    beta=1;
    Ar=6;

    Ttmp=1500; %疊代溫度 初始猜測
    Tp=100;    %Tstep 每次猜測移動的距離
    count=0;
    DETe(1)=0; %估算誤差差
    threshold =1E-2;
        
for times=1:40    % 帶入各電子溫度資料 與估算結果
        % 找 Vpr-Vpl>0 區域
        for i=1:size(V,1)-1
          if(V(i)<=-0 && V(i+1)>=-0)
            V_0=i;
          end
          if(V(i)<=2 && V(i+1)>=2)
            V_3=i;
          end
        end
%         Sample1=V_0:600;
%         Sample2=V_0:V_3; % 重建區域
        
        % 利用多項式逼進模行 Mpara，從Vscan 預估 debye 跨壓
        %使用 Sample1=505:600; %Data只在Vpr-Vpl>0處左右修正
        
        aVpl=FIVcurveFindVpl(beta,Ttmp);
        Vfix=FDDmodVdcal(V,aVpl,beta,Ttmp,Ar); %求Debye 預估分壓 (Vscan,Vpl,beta,Te,Ar)
        Vf1=length(V);           % Vf1 :I-V curve 計算Te,ne用的 data右極限 
        for i=1:size(V,2)-1
          if(Vfix(i)<aVpl && Vfix(i+1)>=aVpl)  % Vfix 找到電漿電壓所在simple 為Vf1
            Vf1=i;
          end
        end
        
        [aIi1,aIe1]=FIVcurveCalIe(I,Vfix);%以修正 IV curve 取前半段 求離子電流
        
%         figure(5)
%         plot(Vfix,aIi1)
        
%         plot(Vfix,log(aIe1))

        
        for i=1:length(aIe1)-1               % Vf0 :I-V curve 計算Te,ne用的 data左極限
          if((-aIe1(i)-aIi1(i))<0 && (-aIe1(i+1)-aIi1(i+1))>=0)
%           if(-aIe1(i)<5E-11 && -aIe1(i+1)>=5E-11)
            Vf0=i+1;
          end
        end
        count=count+1;
        aIe1(Vf0:Vf1);
        Vfix(Vf0:Vf1);
        
        [aTe,aNe]=FIVcurveFindTe(aIe1(Vf0:Vf1),Vfix(Vf0:Vf1),aVpl);
        %計算 估計誤差
        plT(count)=aTe;
        ETe(count)=abs(aTe-Ttmp); % 估算誤差
        if(count==1)% 初次疊代
          Ttmp=Ttmp+Tp;
        else
          DETe(count)=ETe(count)-ETe(count-1);  
          if  (abs(DETe(count))<threshold )% 小於門檻 跳出
              Dleng=length(plT);
              break;
          end
          if(DETe(count)>0) %估算誤差增加, 往反方向走
            Tp=-Tp;
            Tp=Tp/2;
          end
        end
        
        Ttmp=Ttmp+Tp; %更新猜測溫度;
end        

% close
% figure(3)
% plot(Vfix,log(aIe1))
aTe;
aNe;


end


   
       