%{
目的：
使用指數吻合法，分別擷取不同解析度的IV曲線，再使用同樣的溫濃演算法，計算出溫濃
這邊是改離子區解析度，固定過渡區解析度
細節：

%}
clc;clear;close all;

% 讀資料
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\';
DT='2000K_1E11\漸升式三角波\';
dot_number='離2過10\';
content='掃描電壓';
[wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.Vs]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.I]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.Vc]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
dot_number='離4過10\';
content='掃描電壓';
[wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.Vs]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.I]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.Vc]=textread([path DT dot_number content '.txt'],'%f %f','headerlines',1);
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\';
content='掃描電壓';
[perfect.T,perfect.Vs]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[perfect.T,perfect.I]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);

% 可以用下面這個讀取資料，用上面的方法，每跑一次程式都要重新讀資料，速度又慢，因此使用以下的方法
load Ii_res_data wave
disp(['數據讀取完畢'])

% 輸入不同解析度的掃描脈波，自動偵測脈波起始點編號，脈波起始點時間，脈衝大小，脈衝由負轉正的編號，總脈波數
[wave.N2.T2000D1E11.RI,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.NtoP,wave.N2.T2000D1E11.PN]=FindPulseStart(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.Vs);
[wave.N4.T2000D1E11.RI,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.NtoP,wave.N4.T2000D1E11.PN]=FindPulseStart(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.Vs);

% 電壓時間常數
tau_v=0.001;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 因為我函式沒寫好，用Fit_I的時候，實驗組數要乘上3，所以這裡實際上是16組指數函數法
dot=48;
% 訊雜比
gosa=0;
% 執行次數，越多次平均誤差跟標準差會漸趨穩定
kai=1;

% 執行好幾次溫濃計算
for j=1:kai
%   加上雜訊
    wave.N2.T2000D1E11.Inoise=wave.N2.T2000D1E11.I+rand(size(wave.N2.T2000D1E11.I,1),1)*abs(min(wave.N2.T2000D1E11.I))*gosa/100-abs(min(wave.N2.T2000D1E11.I))*gosa/100/2;
    wave.N4.T2000D1E11.Inoise=wave.N4.T2000D1E11.I+rand(size(wave.N4.T2000D1E11.I,1),1)*abs(min(wave.N4.T2000D1E11.I))*gosa/100-abs(min(wave.N4.T2000D1E11.I))*gosa/100/2;

%   電流時間常數
    tau_c=0.1722;
    
%   因為我函式沒寫好，用Fit_I的時候，實驗組數要乘上3，所以這裡實際上是8組指數函數法，以此類推
    dot=24;
    [wave.N2.T2000D1E11.dot16.IV_rc(1,:),wave.N2.T2000D1E11.dot16.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=12;
    [wave.N4.T2000D1E11.dot16.IV_rc(1,:),wave.N4.T2000D1E11.dot16.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    
    dot=48;
    [wave.N2.T2000D1E11.dot32.IV_rc(1,:),wave.N2.T2000D1E11.dot32.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=24;
    [wave.N4.T2000D1E11.dot32.IV_rc(1,:),wave.N4.T2000D1E11.dot32.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);

    dot=96;
    [wave.N2.T2000D1E11.dot64.IV_rc(1,:),wave.N2.T2000D1E11.dot64.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=48;
    [wave.N4.T2000D1E11.dot64.IV_rc(1,:),wave.N4.T2000D1E11.dot64.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);

    dot=192;
    [wave.N2.T2000D1E11.dot128.IV_rc(1,:),wave.N2.T2000D1E11.dot128.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=96;
    [wave.N4.T2000D1E11.dot128.IV_rc(1,:),wave.N4.T2000D1E11.dot128.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);

    dot=384;
    [wave.N2.T2000D1E11.dot256.IV_rc(1,:),wave.N2.T2000D1E11.dot256.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=192;
    [wave.N4.T2000D1E11.dot256.IV_rc(1,:),wave.N4.T2000D1E11.dot256.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);

    dot=768;
    [wave.N2.T2000D1E11.dot512.IV_rc(1,:),wave.N2.T2000D1E11.dot512.IV_rc(2,:)]=Fit_I(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.amp,wave.N2.T2000D1E11.Inoise,wave.N2.T2000D1E11.PN,wave.N2.T2000D1E11.RT,wave.N2.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);
    dot=384;
    [wave.N4.T2000D1E11.dot512.IV_rc(1,:),wave.N4.T2000D1E11.dot512.IV_rc(2,:)]=Fit_I(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.amp,wave.N4.T2000D1E11.Inoise,wave.N4.T2000D1E11.PN,wave.N4.T2000D1E11.RT,wave.N4.T2000D1E11.RI,baisuu*tau_v,0.4*tau_c,dot);


%   使用指數函數法得到的IV曲線，計算出溫濃
    [T.N2.T2000D1E11.dot16.rc(j),D.N2.T2000D1E11.dot16.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot16.IV_rc(1,:),wave.N2.T2000D1E11.dot16.IV_rc(2,:));
    [T.N4.T2000D1E11.dot16.rc(j),D.N4.T2000D1E11.dot16.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot16.IV_rc(1,:),wave.N4.T2000D1E11.dot16.IV_rc(2,:));
    
    [T.N2.T2000D1E11.dot32.rc(j),D.N2.T2000D1E11.dot32.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot32.IV_rc(1,:),wave.N2.T2000D1E11.dot32.IV_rc(2,:));
    [T.N4.T2000D1E11.dot32.rc(j),D.N4.T2000D1E11.dot32.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot32.IV_rc(1,:),wave.N4.T2000D1E11.dot32.IV_rc(2,:));
    
    [T.N2.T2000D1E11.dot64.rc(j),D.N2.T2000D1E11.dot64.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot64.IV_rc(1,:),wave.N2.T2000D1E11.dot64.IV_rc(2,:));
    [T.N4.T2000D1E11.dot64.rc(j),D.N4.T2000D1E11.dot64.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot64.IV_rc(1,:),wave.N4.T2000D1E11.dot64.IV_rc(2,:));

    [T.N2.T2000D1E11.dot128.rc(j),D.N2.T2000D1E11.dot128.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot128.IV_rc(1,:),wave.N2.T2000D1E11.dot128.IV_rc(2,:));
    [T.N4.T2000D1E11.dot128.rc(j),D.N4.T2000D1E11.dot128.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot128.IV_rc(1,:),wave.N4.T2000D1E11.dot128.IV_rc(2,:));

    [T.N2.T2000D1E11.dot256.rc(j),D.N2.T2000D1E11.dot256.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot256.IV_rc(1,:),wave.N2.T2000D1E11.dot256.IV_rc(2,:));
    [T.N4.T2000D1E11.dot256.rc(j),D.N4.T2000D1E11.dot256.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot256.IV_rc(1,:),wave.N4.T2000D1E11.dot256.IV_rc(2,:));

    [T.N2.T2000D1E11.dot512.rc(j),D.N2.T2000D1E11.dot512.rc(j),~,~]=ronbunTD(wave.N2.T2000D1E11.dot512.IV_rc(1,:),wave.N2.T2000D1E11.dot512.IV_rc(2,:));
    [T.N4.T2000D1E11.dot512.rc(j),D.N4.T2000D1E11.dot512.rc(j),~,~]=ronbunTD(wave.N4.T2000D1E11.dot512.IV_rc(1,:),wave.N4.T2000D1E11.dot512.IV_rc(2,:));
    

    disp(['執行次數  ' num2str(j) '/' num2str(kai) '     進度' num2str(j/kai*100) '%' ])

end

% 看結果
disp(['16點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot16.rc) '   濃度' num2str(D.N2.T2000D1E11.dot16.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot16.rc) '   濃度' num2str(D.N4.T2000D1E11.dot16.rc) ])
disp(['32點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot32.rc) '   濃度' num2str(D.N2.T2000D1E11.dot32.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot32.rc) '   濃度' num2str(D.N4.T2000D1E11.dot32.rc) ])
disp(['64點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot64.rc) '   濃度' num2str(D.N2.T2000D1E11.dot64.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot64.rc) '   濃度' num2str(D.N4.T2000D1E11.dot64.rc) ])
disp(['128點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot128.rc) '   濃度' num2str(D.N2.T2000D1E11.dot128.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot128.rc) '   濃度' num2str(D.N4.T2000D1E11.dot128.rc) ])
disp(['256點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot256.rc) '   濃度' num2str(D.N2.T2000D1E11.dot256.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot256.rc) '   濃度' num2str(D.N4.T2000D1E11.dot256.rc) ])
disp(['512點'])
disp(['T2000D1E11 2點 溫度' num2str(T.N2.T2000D1E11.dot512.rc) '   濃度' num2str(D.N2.T2000D1E11.dot512.rc) ])
disp(['T2000D1E11 4點 溫度' num2str(T.N4.T2000D1E11.dot512.rc) '   濃度' num2str(D.N4.T2000D1E11.dot512.rc) ])

% 可以儲存溫濃的資料，放入Ii_res_analysis處理
% save Ii_res_result T D

% 離2過10
figure()
plot(wave.N2.T2000D1E11.T,wave.N2.T2000D1E11.Vs)
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Voltage(V)');
set(gca,'FontSize',12)
grid on

% 離4過10
figure()
plot(wave.N4.T2000D1E11.T,wave.N4.T2000D1E11.Vs)
xlabel('\fontsize{12}Time(s)');ylabel('\fontsize{12}Voltage(V)');
set(gca,'FontSize',12)
grid on





