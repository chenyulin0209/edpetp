% 各溫濃下時間估算誤差 %

clear;clc;close all;

load Ideal_ID_test_data

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因\';
content='掃描電壓';
[D1E11.T1,D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E11.T1,D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E11.T1,D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

Ti=2;
baisuu=7;
tau_v=0.001;
Ten=1;
kan=0.001;
D=1E11;
[S1,D1E11.A1,D1E11.B1]=Ideal_idou_ID([D1E11.T1,D1E11.I1],Ti+baisuu*tau_v,D,Ten,1);
[S5,D1E11.A5,D1E11.B5]=Ideal_idou_ID([D1E11.T5,D1E11.I5],Ti+baisuu*tau_v,D,Ten,5);
[D1E11.E1(:,2),D1E11.E1(:,1)]=jikansa(D1E11.T1,D1E11.V1,D1E11.A1,D1E11.B1,0.001,abs(min(D1E11.V1)),kan);
[D1E11.E5(:,2),D1E11.E5(:,1)]=jikansa(D1E11.T5,-D1E11.V5,-D1E11.A5,D1E11.B5,0.001,abs(max(D1E11.V5)),kan);
figure()
hold on
% subplot(2,2,3)

xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');




kan=0.1;
for i=1:size(A1.N10,2)
    [E1.N10(i,:,2),E1.N10(i,:,1)]=jikansa(D1E11.T1,D1E11.V1,A1.N10(i),B1.N10(i),0.1,0.6,kan);
    [E5.N10(i,:,2),E5.N10(i,:,1)]=jikansa(D1E11.T5,-D1E11.V5,-A5.N10(i),B5.N10(i),0.1,1,kan);
end
for i=1:size(A1.N20,2)
    [E1.N20(i,:,2),E1.N20(i,:,1)]=jikansa(D1E11.T1,D1E11.V1,A1.N20(i),B1.N20(i),0.1,0.6,kan);
    [E5.N20(i,:,2),E5.N20(i,:,1)]=jikansa(D1E11.T5,-D1E11.V5,-A5.N20(i),B5.N20(i),0.1,1,kan);
end
for i=1:size(A1.N30,2)
    [E1.N30(i,:,2),E1.N30(i,:,1)]=jikansa(D1E11.T1,D1E11.V1,A1.N30(i),B1.N30(i),0.1,0.6,kan);
    [E5.N30(i,:,2),E5.N30(i,:,1)]=jikansa(D1E11.T5,-D1E11.V5,-A5.N30(i),B5.N30(i),0.1,1,kan);
end
for i=1:size(A1.N40,2)
    [E1.N40(i,:,2),E1.N40(i,:,1)]=jikansa(D1E11.T1,D1E11.V1,A1.N40(i),B1.N40(i),0.1,0.6,kan);
    [E5.N40(i,:,2),E5.N40(i,:,1)]=jikansa(D1E11.T5,-D1E11.V5,-A5.N40(i),B5.N40(i),0.1,1,kan);
end
% figure()
plot(D1E11.E1(:,2),D1E11.E1(:,1),'k')
errorbar(E1.N10(1,:,2),mean(E1.N10(:,:,1)),std(E1.N10(:,:,1)),'r')
errorbar(E1.N20(1,:,2),mean(E1.N20(:,:,1)),std(E1.N20(:,:,1)),'m')
errorbar(E1.N30(1,:,2),mean(E1.N30(:,:,1)),std(E1.N30(:,:,1)),'g')
errorbar(E1.N40(1,:,2),mean(E1.N40(:,:,1)),std(E1.N40(:,:,1)),'b')
errorbar(E5.N10(1,:,2),mean(E5.N10(:,:,1)),std(E5.N10(:,:,1)),'r')
errorbar(E5.N20(1,:,2),mean(E5.N20(:,:,1)),std(E5.N20(:,:,1)),'m')
errorbar(E5.N30(1,:,2),mean(E5.N30(:,:,1)),std(E5.N30(:,:,1)),'g')
errorbar(E5.N40(1,:,2),mean(E5.N40(:,:,1)),std(E5.N40(:,:,1)),'b')

plot(D1E11.E5(:,2),D1E11.E5(:,1),'k')
% subplot(2,2,4)
% plot(E1.N200(:,2),E1.N200(:,1),E5.N200(:,2),E5.N200(:,1))
xlim([0.075 1])
xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('Desired Value','64 points','128 points','256 points','512 points','Location','southeast')
set(gca,'FontSize',12)


