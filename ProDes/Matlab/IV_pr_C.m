% 脈波閒置時間最佳化 第二分項實驗 改探針電容 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電容\0.25uF\';
content='掃描電壓';
[pr025uF.T1,pr025uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr025uF.T5,pr025uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr025uF.T1,pr025uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr025uF.T5,pr025uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr025uF.T1,pr025uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr025uF.T5,pr025uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電容\0.5uF\';
content='掃描電壓';
[pr050uF.T1,pr050uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr050uF.T5,pr050uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr050uF.T1,pr050uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr050uF.T5,pr050uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr050uF.T1,pr050uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr050uF.T5,pr050uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改探針電容\1uF\';
content='掃描電壓';
[pr100uF.T1,pr100uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr100uF.T5,pr100uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[pr100uF.T1,pr100uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr100uF.T5,pr100uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[pr100uF.T1,pr100uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[pr100uF.T5,pr100uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);


st=2.01;
ov=4.9;

[pr025uF.IV1(:,1),pr025uF.IV1(:,2)]=Ideal_IV_make(pr025uF.T1,pr025uF.V1,pr025uF.I1,st,ov);
[pr025uF.IV5(:,1),pr025uF.IV5(:,2)]=Ideal_IV_make(pr025uF.T5,pr025uF.V5,pr025uF.I5,st,ov);
[pr025uF.IV]=IV_mix(pr025uF.IV1,pr025uF.IV5);
figure(1)
subplot(2,2,1)
plot(pr025uF.T1,pr025uF.Vs2,pr025uF.T5,pr025uF.Vs5)
subplot(2,2,2)
plot(pr025uF.T1,pr025uF.I1,pr025uF.T5,pr025uF.I5)
subplot(2,2,3)
plot(pr025uF.T1,pr025uF.V1,pr025uF.T5,pr025uF.V5)
subplot(2,2,4)
plot(pr025uF.IV1(:,1),pr025uF.IV1(:,2),pr025uF.IV5(:,1),pr025uF.IV5(:,2))

[pr050uF.IV1(:,1),pr050uF.IV1(:,2)]=Ideal_IV_make(pr050uF.T1,pr050uF.V1,pr050uF.I1,st,ov);
[pr050uF.IV5(:,1),pr050uF.IV5(:,2)]=Ideal_IV_make(pr050uF.T5,pr050uF.V5,pr050uF.I5,st,ov);
[pr050uF.IV]=IV_mix(pr050uF.IV1,pr050uF.IV5);
figure(2)
subplot(2,2,1)
plot(pr050uF.T1,pr050uF.Vs2,pr050uF.T5,pr050uF.Vs5)
subplot(2,2,2)
plot(pr050uF.T1,pr050uF.I1,pr050uF.T5,pr050uF.I5)
subplot(2,2,3)
plot(pr050uF.T1,pr050uF.V1,pr050uF.T5,pr050uF.V5)
subplot(2,2,4)
plot(pr050uF.IV1(:,1),pr050uF.IV1(:,2),pr050uF.IV5(:,1),pr050uF.IV5(:,2))


[pr100uF.IV1(:,1),pr100uF.IV1(:,2)]=Ideal_IV_make(pr100uF.T1,pr100uF.V1,pr100uF.I1,st,ov);
[pr100uF.IV5(:,1),pr100uF.IV5(:,2)]=Ideal_IV_make(pr100uF.T5,pr100uF.V5,pr100uF.I5,st,ov);
[pr100uF.IV]=IV_mix(pr100uF.IV1,pr100uF.IV5);
figure(3)
subplot(2,2,1)
plot(pr100uF.T1,pr100uF.Vs2,pr100uF.T5,pr100uF.Vs5)
subplot(2,2,2)
plot(pr100uF.T1,pr100uF.I1,pr100uF.T5,pr100uF.I5)
subplot(2,2,3)
plot(pr100uF.T1,pr100uF.V1,pr100uF.T5,pr100uF.V5)
subplot(2,2,4)
plot(pr100uF.IV1(:,1),pr100uF.IV1(:,2),pr100uF.IV5(:,1),pr100uF.IV5(:,2))

figure(4)
% subplot(2,2,1)
grid on
hold on
plot(pr025uF.IV(:,1),-pr025uF.IV(:,2),pr050uF.IV(:,1),-pr050uF.IV(:,2),pr100uF.IV(:,1),-pr100uF.IV(:,2))
ylim([-0.5E-6 3E-6])
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Contamination Capacitance of Probe 0.25uF','Contamination Capacitance of Probe 0.5uF','Contamination Capacitance of Probe 1uF','Location','northwest')
set(gca,'FontSize',12)
figure()
subplot(2,2,2)
plot(pr025uF.IV1(:,1),pr025uF.IV1(:,2),pr025uF.IV5(:,1),pr025uF.IV5(:,2))
subplot(2,2,3)
plot(pr050uF.IV1(:,1),pr050uF.IV1(:,2),pr050uF.IV5(:,1),pr050uF.IV5(:,2))
subplot(2,2,4)
plot(pr100uF.IV1(:,1),pr100uF.IV1(:,2),pr100uF.IV5(:,1),pr100uF.IV5(:,2))



