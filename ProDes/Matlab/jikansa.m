% 由電壓(j) 
%{
目的:   由 實際雙汙染跨壓-time vs 指數放電ID結果  比較 時間估算誤差
input: 
    Vreal Treal 實際雙汙染跨壓 時間， 
    A B 指數下降ID 參數， 
    left right step:預估電壓下限 上限 間格
    Sartime : 電壓關shut down 時間
output: Et 時間估算誤差, Ev 預估電壓。
Note:
    推估方程: Y = A*exp(B*t)
%}
function[Ev,Et]=jikansa(Sartime,Treal,Vreal,A,B,left,right,step)
    x=1;
    for j=left:step:right
        [f1,~,~]=D_find_T(Treal,Vreal,-j);
        f1=f1-Sartime;
        f2=log(j/A)/B;
        Ev(x)=j;
        Et(x)=real(f1-f2);
        x=x+1;
    end
end