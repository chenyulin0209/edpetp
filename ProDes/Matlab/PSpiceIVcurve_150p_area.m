clc;
clear;

A = csvread('pspice_result_area.csv');		% 將 data.csv 的內容讀到矩陣 A
B = csvread('ideal_I_Vsweep.csv');


%x6
Vf_x6=A(242,2);
Vs_x6=A(:,1)';
Vp_x6=A(:,2)';
I_x6=A(:,3)';

Videal=B(:,1)';
Iideal=B(:,2)';

subplot(2,2,1);
plot(Vp_x6,(I_x6*10^6),'b');
hold on;
plot(Vs_x6+Vf_x6,(I_x6*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : area ratio x6');
axis([-1.5 0 -0.02 0.04]);

%x100
Vf_x100=A(242,4);
Vp_x100=A(:,4);
I_x100=A(:,5);

subplot(2,2,2);
plot(Vp_x100,(I_x100*10^6),'b');
hold on;
plot(Vs_x6+Vf_x100,(I_x100*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : area ratio x100');
axis([-1.5 0 -0.02 0.04]);

%x1000
Vf_x1000=A(242,6);
Vp_x1000=A(:,6)
I_x1000=A(:,7);

subplot(2,2,3);
plot(Vp_x1000,(I_x1000*10^6),'b');
hold on;
plot(Vs_x6+Vf_x1000,(I_x1000*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('ideal','distorted');
legend('Location','NorthWest');
title('I-V curve : area ratio x1000');
axis([-1.5 0 -0.02 0.04]);
%---------------------------------------
figure;
subplot(2,2,1);
plot(Videal,(Iideal),'b');
hold on;
plot(Vp_x6,(I_x6*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : area ratio x6');


subplot(2,2,2);
plot(Videal,(Iideal),'b');
hold on;
plot(Vp_x100,(I_x100*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : area ratio x100');
%axis([-0.5 0.5 -0.02 0.04]);


subplot(2,2,3);
plot(Videal,(Iideal),'b');
%plot(Vs_x6,(I_x1000*10^6),'b');
hold on;
plot(Vp_x1000,(I_x1000*10^6),'--g');

grid on;
xlabel('Probe voltage relative to the plasma potentail(V)');
ylabel('Current(uA)');
legend('theory data','pspice probe data');
legend('Location','NorthWest');
title('I-V curve : area ratio x1000');
%axis([-0.5 0.5 -0.02 0.04]);
%---------------------------------------
for i=1:1:476
    err_x6(i)=(Vp_x6(i)-Vf_x6)/Vs_x6(i)*100;
end;

for i=1:1:476
    err_x100(i)=(Vp_x100(i)-Vf_x100)/Vs_x6(i)*100;
end;

for i=1:1:476
    err_x1000(i)=(Vp_x1000(i)-Vf_x1000)/Vs_x6(i)*100;
end;

figure;
plot(Vs_x6,err_x6,':b');
hold on;
plot(Vs_x6,err_x100,'--g');
hold on;
plot(Vs_x6,err_x1000,'r');

grid on;
xlabel('Sweeping voltage(V)');
ylabel('Voltage of Probe-to-Sweep ratio(%)');
legend('area ratio x6','area ratio x100','area ratio x1000');
legend('Location','SouthWest');
title('Voltage of Probe-to-Sweep with area ratio relation');
axis([-0.5 0.5 0 100]);