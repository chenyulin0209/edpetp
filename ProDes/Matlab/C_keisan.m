function[C]=C_keisan(t,I,SP)
    K=(I(1)-I(3))/(I(1)-I(2));
    C=log(K-1)/SP;
end