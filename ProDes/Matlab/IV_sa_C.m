% 脈波閒置時間最佳化 第二分項實驗 改酬載電容 %
% 碳針：面積=A beta=1 探針汙染層：電容=0.5uF 電阻=1000k %
% 酬載：面積=6A beta=1 酬載汙染層：電容=3uF 電阻=160k %
% 電子溫度=2000K 電子濃度=1E11  %
% 配合生成脈波程式mannyou_step %

clc;clear;close all;

tau_c=0.383;
tau_v=tau_c/500;
seikaku=-7.07*10^(-7);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電容\1.5uF\';
content='掃描電壓';
[ps15uF.T1,ps15uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps15uF.T5,ps15uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps15uF.T1,ps15uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps15uF.T5,ps15uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps15uF.T1,ps15uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps15uF.T5,ps15uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電容\3uF\';
content='掃描電壓';
[ps30uF.T1,ps30uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps30uF.T5,ps30uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps30uF.T1,ps30uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps30uF.T5,ps30uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps30uF.T1,ps30uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps30uF.T5,ps30uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\改酬載電容\6uF\';
content='掃描電壓';
[ps60uF.T1,ps60uF.Vs2]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps60uF.T5,ps60uF.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[ps60uF.T1,ps60uF.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps60uF.T5,ps60uF.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[ps60uF.T1,ps60uF.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[ps60uF.T5,ps60uF.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

st=2.01;
ov=4.9;

[ps15uF.IV1(:,1),ps15uF.IV1(:,2)]=Ideal_IV_make(ps15uF.T1,ps15uF.V1,ps15uF.I1,st,ov);
[ps15uF.IV5(:,1),ps15uF.IV5(:,2)]=Ideal_IV_make(ps15uF.T5,ps15uF.V5,ps15uF.I5,st,ov);
[ps15uF.IV]=IV_mix(ps15uF.IV1,ps15uF.IV5);
figure(1)
subplot(2,2,1)
plot(ps15uF.T1,ps15uF.Vs2,ps15uF.T5,ps15uF.Vs5)
subplot(2,2,2)
plot(ps15uF.T1,ps15uF.I1,ps15uF.T5,ps15uF.I5)
subplot(2,2,3)
plot(ps15uF.T1,ps15uF.V1,ps15uF.T5,ps15uF.V5)
subplot(2,2,4)
plot(ps15uF.IV1(:,1),ps15uF.IV1(:,2),ps15uF.IV5(:,1),ps15uF.IV5(:,2))

[ps30uF.IV1(:,1),ps30uF.IV1(:,2)]=Ideal_IV_make(ps30uF.T1,ps30uF.V1,ps30uF.I1,st,ov);
[ps30uF.IV5(:,1),ps30uF.IV5(:,2)]=Ideal_IV_make(ps30uF.T5,ps30uF.V5,ps30uF.I5,st,ov);
[ps30uF.IV]=IV_mix(ps30uF.IV1,ps30uF.IV5);
figure(2)
subplot(2,2,1)
plot(ps30uF.T1,ps30uF.Vs2,ps30uF.T5,ps30uF.Vs5)
subplot(2,2,2)
plot(ps30uF.T1,ps30uF.I1,ps30uF.T5,ps30uF.I5)
subplot(2,2,3)
plot(ps30uF.T1,ps30uF.V1,ps30uF.T5,ps30uF.V5)
subplot(2,2,4)
plot(ps30uF.IV1(:,1),ps30uF.IV1(:,2),ps30uF.IV5(:,1),ps30uF.IV5(:,2))

[ps60uF.IV1(:,1),ps60uF.IV1(:,2)]=Ideal_IV_make(ps60uF.T1,ps60uF.V1,ps60uF.I1,st,ov);
[ps60uF.IV5(:,1),ps60uF.IV5(:,2)]=Ideal_IV_make(ps60uF.T5,ps60uF.V5,ps60uF.I5,st,ov);
[ps60uF.IV]=IV_mix(ps60uF.IV1,ps60uF.IV5);
figure(3)
subplot(2,2,1)
plot(ps60uF.T1,ps60uF.Vs2,ps60uF.T5,ps60uF.Vs5)
subplot(2,2,2)
plot(ps60uF.T1,ps60uF.I1,ps60uF.T5,ps60uF.I5)
subplot(2,2,3)
plot(ps60uF.T1,ps60uF.V1,ps60uF.T5,ps60uF.V5)
subplot(2,2,4)
plot(ps60uF.IV1(:,1),ps60uF.IV1(:,2),ps60uF.IV5(:,1),ps60uF.IV5(:,2))

figure(4)
% subplot(2,2,1)
grid on
hold on
plot(ps15uF.IV(:,1),-ps15uF.IV(:,2),ps30uF.IV(:,1),-ps30uF.IV(:,2),ps60uF.IV(:,1),-ps60uF.IV(:,2))
ylim([-0.5E-6 3E-6])
% title('\fontsize{12}不同酬載汙染層電容下的IV曲線');
xlabel('\fontsize{12}Voltage of Contamination(V)');ylabel('\fontsize{12}Current(A)');
legend('Contamination Capacitance of Satellite 1.5uF','Contamination Capacitance of Satellite 3uF','Contamination Capacitance of Satellite 6uF','Location','northwest')
set(gca,'FontSize',12)
figure()
subplot(2,2,2)
plot(ps15uF.IV1(:,1),ps15uF.IV1(:,2),ps15uF.IV5(:,1),ps15uF.IV5(:,2))
subplot(2,2,3)
plot(ps30uF.IV1(:,1),ps30uF.IV1(:,2),ps30uF.IV5(:,1),ps30uF.IV5(:,2))
subplot(2,2,4)
plot(ps60uF.IV1(:,1),ps60uF.IV1(:,2),ps60uF.IV5(:,1),ps60uF.IV5(:,2))



