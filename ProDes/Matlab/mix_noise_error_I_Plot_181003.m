clc;clear;close all;
load('CurrCuit_200k_1E11_tau_v_0.1674over500_Error_32_181003.1_0.03to0.1.mat');

% %     掃描電壓1.0V
% figure()
% hold on
% errorbar(E128_p10(:,1),E128_p10(:,2),E128_p10(:,3));
% errorbar(E64_p10(:,1),E64_p10(:,2),E64_p10(:,3));
% errorbar(E32_p10(:,1),E32_p10(:,2),E32_p10(:,3));
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('1.0V')
% %     掃描電壓0.8V
% figure()
% hold on
% errorbar(E128_p08(:,1),E128_p08(:,2),E128_p08(:,3)*0.1);
% errorbar(E64_p08(:,1),E64_p08(:,2),E64_p08(:,3)*0.1);
% errorbar(E32_p08(:,1),E32_p08(:,2),E32_p08(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('0.8V')
% %     掃描電壓0.4V
% figure()
% hold on
% errorbar(E128_p04(:,1),E128_p04(:,2),E128_p04(:,3)*0.1);
% errorbar(E64_p04(:,1),E64_p04(:,2),E64_p04(:,3)*0.1);
% errorbar(E32_p04(:,1),E32_p04(:,2),E32_p04(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('0.4V')
% %     掃描電壓0.2V
% figure()
% hold on
% errorbar(E128_p02(:,1),E128_p02(:,2),E128_p02(:,3)*0.1);
% errorbar(E64_p02(:,1),E64_p02(:,2),E64_p02(:,3)*0.1);
% errorbar(E32_p02(:,1),E32_p02(:,2),E32_p02(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('0.2V')
% %     掃描電壓-1.0V
% figure()
% hold on
% errorbar(E128_n10(:,1),E128_n10(:,2),E128_n10(:,3)*0.1);
% errorbar(E64_n10(:,1),E64_n10(:,2),E64_n10(:,3)*0.1);
% errorbar(E32_n10(:,1),E32_n10(:,2),E32_n10(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('-1.0V')
% %     掃描電壓-3.0V
% figure()
% hold on
% errorbar(E128_n30(:,1),E128_n30(:,2),E128_n30(:,3)*0.1);
% errorbar(E64_n30(:,1),E64_n30(:,2),E64_n30(:,3)*0.1);
% errorbar(E32_n30(:,1),E32_n30(:,2),E32_n30(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('-3.0V')
% %     掃描電壓-5.0V
% figure()
% hold on
% errorbar(E128_n50(:,1),E128_n50(:,2),E128_n50(:,3)*0.1);
% errorbar(E64_n50(:,1),E64_n50(:,2),E64_n50(:,3)*0.1);
% errorbar(E32_n50(:,1),E32_n50(:,2),E32_n50(:,3)*0.1);
% xlim([0 0.22]);
% xlabel('\fontsize{12}Normalized Sample Period(\tau_c(1V))');ylabel('\fontsize{12}Error(%)');
% % legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')
% legend('128組','64組','32組','location','northwest')
% title('-5.0V')


% legend('128組','64組','32組','16組','8組','4組','2組','location','northwest')

point.p10=E32_p10;
point.p08=E32_p08;
point.p04=E32_p04;
point.n30=E32_n30;
point.n50=E32_n50;



load('CurrCuit_200k_1E11_tau_v_0.1674over500_Error_32_181003.2_0.1to0.5.mat');

point.p10=[point.p10;E32_p10 ];
hold on
point.p08=[point.p08;E32_p08 ];
point.p04=[point.p04;E32_p04 ];
point.n30=[point.n30;E32_n30 ];
point.n50=[point.n50;E32_n50 ];

errorbar(point.p10(:,1),point.p10(:,2),point.p10(:,3)*0.01);
errorbar(point.p08(:,1),point.p08(:,2),point.p08(:,3)*0.01);
errorbar(point.p04(:,1),point.p04(:,2),point.p04(:,3)*0.01);
%errorbar(point.p02(:,1),point.p02(:,2),point.p02(:,3)*0.1);
%errorbar(point.n10(:,1),point.n10(:,2),point.n10(:,3)*0.01);
errorbar(point.n30(:,1),point.n30(:,2),point.n30(:,3)*0.01);
errorbar(point.n50(:,1),point.n50(:,2),point.n50(:,3)*0.01);
legend('1.0V','0.8V','0.4V','-3.0V','-5.0V','location','northwest')
legend('1.0V','0.8V','0.4V','-3.0V','-5.0V','location','northwest')




