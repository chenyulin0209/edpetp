%{
    目的:
    輸入:
    輸出: 
%}
clc;clear;close all;

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\充電及放電\定便因_new\';
content='雙汙染跨壓_內插專用';
[T10,V10]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

    sizeof = length(T10);
    count = 1;
    BeforeTime= T10(1);
    Step = 15*10^-8;
    
    index=1;
    
   
    for t=T10(1):Step:T10(length(T10))
        if t>= T10(index+1)
            index=index+1;
        end
        T10_Inter(count)=t;
        V10_Inter(count)=...
           ((V10(index+1)-V10(index)) / (T10(index+1)-T10(index))) * (t-T10(index))+ V10(index);
        count=count+1;
    end
    
    
    fid=fopen('C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\充電及放電\定便因_new\-5V\雙汙染跨壓_內插後.txt','w+');

    fprintf(fid,'Time \t Voltage \r\n');
    for i=1:1:length(T10_Inter)
      fprintf(fid,'%0.14f \t %0.14f \r\n',T10_Inter(i),V10_Inter(i));
    end
    
    fclose(fid);

