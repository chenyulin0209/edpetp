clc;clear;close all;
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\tau_v_0.1674over500\';
content='迴路電流';
[VC_500.p10.T,VC_500.p10.I]=textread([path '1.0V高解析\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.n50.T,VC_500.n50.I]=textread([path '-5.0V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.p04.T,VC_500.p04.I]=textread([path '0.4V\' content '.txt'],'%f %f','headerlines',1);
% [VC_500.p08.T,VC_500.p08.I]=textread([path '0.8V\' content '.txt'],'%f %f','headerlines',1);

% 電流時間常數
tau_c=0.1674;
% 設定電流和電壓時間常數的比值
tau_v=tau_c/500;
% 幾倍的電流時間常數當做取樣區間，由前面實驗知道0.4倍最理想
interval=0.4*tau_c;
% 幾倍的電壓時間常數取樣第一個值，由前面實驗知道7倍最理想
baisuu=7;
% 快速取樣的取樣週期
rapid_S=15*10^(-6);

Ireal=[-19.0513,4.08347,6.39084,8.69623,11.0059,13.3189]*10^(-7);
Ireal_seco=[-5.83]*10^(-7);
x=1;
i=0.1*tau_c;
gosa=0;
 Inoise=VC_500.p10.I+rand(685601,1)*Ireal_seco*gosa/100-Ireal_seco*gosa/100/2;
%           單組實驗
            [E_rc.p10(1),data_tamp,A,B,C128rc_p10,~]=santori_ID([VC_500.p10.T,Inoise],tau_v*baisuu,i,3,Ireal(1));
%           多組實驗
            [E128rc_p10(1),data_tamp,A,B,C128rc_p10(1),~]=mean_I_ID([VC_500.p10.T,Inoise],tau_v*baisuu,i,rapid_S,384,Ireal(1));
E_rc.p10(1)
E128rc_p10(1)
figure()  
plot(VC_500.p10.T,Inoise);
hold on   
plot(data_tamp(:,1),data_tamp(:,2),'x');
t=0:0.01:0.1;
plot(t,A+B*exp(C128rc_p10(1)*t));

