clear;
clc;

q=1.602*(10^-19);
me=9.109*(10^-31);
mi=14*1.66*(10^-27);%mi for N+/1.66*...=1/Na
k=1.38064852*(10^-23);
%BCU probe area
Ap=58.2*(10^-4);%ncku:78.5,we:58.2
Ar=58.2*(10^-4)*6;


%electron density at height=??? ncku
ne=1*(10^3)*(10^6);%ncku:3*(10^4)*(10^6)
ni=ne;%ncku:3*(10^4)*(10^6)

Te=1500;%ncku:1500
B=0.5;

Qc=sqrt(k*Te/2/pi/me);
Pc=sqrt(k*Te/2/pi/mi);

detV=-1:0.01:0.5;%detV=-1:0.01:2;


for i=1:151; %
    if(detV(i)>0)
        Iep(i)=(10^6)*q*Ap*ne*Qc*(1+(q*(detV(i))/(k*Te)))^B;
        Iip(i)=(10^6)*q*Ap*ne*Pc*exp(-q*(detV(i))/(k*Te));
    else
        Iep(i)=(10^6)*q*Ap*ne*Qc*exp((q*(detV(i))/(k*Te)));
        Iip(i)=(10^6)*q*Ap*ne*Pc*(1-q*(detV(i))/(k*Te))^B;
    end
end
Ip=Iep-Iip;

plot(detV,Ip);
grid on;
xlabel('Probe voltage relativeto the plasma potentail(V)');
ylabel('Current(uA)');


hold on;


for i=1:151 %301
    if(detV(i)>0)
        Ier(i)=(10^6)*q*Ar*ne*Qc*(1+(q*(detV(i))/(k*Te)))^B;
        Iir(i)=(10^6)*q*Ar*ne*Pc*exp(-q*(detV(i))/(k*Te));
    else
        Ier(i)=(10^6)*q*Ar*ne*Qc*exp((q*(detV(i))/(k*Te)));
        Iir(i)=(10^6)*q*Ar*ne*Pc*(1-q*(detV(i))/(k*Te))^B;
    end
end
Ir=Ier-Iir;
plot(detV,Ir,'g');
legend('Probe','Reference');

detV_tr=detV';
Ip_tr=Ip';
Ir_tr=Ir';
%--------------------------------------

%find floating voltage
x=-2:0.01:0;
Bright=0;
Bleft=-2;
Mid=(Bleft+Bright)/2;
Iep2=(10^6)*q*Ap*ne*Qc*exp((q*(Mid)/(k*Te)));
Iip2=(10^6)*q*Ap*ne*Pc*(1-q*(Mid)/(k*Te))^B;
Ip2=Iep2-Iip2;

Ier2=(10^6)*q*Ar*ne*Qc*exp((q*(Mid)/(k*Te)));
Iir2=(10^6)*q*Ar*ne*Pc*(1-q*(Mid)/(k*Te))^B;
Ir2=Ier2-Iir2;
    
while abs(Ir2-Ip2)>0.001
    Mid=(Bleft+Bright)/2;
    Iep2=(10^6)*q*Ap*ne*Qc*exp((q*(Mid)/(k*Te)));
    Iip2=(10^6)*q*Ap*ne*Pc*(1-q*(Mid)/(k*Te))^B;
    Ip2=Iep2-Iip2;
    
    Ier2=(10^6)*q*Ar*ne*Qc*exp((q*(Mid)/(k*Te)));
    Iir2=(10^6)*q*Ar*ne*Pc*(1-q*(Mid)/(k*Te))^B;
    Ir2=Ier2-Iir2;
    

    if(Ir2-Ip2>0)
        Bright=Mid;
    else
        Bleft=Mid;
    end
end

disp(Mid); %sol:Mid= -0.5488

%disp('       Vin       Vr       Vp');


%for Vin=0.1:0.1:5
%    for Vx=-2:0.01:0
%        Iep2=(10^6)*q*Ap*ne*Qc*exp((q*(Vx)/(k*Te)));
%        Iip2=(10^6)*q*Ap*ne*Pc*(1-q*(Vx)/(k*Te))^B;
%        Ip2=Iep2-Iip2;

%        Ier2=(10^6)*q*Ar*ne*Qc*exp((q*(Vx+Vin)/(k*Te)));
%        Iir2=(10^6)*q*Ar*ne*Pc*(1-q*(Vx+Vin)/(k*Te))^B;
%        Ir2=Ier2-Iir2;

%        if(Ip2>0 && Ip2+Ir2<0.1)
%            Vrec(1)=Vin;
%            Vrec(2)=Vx;
%            Vrec(3)=Vx+Vin;
%            disp(Vrec);
%            break;
%        end
%    end
%end











