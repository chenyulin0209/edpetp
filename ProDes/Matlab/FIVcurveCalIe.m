%{
  Debye IV 在 Vpr<-1時  Ii~I ，最小方差6次多項式逼近 Ii,  I-Ii=Ie
  Vpr 需包含 V=-5 ~ -1 的範圍 
  input :Vpr=(1xN) I =(1xN)
  output:Ii =(1xN) Ie=(1xN)
%}
function [Ii,Ie]=FIVcurveCalIe(I,Vpr)


Vpr;
right=-1.2;
% right=0;

     dI=zeros(1,length(I));   
     for i=2:length(I)-1
         dI(i)=abs((I(i+1)-I(i-1))/(Vpr(i+1)-Vpr(i-1)));
     end
        V_5=1;
        for i=1:length(Vpr)-1
            Vpr(i);
          if(Vpr(i)<=-5 && Vpr(i+1)>=-5)
            V_5=i+1;
          end
          if(Vpr(i)<=right && Vpr(i+1)>=right)
            V_1=i;
          end
        end
        
        Sample1=V_5:V_1;                    % 第一段取的資料
        %Sample2=1:length(Vpr);

        %Ma=Mb*Mx +err 求Mx
        X =( Vpr(Sample1) )';
        
        Ma=(I(Sample1))'*1E12;
%         Mb=[ones(size(X,1),1), X, X.^2, X.^3, X.^4, X.^5, X.^6];
Mb=[ones(size(X,1),1), X];
        
        Mx=(Mb'*Mb)\Mb'*Ma;
        
        fx=( Vpr )';
%         Ii=([ones(size(fx,1),1), fx, fx.^2,fx.^3,fx.^4,fx.^5,fx.^6]*Mx*1E-12)';
Ii=([ones(size(fx,1),1), fx]*Mx*1E-12)';
        Ie=I-Ii;
        
                

        
end