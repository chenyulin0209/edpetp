%{
  OML theory Ie在 Vpr >Vpl 為 exp function
  ln(Ie)=ln(e A ne sqrt(k Te / 2 pi me))+ e/(k Te)(Vpr-Vpl)
  最小方差
%}
function [Te,Ne]=FIVcurveFindTe(Ie,Vpr,Vpl)

% [Vprs,Ies]=FVcurveSmooth(Vpr,Ie,Vpr(1),Vpr(length(Vpr)),0.01);
Vprs=Vpr;
Ies=Ie;

            k=1.3806488*10^-23;
            e=1.602*10^-19;
            A=58.2*10^(-4);
            m_e=9.109*10^-31;
            
        X =( Vprs-Vpl )';
        Ma=log((-Ies)*1E12)';
        Mb=[ones(size(X,1),1), X];
        
        
        Mx=real((Mb'*Mb)\Mb'*Ma);

Te= e/k/Mx(2); %猜測T
Ne= exp(Mx(1))/A/e*sqrt(2*pi*m_e/k/Te)/1E12; %電流放大 1E9倍
end