%{
目的：
顯示理想、非理想脈波以及理想、非理想脈波響應電流，有什麼差別
說明不同電壓電流時間常數比，對於最大值法的影響
細節：
橫軸有正規化
%}
clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 理想情況下，量測到的電流值
seikaku=-1.90522*10^(-6);

% 讀資料
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='掃描電壓';
[tau_v_ideal.T,tau_v_ideal.V]=textread([path '理想\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_100.T,tau_v_over_100.V]=textread([path 'tau_v_0.1674over100\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_200.T,tau_v_over_200.V]=textread([path 'tau_v_0.1674over200\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300.T,tau_v_over_300.V]=textread([path 'tau_v_0.1674over300\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_400.T,tau_v_over_400.V]=textread([path 'tau_v_0.1674over400\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500.T,tau_v_over_500.V]=textread([path 'tau_v_0.1674over500\1.0V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[tau_v_ideal.T,tau_v_ideal.I]=textread([path '理想\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_100.T,tau_v_over_100.I]=textread([path 'tau_v_0.1674over100\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_200.T,tau_v_over_200.I]=textread([path 'tau_v_0.1674over200\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300.T,tau_v_over_300.I]=textread([path 'tau_v_0.1674over300\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_400.T,tau_v_over_400.I]=textread([path 'tau_v_0.1674over400\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500.T,tau_v_over_500.I]=textread([path 'tau_v_0.1674over500\1.0V\' content '.txt'],'%f %f','headerlines',1);

% 理想、非理想脈波比較圖
figure(1)
plot(tau_v_ideal.T/tau_c,tau_v_ideal.V,tau_v_over_100.T/tau_c,tau_v_over_100.V)
xlim([0 0.5])
ylim([-0.2 1.2])
x_axis=[0:0.1:0.5];y_axis=[-0.2:0.2:1.2];
set(gca,'XTick',x_axis);set(gca,'XTickLabel',x_axis');set(gca,'YTick',y_axis);set(gca,'YTickLabel',y_axis');
xlabel('\fontsize{12}Normalized Time(\tau_c)');ylabel('\fontsize{12}Voltage(V)');
legend('\fontsize{12}Theoretical Pulse','\fontsize{12}True Pulse','Location','Southeast');
set(gca,'FontSize',12)
grid on

% 理想、非理想脈波響應電流比較圖
figure(2)
plot(tau_v_ideal.T/tau_c,-tau_v_ideal.I,tau_v_over_100.T/tau_c,-tau_v_over_100.I)
xlim([0 0.5])
ylim([-0.5*10^(-6) 2.2*10^(-6)])
xlabel('\fontsize{12}Normalized Time(\tau_c)');ylabel('\fontsize{12}Current(A)');
legend('\fontsize{12}Current Response of Theoretical Pulse','\fontsize{12}Current Response of True Pulse','Location','Southeast');
set(gca,'FontSize',12)
grid on

% 電壓電流時間常數比，對電流取樣的影響
figure(3)
hold on
TT(1)=0;
DD(1)=-seikaku;
% 找出電流最低點的時間和數值
[TT(2),DD(2),~]=D_find_T(tau_v_over_100.T,-tau_v_over_100.I,-min(tau_v_over_100.I))
[TT(3),DD(3),~]=D_find_T(tau_v_over_200.T,-tau_v_over_200.I,-min(tau_v_over_200.I))
[TT(4),DD(4),~]=D_find_T(tau_v_over_300.T,-tau_v_over_300.I,-min(tau_v_over_300.I))
[TT(5),DD(5),~]=D_find_T(tau_v_over_400.T,-tau_v_over_400.I,-min(tau_v_over_400.I))
[TT(6),DD(6),~]=D_find_T(tau_v_over_500.T,-tau_v_over_500.I,-min(tau_v_over_500.I))

% 畫出電流曲線
plot(...
tau_v_ideal.T/tau_c,-tau_v_ideal.I,...
tau_v_over_500.T/tau_c,-tau_v_over_500.I,...
tau_v_over_400.T/tau_c,-tau_v_over_400.I,...
tau_v_over_300.T/tau_c,-tau_v_over_300.I,...
tau_v_over_200.T/tau_c,-tau_v_over_200.I,...
tau_v_over_100.T/tau_c,-tau_v_over_100.I)

% 點出各電流曲線的最高點
plot(...
TT(1)/tau_c,DD(1),'o',...
TT(6)/tau_c,DD(6),'o',...
TT(5)/tau_c,DD(5),'o',...
TT(4)/tau_c,DD(4),'o',...
TT(3)/tau_c,DD(3),'o',...
TT(2)/tau_c,DD(2),'o')
xlim([0 0.2])
ylim([-0.5*10^(-6) 2.2*10^(-6)])
xlabel('\fontsize{12}Normalized Time(\tau_c)');ylabel('\fontsize{12}Current(A)');
legend('\tau_c/\tau_v=\infty','\tau_c/\tau_v=500','\tau_c/\tau_v=400','\tau_c/\tau_v=300','\tau_c/\tau_v=200','\tau_c/\tau_v=100','Location','Southeast')
set(gca,'FontSize',12)
grid on


