% 閒置曲線ID %
%{
    目的: 電壓放電曲線 擷取點 並用__法求出AB 回歸曲線 
    輸入: 
        Data: 模擬 IT 數據
        StartTime:  擷取點起始延遲
        Td:  擷取點時間(x倍tau_v) Td
        Ts:  ASA_ADC擷取頻率
        times: 擷取點數
        Tp: 擷取點時間(x倍tau_c) Tp
        Density: 電子濃度
        Tensuu: 未知
        pn: 輸入電壓
    輸出:
        dot: 時間、電壓、電流
        A1: Y=A*exp(Bt) 式中 A
        B1: Y=A*exp(Bt) 式中 B
%}
% 閒置曲線ID %
function[AB_value,AA,BB]=Ideal_idou_ID_HiDen(Data ,StartTime ,Td ,Ts ,times, Tp, Density, Tensuu, pn)
    %{
        Tp -> 下降取擷取點 = StartTime+Td + n * Tp
    %}
    %攫取區域數量
    n=2;
    % Load -5.0V ~ 1.0V stap為0.00001 
    load('ideal_IV_table_2.mat');

    if pn==1
        
        % 加入雜訊，雜訊大小是用最大電流的%數決定
        gosa=10;
        Ireal_seco=[22.15]*10^(-8);
        Length = length(Data(:,2));
        Data(:,2) = Data(:,2)+ rand(Length,1) * Ireal_seco * gosa/100-Ireal_seco*gosa/100/2;

        % 起始時間電流%
        [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Td);
        %用電流在IV取線上找到對應電壓% -> 最高電壓
        [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
        %
        dot(1,1)=dot1(1);
        dot(1,2)=dot1(2);
        dot(1,3)=dot1(3);
        
        tamp=1;
        for i=1:n
            if(i>1)
                tamp=tamp+4;
            end
            
            [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Td+(i-1)*Tp);
            [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
            ValHiDen(1,tamp)=dot1(1);
            ValHiDen(1,tamp+1)=dot1(2);
            ValHiDen(1,tamp+2)=dot1(3);
            
            for j=1:times-1
                % 由時間(自訂)找相對應電流(Data)
                [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),real(StartTime+Td+(i-1)*Tp+j*Ts));
                % 由電流(Data)找相對應電壓(ideal)
                [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
                ValHiDen(j+1,tamp)=dot1(1);
                ValHiDen(j+1,tamp+1)=dot1(2);
                ValHiDen(j+1,tamp+2)=dot1(3);
            end
        end
        
        A3=ValHiDen(:,3);
        Y7=ValHiDen(:,7);
        B1 = inv(A3'*A3)*A3'*Y7/Tp;
         B1=- B1;
        BB=B1;
        % 計算 V( T_d+nT_s+kT_p)= V( T_d)(exp(b T_s)^ n (exp(b T_p) ^ k 中的
        % exp(b T_s) 和 exp(b T_p)
        count=1;
        for k=1:n
            ValHiDen_Exp_bTp(k) = exp(B1*Tp)^(k-1);
            for m=1:times
               ValHiDen_Exp_bTs(m) = exp(B1*Ts)^(m-1);
               ArrayExp(count)= ValHiDen_Exp_bTs(m)*ValHiDen_Exp_bTp(k);
               count=count+1;
            end
        end
        
        A4=ArrayExp';
        Y8=[ValHiDen(:,1:3);ValHiDen(:,5:7)];
        V_Td = inv(A4'*A4)*A4'*Y8(:,3);
%         V_Td =  data_sum(2)*sum(ArrayExp(:)) / ( sum(ArrayExp(:))^2 );
%         %  A1*exp(B1*Td) = V_Td 推導出 A1 = exp(log(V_Td)/B1/Td)
         A1 = exp(log(V_Td)-B1*Td);
         A1=real(A1);
         AA=A1;
         
        t=0.001:0.001:5;
        plot(t,AA*exp(BB*t));
        hold on;
        legend('10^-6','10^-5','10^-4','10^-3','10^-2','location','northwest');
        AB_value(1)=AA;
        AB_value(2)=BB;
    end
    if pn==5
        
        % 加入雜訊，雜訊大小是用最大電流的%數決定
        gosa=10;
        Ireal_seco=[-4.533]*10^(-8);
        Length = length(Data(:,2));
        Data(:,2) = Data(:,2)+ rand(Length,1) * Ireal_seco * gosa/100-Ireal_seco*gosa/100/2;

        [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Td);
        [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
        dot(1,1)=dot1(1);
        dot(1,2)=dot1(2);
        dot(1,3)=dot1(3);

         tamp=1;
         for i=1:n
            if(i>1)
                tamp=tamp+4;
            end
            
            [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),StartTime+Td+(i-1)*Tp);
            [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
            ValHiDen(1,tamp)=dot1(1);
            ValHiDen(1,tamp+1)=dot1(2);
            ValHiDen(1,tamp+2)=dot1(3);
            
            for j=1:times-1
                % 由時間(自訂)找相對應電流(Data)
                [dot1(1),dot1(2),~]=T_find_D(Data(:,1),Data(:,2),real(StartTime+Td+(i-1)*Tp+j*Ts));
                % 由電流(Data)找相對應電壓(ideal)
                [~,dot1(3),~]=T_find_D(Guess.IV(:,1),Guess.IV(:,2),dot1(2));
                ValHiDen(j+1,tamp)=dot1(1);
                ValHiDen(j+1,tamp+1)=dot1(2);
                ValHiDen(j+1,tamp+2)=dot1(3);
            end
         end
        A3=ValHiDen(:,3);
        Y7=ValHiDen(:,7);
        B1 = inv(A3'*A3)*A3'*Y7/Tp;
         B5=- B1;
         BB=B5;

        % 計算 V( T_d+nT_s+kT_p)= V( T_d)(exp(b T_s)^ n (exp(b T_p) ^ k 中的
        % exp(b T_s) 和 exp(b T_p)
        count=1;
        for k=1:n
            ValHiDen_Exp_bTp(k) = exp(B5*Tp)^(k-1);
            for m=1:times
               ValHiDen_Exp_bTs(m) = exp(B5*Ts)^(m-1);
               ArrayExp(count)= ValHiDen_Exp_bTs(m)*ValHiDen_Exp_bTp(k);
               count=count+1;
            end
        end
        
        A4=ArrayExp';
        Y8=[ValHiDen(:,1:3);ValHiDen(:,5:7)];
        V_Td = inv(A4'*A4)*A4'*Y8(:,3);
%         V_Td =  data_sum(2)*sum(ArrayExp(:)) / ( sum(ArrayExp(:))^2 );
%         %  A1*exp(B1*Td) = V_Td 推導出 A1 = exp(log(V_Td)/B1/Td)
         A1 = exp(log(V_Td)-B5*Td);
         A5=real(A1);
    
        AA=A5;
        
        figure();
        t=0.001:0.001:5;
        plot(t,AA*exp(BB*t));
        hold on;
        
        AB_value(1)=AA;
        AB_value(2)=BB;
     end
    
        
        
end










