% 陪ボIi_res挡狦 %

clc;clear;close all;

% load Ii_res_result2000
load lowest_res_result

disp(['512翴'])
disp(['T1500D1E11 放' num2str(mean(T.T1500D1E11.rc)) ' 夹非畉' num2str(std(T.T1500D1E11.rc)) '   緻' num2str(mean(D.T1500D1E11.rc)) ' 夹非畉' num2str(std(D.T1500D1E11.rc)) ])
disp(['T2000D1E9 放' num2str(mean(T.T2000D1E9.rc)) ' 夹非畉' num2str(std(T.T2000D1E9.rc)) '   緻' num2str(mean(D.T2000D1E9.rc)) ' 夹非畉' num2str(std(D.T2000D1E9.rc)) ])
disp(['T2000D1E10 放' num2str(mean(T.T2000D1E10.rc)) ' 夹非畉' num2str(std(T.T2000D1E10.rc)) '   緻' num2str(mean(D.T2000D1E10.rc)) ' 夹非畉' num2str(std(D.T2000D1E10.rc)) ])
disp(['T2000D1E11 放' num2str(mean(T.T2000D1E11.rc)) ' 夹非畉' num2str(std(T.T2000D1E11.rc)) '   緻' num2str(mean(D.T2000D1E11.rc)) ' 夹非畉' num2str(std(D.T2000D1E11.rc)) ])
disp(['T2000D1E12 放' num2str(mean(T.T2000D1E12.rc)) ' 夹非畉' num2str(std(T.T2000D1E12.rc)) '   緻' num2str(mean(D.T2000D1E12.rc)) ' 夹非畉' num2str(std(D.T2000D1E12.rc)) ])
disp(['T2500D1E11 放' num2str(mean(T.T2500D1E11.rc)) ' 夹非畉' num2str(std(T.T2500D1E11.rc)) '   緻' num2str(mean(D.T2500D1E11.rc)) ' 夹非畉' num2str(std(D.T2500D1E11.rc)) ])


figure()
% subplot(2,2,1)
hold on
errorbar(...
    [9 10 11 12],...
    [(mean(T.T2000D1E9.rc)-2000)/2000*100 (mean(T.T2000D1E10.rc)-2000)/2000*100 (mean(T.T2000D1E11.rc)-2000)/2000*100 (mean(T.T2000D1E12.rc)-2000)/2000*100],...
    [std(T.T2000D1E9.rc)/2000*100 std(T.T2000D1E10.rc)/2000*100 std(T.T2000D1E11.rc)/2000*100 std(T.T2000D1E12.rc)/2000*100],'b')
plot([9 12],[0 0],'g')
xlabel('\fontsize{12}Electron Density 10^n(cm^{-3})');ylabel('\fontsize{12}Error of Electron Density(%)');
legend('Log-Linear Regression','Desired Value')
set(gca,'FontSize',12)
grid on

figure()
% subplot(2,2,3)
hold on
errorbar(...
    [9 10 11 12],...
    [(mean(D.T2000D1E9.rc)-1E9)/1E9*100 (mean(D.T2000D1E10.rc)-1E10)/1E10*100 (mean(D.T2000D1E11.rc)-1E11)/1E11*100 (mean(D.T2000D1E12.rc)-1E12)/1E12*100],...
    [std(D.T2000D1E9.rc)/1E9*100 std(D.T2000D1E10.rc)/1E10*100 std(D.T2000D1E11.rc)/1E11*100 std(D.T2000D1E12.rc)/1E12*100],'b')
plot([9 12],[0 0],'g')
xlabel('\fontsize{12}Electron Density 10^n(cm^{-3})');ylabel('\fontsize{12}Error of Electron Temperature(%)');
legend('Log-Linear Regression','Desired Value')
set(gca,'FontSize',12)
grid on

figure()
% subplot(2,2,2)
hold on
errorbar(...
    [1500 2000 2500],...
    [(mean(T.T1500D1E11.rc)-1500)/1500*100 (mean(T.T2000D1E11.rc)-2000)/2000*100 (mean(T.T2500D1E11.rc)-2500)/2500*100],...
    [std(T.T1500D1E11.rc)/1500*100 std(T.T2000D1E11.rc)/2000*100 std(T.T2500D1E11.rc)/2500*100],'b')
plot([1500 2500],[0 0],'g')
xlabel('\fontsize{12}Electron Electron Temperature(K)');ylabel('\fontsize{12}Error of Electron Temperature(%)');
legend('Log-Linear Regression','Desired Value')
set(gca,'FontSize',12)
grid on

figure()
% subplot(2,2,4)
hold on
errorbar(...
    [1500 2000 2500],...
    [(mean(D.T1500D1E11.rc)-1E11)/1E11*100 (mean(D.T2000D1E11.rc)-1E11)/1E11*100 (mean(D.T2500D1E11.rc)-1E11)/1E11*100],...
    [std(D.T1500D1E11.rc)/1E11*100 std(D.T2000D1E11.rc)/1E11*100 std(D.T2500D1E11.rc)/1E11*100],'b')
plot([1500 2500],[0 0],'g')
xlabel('\fontsize{12}Electron Electron Temperature(K)');ylabel('\fontsize{12}Error of Electron Density(%)');
legend('Log-Linear Regression','Desired Value')
set(gca,'FontSize',12)
grid on
