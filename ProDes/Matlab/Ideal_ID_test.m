% 面積比6，閒置電流吻合 %

clear;clc;close all;

%%%%%% 路徑 %%%%%%
path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因(高解)\';
content='迴路電流';
[Real.T1,Real.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[Real.T5,Real.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[~,Real.Vc1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[~,Real.Vc5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='掃描電壓';
[~,Real.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[~,Real.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);

% save Ideal_ID_test Real
% load Ideal_ID_test

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2500K_1E11\';
content='迴路電流';
[~,Guess.IV(:,1)]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
content='掃描電壓';
[~,Guess.IV(:,2)]=textread([path '無汙染IV曲線\' content '.txt'],'%f %f','headerlines',1);
% save idou_ID_fast Guess
% load idou_ID_fast

time=[1.9:15E-6:3];
x=1;
for i=2:size(Real.T1,1)
    if Real.T1(i-1)<=time(x) && Real.T1(i)>=time(x)
        Real.Tfix1(x,1)=Real.T1(i-1);
        Real.Ifix1(x,1)=Real.I1(i-1);
        x=x+1;
        if x==size(time,2)
            break
        end
    end
end
x=1;
for i=2:size(Real.T5,1)
    if Real.T5(i-1)<=time(x) && Real.T5(i)>=time(x)
        Real.Tfix5(x,1)=Real.T5(i-1);
        Real.Ifix5(x,1)=Real.I5(i-1);
        x=x+1;
        if x==size(time,2)
            break
        end
    end
end

gosa=5;
kai=1;


for i=1:kai
    tic
    
    Real.Inoise1=Real.Ifix1+rand(size(Real.Ifix1,1),1)*abs(max(Real.I1))*gosa/100-abs(max(Real.I1))*gosa/100/2;
    Real.Inoise5=Real.Ifix5+rand(size(Real.Ifix5,1),1)*abs(min(Real.I5))*gosa/100-abs(min(Real.I5))*gosa/100/2;

%     [idouT1,idouI1]=idou_ID([Real.Tfix1(:,1) Real.Inoise1(:,1)],100);
%     [idouT5,idouI5]=idou_ID([Real.Tfix5(:,1) Real.Inoise5(:,1)],100);
    tensuu=64;
    [ten1,A1.N10(i),B1.N10(i)]=idou_ID_fast([Real.Tfix1(:,1),Real.Inoise1(:,1)],2+7*0.001,1E11,tensuu,1);
    [ten5,A5.N10(i),B5.N10(i)]=idou_ID_fast([Real.Tfix5(:,1),Real.Inoise5(:,1)],2+7*0.001,1E11,tensuu,5);
    tensuu=128;
    [ten1,A1.N20(i),B1.N20(i)]=idou_ID_fast([Real.Tfix1(:,1),Real.Inoise1(:,1)],2+7*0.001,1E11,tensuu,1);
    [ten5,A5.N20(i),B5.N20(i)]=idou_ID_fast([Real.Tfix5(:,1),Real.Inoise5(:,1)],2+7*0.001,1E11,tensuu,5);
    tensuu=256;
    [ten1,A1.N30(i),B1.N30(i)]=idou_ID_fast([Real.Tfix1(:,1),Real.Inoise1(:,1)],2+7*0.001,1E11,tensuu,1);
    [ten5,A5.N30(i),B5.N30(i)]=idou_ID_fast([Real.Tfix5(:,1),Real.Inoise5(:,1)],2+7*0.001,1E11,tensuu,5);
    tensuu=512;
    [ten1,A1.N40(i),B1.N40(i)]=idou_ID_fast([Real.Tfix1(:,1),Real.Inoise1(:,1)],2+7*0.001,1E11,tensuu,1);
    [ten5,A5.N40(i),B5.N40(i)]=idou_ID_fast([Real.Tfix5(:,1),Real.Inoise5(:,1)],2+7*0.001,1E11,tensuu,5);

    toc

    disp(['執行次數  ' num2str(i) '/' num2str(kai) '     進度' num2str(i/kai*100) '%' ])


end

% save Ideal_ID_test_data A1 A5 B1 B5

figure()
% subplot(2,2,1)
% plot(Real.T1,Real.I1,Real.Tfix1(:,1),Real.Inoise1(:,1),idouT1,idouI1)
% subplot(2,2,2)
% plot(Real.T5,Real.I5,Real.Tfix5(:,1),Real.Inoise5(:,1),idouT5,idouI5)


subplot(2,2,2)
hold on
plot(Real.T1,Real.Vc1,Real.T5,Real.Vc5)
t=2:0.001:5;
ke1=A1(1)*exp(B1(1)*t);
ke5=A5(1)*exp(B5(1)*t);
plot(t,-ke1,'r--')
plot(t,ke5,'r--')
plot(ten1(:,1),ten1(:,3),'rx')
plot(ten5(:,1),ten5(:,3),'rx')

subplot(2,2,3)
hold on
plot(Real.T1,Real.I1,Real.T5,Real.I5)
plot(ten1(:,1),ten1(:,2),'rx')
plot(ten5(:,1),ten5(:,2),'rx')

subplot(2,2,4)
hold on
plot(Real.T1,Real.Vs1,Real.T5,Real.Vs5)




% Real.Inoise1=Real.I1+rand(size(Real.I1,1),1)*abs(max(Real.I1))*gosa/100-abs(max(Real.I1))*gosa/100/2;

% 放電tau %

% ten=1;
% Srapid=0.01;
% D=1E11;
% 
% tau_v=0.001;
% Ti=2;
% Ten=100;
% kan=0.01;
% 
% gosa=5;
% 
% Real.Inoise1=Real.I1+rand(size(Real.I1,1),1)*abs(max(Real.I1))*gosa/100-abs(max(Real.I1))*gosa/100/2;
% Real.Inoise5=Real.I5+rand(size(Real.I5,1),1)*abs(min(Real.I5))*gosa/100-abs(min(Real.I5))*gosa/100/2;
% 
% [idouT,idouI]=idou_ID_fast([Real.T1 Real.Inoise1],100,1.9,3.0,Ti+7*tau_v,Guess.IV,D);
% 
% 
% 
% figure(1)
% subplot(2,2,1)
% plot(Real.T1,Real.Inoise1,idouT,idouI)


% subplot(2,2,2)
% plot(Real.T5,Real.Inoise5)



% 
% figure(2)
% y=1;
% for i=[7]*tau_v
%     disp(['ID參數'])
%     [S1,A1(y),B1(y)]=Ideal_idou_ID([Real.T1,Real.I1],Ti+i,D,Ten,1);
%     [S5,A5(y),B5(y)]=Ideal_idou_ID([Real.T5,Real.I5],Ti+i,D,Ten,5);
%     disp(['時間差'])
%     [E1s(y,:,2),E1s(y,:,1)]=jikansa(Real.T1,Real.Vc1,A1(y),B1(y),0.001,abs(min(Real.Vc1)),kan);
%     [E5s(y,:,2),E5s(y,:,1)]=jikansa(Real.T5,-Real.Vc5,-A5(y),B5(y),0.001,abs(max(Real.Vc5)),kan);
%     disp(['結束'])
%     y=y+1;
% end
% close
% 
% figure(3)
% 
% % subplot(2,2,1)
% % hold on
% % plot(E1s(1,:,2),E1s(1,:,1),E1s(2,:,2),E1s(2,:,1),E1s(3,:,2),E1s(3,:,1),E1s(4,:,2),E1s(4,:,1),E1s(5,:,2),E1s(5,:,1))
% % plot(E5s(1,:,2),E5s(1,:,1),E5s(2,:,2),E5s(2,:,1),E5s(3,:,2),E5s(3,:,1),E5s(4,:,2),E5s(4,:,1),E5s(5,:,2),E5s(5,:,1))
% % xlim([0 0.1])
% % xlabel('\fontsize{12}容許電壓(V)');ylabel('\fontsize{12}時間差(s)');
% % legend('起始延遲5\tau_{v}','起始延遲6\tau_{v}','起始延遲7\tau_{v}','起始延遲8\tau_{v}','起始延遲9\tau_{v}','Location','southeast')
% % set(gca,'FontSize',12)
% % grid on
% 
% subplot(2,2,1)
% hold on
% plot(E1s(1,:,2),E1s(1,:,1))
% plot(E5s(1,:,2),E5s(1,:,1))
% % xlim([0 0.1])
% xlabel('\fontsize{12}容許電壓(V)');ylabel('\fontsize{12}時間差(s)');
% set(gca,'FontSize',12)
% grid on
% 
% 
% % 濃度估錯的誤差
% % 改濃度誤差的幅度
% % 改溫度誤差的幅度
% % 雜訊，取樣延遲，影響
% % 雜訊，移動平均組數，影響
% 
% 
% subplot(2,2,2)
% hold on
% plot(Real.T1,Real.Vc1,Real.T5,Real.Vc5)
% t=2:0.001:5;
% ke1=A1(1)*exp(B1(1)*t);
% ke5=A5(1)*exp(B5(1)*t);
% plot(t,-ke1,'r--')
% plot(t,ke5,'r--')
% plot(S1(:,1),S1(:,3),'rx')
% plot(S5(:,1),S5(:,3),'rx')
% 
% subplot(2,2,3)
% hold on
% plot(Real.T1,Real.I1,Real.T5,Real.I5)
% plot(S1(:,1),S1(:,2),'rx')
% plot(S5(:,1),S5(:,2),'rx')
% 
% subplot(2,2,4)
% hold on
% plot(Real.T1,Real.Vs1,Real.T5,Real.Vs5)


