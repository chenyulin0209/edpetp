
% 
% for i=1:1:100
%     % Time
%     Data(i,1) = i/100;
%     % Current
%     Data(i,2) = i/10*2;
% end
% StartTime=0.5;
% SamplePeriod=0.1; 
% SamplePoint = 3;
% minPeriod = 0.05;
function[Error,MaxCurrent,Got_data,A1,B1,C,kk]=santori_ID_180925(Data,StartTime,SamplePeriod,minPeriod,SamplePoint,RealValue)
    kk=1;

    %PSPICE模擬出的總點數
    DataLength=size(Data);
    %取樣開始時間
    SampleTime=StartTime;
    %存取資料
    Total_point = Data;
    
    tamp=1;ramp=1;
    for TimePoint=StartTime:minPeriod:StartTime+SamplePoint*SamplePeriod-minPeriod
        TimePoint;
        for j=ramp:DataLength(1)
            if Data(j,1) == TimePoint
                    Got_data(1,tamp)=Data(j,1);
                    Got_data(2,tamp)=Data(j,2);
                    tamp = tamp+1;
                    ramp = j;
                    break;
            elseif Data(j,1) <= TimePoint && Data(j+1,1) > TimePoint 
                    Got_data(1,tamp)=Data(j,1);
                    Got_data(2,tamp)=Data(j,2);
                    tamp = tamp+1;
                    ramp = j;
                    break;
            end
        end
    end
    
    GetDataLength=size(Got_data);
    ramp=0;
    circle = GetDataLength(2)/3;
   
        Average(1,1) = mean(Got_data(1,1:circle));
        Average(1,2) = mean(Got_data(1,circle+1:circle*2));
        Average(1,3) = mean(Got_data(1,circle*2+1:GetDataLength(2)));
        
        Average(2,1) = mean(Got_data(2,1:circle));
        Average(2,2) = mean(Got_data(2,circle+1:circle*2));
        Average(2,3) = mean(Got_data(2,circle*2+1:GetDataLength(2)));
       
    %   計算出係數C(這邊有用到一個副函式C_keisan，你對副函式按右鍵，會有一個OPEN C_keisan，就可以看到他內容了)
     C=real(C_keisan(Average(1,:),Average(2,:),SamplePeriod));
    %   透過取樣點的編號找到相對應的電流值存進矩陣
     Y=Got_data(2,:)';
     one=ones(1,GetDataLength(2))';
     %測試
    % C=2;
     
     ExpCt=exp(C*Got_data(1,:))';
     
     funcA = [one ExpCt];
     %   使出最小方差法
    funcB=inv(funcA'*funcA)*funcA'*Y;
    ID_result_funcB=real(funcB);
    
    MaxCurrent = ID_result_funcB(1,1)+ID_result_funcB(2,1);
    ID_result_funcB;
    A1=ID_result_funcB(1,1);
    B1=ID_result_funcB(2,1);
    
    Error = (((A1+B1)-RealValue)/RealValue)*100;
    
    

end