% 各溫濃下時間估算誤差 %

clear;clc;close all;

path='C:\Users\LinChen\Desktop\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\充電及放電\定變因\';
content='掃描電壓';
[D1E11.T1,D1E11.Vs1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.Vs5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='迴路電流';
[D1E11.T1,D1E11.I1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.I5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);
content='汙染跨壓';
[D1E11.T1,D1E11.V1]=textread([path '1V\' content '.txt'],'%f %f','headerlines',1);
[D1E11.T5,D1E11.V5]=textread([path '-5V\' content '.txt'],'%f %f','headerlines',1);


ten=1;
Srapid=0.01;


tau_v=0.001;
Ti=2;
Ten=100;
kan=0.001;
baisuu=7;
x=1;
for i=[4 5 6 7 8]
    baisuu=i;
    D=1E11;
    [S1,D1E11.A1,D1E11.B1]=Ideal_idou_ID([D1E11.T1,D1E11.I1],Ti+baisuu*tau_v,D,Ten,1);
    [S5,D1E11.A5,D1E11.B5]=Ideal_idou_ID([D1E11.T5,D1E11.I5],Ti+baisuu*tau_v,D,Ten,5);
    [D1E11.E1(x,:,2),D1E11.E1(x,:,1)]=jikansa(D1E11.T1,D1E11.V1,D1E11.A1,D1E11.B1,0.001,abs(min(D1E11.V1)),kan);
    [D1E11.E5(x,:,2),D1E11.E5(x,:,1)]=jikansa(D1E11.T5,-D1E11.V5,-D1E11.A5,D1E11.B5,0.001,abs(max(D1E11.V5)),kan);
    x=x+1;
end
figure()
hold on
% subplot(2,2,3)
plot(D1E11.E1(1,:,2),D1E11.E1(1,:,1),D1E11.E1(2,:,2),D1E11.E1(2,:,1),D1E11.E1(3,:,2),D1E11.E1(3,:,1),D1E11.E1(4,:,2),D1E11.E1(4,:,1),D1E11.E1(5,:,2),D1E11.E1(5,:,1))
plot(D1E11.E5(1,:,2),D1E11.E5(1,:,1),D1E11.E5(2,:,2),D1E11.E5(2,:,1),D1E11.E5(3,:,2),D1E11.E5(3,:,1),D1E11.E5(4,:,2),D1E11.E5(4,:,1),D1E11.E5(5,:,2),D1E11.E5(5,:,1))

xlabel('\fontsize{12}Tolerance of Voltage(V)');ylabel('\fontsize{12}Estimated Time Difference(s)');
legend('4\tau_v','5\tau_v','6\tau_v','7\tau_v','8\tau_v','location','southeast')
set(gca,'FontSize',12)


