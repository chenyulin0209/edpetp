clc;clear;close all;

% 電流時間常數
tau_c=0.1674;
% 理想情況下，量測到的電流值
seikaku=-1.90522*10^(-6);

% 讀資料
path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_ideal.T,tau_v_ideal.I]=textread([path '理想\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_100_p1V.T,tau_v_over_100_p1V.I]=textread([path 'tau_v_0.1674over100\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_p1V.T,tau_v_over_300_p1V.I]=textread([path 'tau_v_0.1674over300\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_p1V.T,tau_v_over_500_p1V.I]=textread([path 'tau_v_0.1674over500\1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_p1V.T,tau_v_over_1000_p1V.I]=textread([path 'tau_v_0.1674over1000\1.0V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_over_100_n1V.T,tau_v_over_100_n1V.I]=textread([path 'tau_v_0.1674over100\-1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_n1V.T,tau_v_over_300_n1V.I]=textread([path 'tau_v_0.1674over300\-1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_n1V.T,tau_v_over_500_n1V.I]=textread([path 'tau_v_0.1674over500\-1.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_n1V.T,tau_v_over_1000_n1V.I]=textread([path 'tau_v_0.1674over1000\-1.0V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_over_100_n2V.T,tau_v_over_100_n2V.I]=textread([path 'tau_v_0.1674over100\-2.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_n2V.T,tau_v_over_300_n2V.I]=textread([path 'tau_v_0.1674over300\-2.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_n2V.T,tau_v_over_500_n2V.I]=textread([path 'tau_v_0.1674over500\-2.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_n2V.T,tau_v_over_1000_n2V.I]=textread([path 'tau_v_0.1674over1000\-2.0V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_over_100_n3V.T,tau_v_over_100_n3V.I]=textread([path 'tau_v_0.1674over100\-3.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_n3V.T,tau_v_over_300_n3V.I]=textread([path 'tau_v_0.1674over300\-3.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_n3V.T,tau_v_over_500_n3V.I]=textread([path 'tau_v_0.1674over500\-3.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_n3V.T,tau_v_over_1000_n3V.I]=textread([path 'tau_v_0.1674over1000\-3.0V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_over_100_n4V.T,tau_v_over_100_n4V.I]=textread([path 'tau_v_0.1674over100\-4.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_n4V.T,tau_v_over_300_n4V.I]=textread([path 'tau_v_0.1674over300\-4.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_n4V.T,tau_v_over_500_n4V.I]=textread([path 'tau_v_0.1674over500\-4.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_n4V.T,tau_v_over_1000_n4V.I]=textread([path 'tau_v_0.1674over1000\-4.0V\' content '.txt'],'%f %f','headerlines',1);

path='C:\Users\q8529\Documents\NCU_LAB\07_EDPETP_handover\ProDes\Matlab\數據\2000K_1E11\單脈衝\';
content='迴路電流';
[tau_v_over_100_n5V.T,tau_v_over_100_n5V.I]=textread([path 'tau_v_0.1674over100\-5.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_300_n5V.T,tau_v_over_300_n5V.I]=textread([path 'tau_v_0.1674over300\-5.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_500_n5V.T,tau_v_over_500_n5V.I]=textread([path 'tau_v_0.1674over500\-5.0V\' content '.txt'],'%f %f','headerlines',1);
[tau_v_over_1000_n5V.T,tau_v_over_1000_n5V.I]=textread([path 'tau_v_0.1674over1000\-5.0V\' content '.txt'],'%f %f','headerlines',1);
x=1;
TT(1)=0;
DD_1(1)=-seikaku;
% 找出電流最低點的時間和數值
[TT(2),DD_1(2),~]=D_find_T(tau_v_over_100_p1V.T,-tau_v_over_100_p1V.I,-min(tau_v_over_100_p1V.I));
[TT(2),DD_2(2),~]=D_find_T(tau_v_over_100_n1V.T,-tau_v_over_100_n1V.I,-min(tau_v_over_100_n1V.I));
[TT(2),DD_3(2),~]=D_find_T(tau_v_over_100_n2V.T,-tau_v_over_100_n2V.I,-min(tau_v_over_100_n2V.I));
[TT(2),DD_4(2),~]=D_find_T(tau_v_over_100_n3V.T,-tau_v_over_100_n3V.I,-min(tau_v_over_100_n3V.I));
[TT(2),DD_5(2),~]=D_find_T(tau_v_over_100_n4V.T,-tau_v_over_100_n4V.I,-min(tau_v_over_100_n4V.I));
[TT(2),DD_6(2),~]=D_find_T(tau_v_over_100_n5V.T,-tau_v_over_100_n5V.I,-min(tau_v_over_100_n5V.I));
x
x=x+1;
A_1(1)=(DD_1(2)-DD_1(1))/DD_1(1)*100;
A_1(2)=(DD_2(2)-DD_1(1))/DD_1(1)*100;
A_1(3)=(DD_3(2)-DD_1(1))/DD_1(1)*100;
A_1(4)=(DD_4(2)-DD_1(1))/DD_1(1)*100;
A_1(5)=(DD_5(2)-DD_1(1))/DD_1(1)*100;
A_1(6)=(DD_6(2)-DD_1(1))/DD_1(1)*100;
max_100=max([A_1(1),A_1(2),A_1(3),A_1(4),A_1(5),A_1(6)])
min_100=min([A_1(1),A_1(2),A_1(3),A_1(4),A_1(5),A_1(6)])

[TT(3),DD_1(3),~]=D_find_T(tau_v_over_300_p1V.T,-tau_v_over_300_p1V.I,-min(tau_v_over_300_p1V.I));
[TT(3),DD_2(3),~]=D_find_T(tau_v_over_300_n1V.T,-tau_v_over_300_n1V.I,-min(tau_v_over_300_n1V.I));
[TT(3),DD_3(3),~]=D_find_T(tau_v_over_300_n2V.T,-tau_v_over_300_n2V.I,-min(tau_v_over_300_n2V.I));
[TT(3),DD_4(3),~]=D_find_T(tau_v_over_300_n3V.T,-tau_v_over_300_n3V.I,-min(tau_v_over_300_n3V.I));
[TT(3),DD_5(3),~]=D_find_T(tau_v_over_300_n4V.T,-tau_v_over_300_n4V.I,-min(tau_v_over_300_n4V.I));
[TT(3),DD_6(3),~]=D_find_T(tau_v_over_300_n5V.T,-tau_v_over_300_n5V.I,-min(tau_v_over_300_n5V.I));
x
x=x+1;
A_2(1)=(DD_1(3)-DD_1(1))/DD_1(1)*100;
A_2(2)=(DD_2(3)-DD_1(1))/DD_1(1)*100;
A_2(3)=(DD_3(3)-DD_1(1))/DD_1(1)*100;
A_2(4)=(DD_4(3)-DD_1(1))/DD_1(1)*100;
A_2(5)=(DD_5(3)-DD_1(1))/DD_1(1)*100;
A_2(6)=(DD_6(3)-DD_1(1))/DD_1(1)*100;
max_300=max([A_2(1),A_2(2),A_2(3),A_2(4),A_2(5),A_2(6)])
min_300=min([A_2(1),A_2(2),A_2(3),A_2(4),A_2(5),A_2(6)])

[TT(4),DD_1(4),~]=D_find_T(tau_v_over_500_p1V.T,-tau_v_over_500_p1V.I,-min(tau_v_over_500_p1V.I));
[TT(4),DD_2(4),~]=D_find_T(tau_v_over_500_n1V.T,-tau_v_over_500_n1V.I,-min(tau_v_over_500_n1V.I));
[TT(4),DD_3(4),~]=D_find_T(tau_v_over_500_n2V.T,-tau_v_over_500_n2V.I,-min(tau_v_over_500_n2V.I));
[TT(4),DD_4(4),~]=D_find_T(tau_v_over_500_n3V.T,-tau_v_over_500_n3V.I,-min(tau_v_over_500_n3V.I));
[TT(4),DD_5(4),~]=D_find_T(tau_v_over_500_n4V.T,-tau_v_over_500_n4V.I,-min(tau_v_over_500_n4V.I));
[TT(4),DD_6(4),~]=D_find_T(tau_v_over_500_n5V.T,-tau_v_over_500_n5V.I,-min(tau_v_over_500_n5V.I));

x
x=x+1;
A_3(1)=(DD_1(4)-DD_1(1))/DD_1(1)*100;
A_3(2)=(DD_2(4)-DD_1(1))/DD_1(1)*100;
A_3(3)=(DD_3(4)-DD_1(1))/DD_1(1)*100;
A_3(4)=(DD_4(4)-DD_1(1))/DD_1(1)*100;
A_3(5)=(DD_5(4)-DD_1(1))/DD_1(1)*100;
A_3(6)=(DD_6(4)-DD_1(1))/DD_1(1)*100;
max_500=max([A_3(1),A_3(2),A_3(3),A_3(4),A_3(5),A_3(6)])
min_500=min([A_3(1),A_3(2),A_3(3),A_3(4),A_3(5),A_3(6)])

[TT(5),DD_1(5),~]=D_find_T(tau_v_over_1000_p1V.T,-tau_v_over_1000_p1V.I,-min(tau_v_over_1000_p1V.I));
[TT(5),DD_2(5),~]=D_find_T(tau_v_over_1000_n1V.T,-tau_v_over_1000_n1V.I,-min(tau_v_over_1000_n1V.I));
[TT(5),DD_3(5),~]=D_find_T(tau_v_over_1000_n2V.T,-tau_v_over_1000_n2V.I,-min(tau_v_over_1000_n2V.I));
[TT(5),DD_4(5),~]=D_find_T(tau_v_over_1000_n3V.T,-tau_v_over_1000_n3V.I,-min(tau_v_over_1000_n3V.I));
[TT(5),DD_5(5),~]=D_find_T(tau_v_over_1000_n4V.T,-tau_v_over_1000_n4V.I,-min(tau_v_over_1000_n4V.I));
[TT(5),DD_6(5),~]=D_find_T(tau_v_over_1000_n5V.T,-tau_v_over_1000_n5V.I,-min(tau_v_over_1000_n5V.I));
x
x=x+1;
A_4(1)=(DD_1(5)-DD_1(1))/DD_1(1)*100;
A_4(2)=(DD_2(5)-DD_1(1))/DD_1(1)*100;
A_4(3)=(DD_3(5)-DD_1(1))/DD_1(1)*100;
A_4(4)=(DD_4(5)-DD_1(1))/DD_1(1)*100;
A_4(5)=(DD_5(5)-DD_1(1))/DD_1(1)*100;
A_4(6)=(DD_6(5)-DD_1(1))/DD_1(1)*100;
max_1000=max([A_4(1),A_4(2),A_4(3),A_4(4),A_4(5),A_4(6)])
min_1000=min([A_4(1),A_4(2),A_4(3),A_4(4),A_4(5),A_4(6)])







