%{
ヘ
だ猂ronbun_T暗ㄓ挡狦ゑ耕程猭㎝计猭芠诡ㄢキА粇畉㎝夹非畉
灿竊

%}
clc;clear;close all;

% 弄ronbun_T挡狦
load ronbun_T_data

% рㄢ贺よ猭キА放㎝夹非畉キА緻㎝夹非畉陪ボㄓ
disp(['计  放キА ' num2str(mean(T.T1500D1E11.rc)) '   放夹非畉 ' num2str(std(T.T1500D1E11.rc)) '  緻キА ' num2str(mean(D.T1500D1E11.rc)) '  緻夹非畉 ' num2str(std(D.T1500D1E11.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T1500D1E11.ma)) '   放夹非畉 ' num2str(std(T.T1500D1E11.ma)) '  緻キА ' num2str(mean(D.T1500D1E11.ma)) '  緻夹非畉 ' num2str(std(D.T1500D1E11.ma)) ])
disp(['计  放キА ' num2str(mean(T.T2000D1E11.rc)) '   放夹非畉 ' num2str(std(T.T2000D1E11.rc)) '  緻キА ' num2str(mean(D.T2000D1E11.rc)) '  緻夹非畉 ' num2str(std(D.T2000D1E11.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2000D1E11.ma)) '   放夹非畉 ' num2str(std(T.T2000D1E11.ma)) '  緻キА ' num2str(mean(D.T2000D1E11.ma)) '  緻夹非畉 ' num2str(std(D.T2000D1E11.ma)) ])
disp(['计  放キА ' num2str(mean(T.T2500D1E11.rc)) '   放夹非畉 ' num2str(std(T.T2500D1E11.rc)) '  緻キА ' num2str(mean(D.T2500D1E11.rc)) '  緻夹非畉 ' num2str(std(D.T2500D1E11.rc)) ])
disp(['程猭  放キА ' num2str(mean(T.T2500D1E11.ma)) '   放夹非畉 ' num2str(std(T.T2500D1E11.ma)) '  緻キА ' num2str(mean(D.T2500D1E11.ma)) '  緻夹非畉 ' num2str(std(D.T2500D1E11.ma)) ])

% 礶ぃ緻放︳衡粇畉瓜
figure()
hold on
errorbar(...
    [1500 2000 2500],...
    [(mean(T.T1500D1E11.rc-1500)/1500*100) (mean(T.T2000D1E11.rc-2000)/2000*100) (mean(T.T2500D1E11.rc-2500)/2500*100)],...
    [(std(T.T1500D1E11.rc)/1500*100) (std(T.T2000D1E11.rc)/2000*100) (std(T.T2500D1E11.rc)/2500*100)],'r')
errorbar(...
    [1500 2000 2500],...
    [(mean(T.T1500D1E11.ma-1500)/1500*100) (mean(T.T2000D1E11.ma-2000)/2000*100) (mean(T.T2500D1E11.ma-2500)/2500*100)],...
    [(std(T.T1500D1E11.ma)/1500*100) (std(T.T2000D1E11.ma)/2000*100) (std(T.T2500D1E11.ma)/2500*100)],'b')
plot([1500 2500],[0 0],'g')
xlabel('\fontsize{12}Electron Temperature(K)');ylabel('\fontsize{12}Error of Electron Temperature(%)');
legend('Log-Linear Regression','Maximum Current Algorithm','Desired Value','location','northeast')
set(gca,'FontSize',12)
grid on

% ㏕﹚跑筿緻1E11
Dre=1E11;
% 礶ぃ緻緻︳衡粇畉瓜
figure()
hold on
errorbar(...
    [1500 2000 2500],...
    [(mean(D.T1500D1E11.rc-Dre)/Dre*100) (mean(D.T2000D1E11.rc-Dre)/Dre*100) (mean(D.T2500D1E11.rc-Dre)/Dre*100)],...
    [(std(D.T1500D1E11.rc)/Dre*100) (std(D.T2000D1E11.rc)/Dre*100) (std(D.T2500D1E11.rc)/Dre*100)],'r')
errorbar(...
    [1500 2000 2500],...
    [(mean(D.T1500D1E11.ma-Dre)/Dre*100) (mean(D.T2000D1E11.ma-Dre)/Dre*100) (mean(D.T2500D1E11.ma-Dre)/Dre*100)],...
    [(std(D.T1500D1E11.ma)/Dre*100) (std(D.T2000D1E11.ma)/Dre*100) (std(D.T2500D1E11.ma)/Dre*100)],'b')
plot([1500 2500],[0 0],'g')
ylim([-10 35])
xlabel('\fontsize{12}Electron Temperature(K)');ylabel('\fontsize{12}Error of Electron Density(%)');
legend('Log-Linear Regression','Maximum Current Algorithm','Desired Value','location','northeast')
set(gca,'FontSize',12)
grid on




